final Map<String, String> enUs = {
  // Home Screen
  "lbl_32": "32", "lbl_5": "5", "lbl_active_form": "Active Form",
  "lbl_all_report": "All Report", "lbl_my_report": "My Report",

  // Home Two Screen
  "lbl_12": "12",

  // Hasil Pencarian Screen
  "lbl_1": "1", "lbl_minggu_ini2": "Minggu INi",
  "lbl_paslon_kaltim": "Paslon Kaltim",

  // Profile Screen
  "lbl_0210230223": "0210230223",
  "lbl_form_saya": "Form Saya",
  "lbl_klien_c": "Klien C",
  "lbl_klien_d": "Klien D",
  "lbl_test_gmail_com": "Test@gmail.com",
  "lbl_undangan": "Undangan",
  "lbl_workspace_saya": "Workspace Saya",
  "msg_1232138693834912": "1232138693834912",

  // Notifikasi One Screen
  "lbl_info": "Info", "lbl_pilpres_2024": "Pilpres 2024",

  // Search Screen
  "msg_filter_terpasang": "Filter terpasang",

  // Workspace Screen
  "lbl_formulir": "Formulir",
  "msg_subroto_suroto_ebdesk": "Subroto (Suroto@ebdesk)",

  // Form Laporan Screen
  "lbl_pilih_form": "Pilih Form",
  "msg_amet_minim_mollit3":
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. ",

  // Form Laporan One Screen
  "lbl_batal": "Batal",

  // Edit Profile One Screen
  "lbl_password": "Password", "msg_account_information": "Account Information",

  // Edit Profile Screen
  "lbl_address": "Address",
  "lbl_edit_photo": "Edit Photo",
  "lbl_gender": "Gender",
  "lbl_male": "Male",
  "lbl_name": "Name*",
  "lbl_name2": "Name",
  "msg_3172321433431321": "3172321433431321",
  "msg_account_information2": "Account Information ",
  "msg_jalan_pasar_minggu": "Jalan Pasar Minggu no 62",
  "msg_y_andri_r07_gmail_com": "y.andri.r07@gmail.com",

  // Change Password Screen
  "lbl_ask_me_anything": "Ask me anything",
  "lbl_change_password": "Change Password",
  "lbl_new_password": "New Password",
  "msg_confirm_password": "Confirm Password",
  "msg_current_password": "Current Password",

  // Login Screen
  "lbl3": "•••••••••••",
  "lbl_daftar": "Daftar",
  "lbl_email3": "Email ",
  "lbl_ingat_saya": "Ingat Saya",
  "msg_belum_punya_akun": "Belum punya akun?",
  "msg_johndoe_gmail_com": "johndoe@gmail.com",
  "msg_masuk_ke_dalam_akunmu": "Masuk ke dalam akunmu",
  "msg_selamat_datang": "Selamat Datang, Silahkan Isi Detil Anda",

  // Register Screen
  "lbl_buat_akun": "Buat Akun",
  "lbl_john_doe": "John Doe",
  "lbl_log_in": "Log in",
  "lbl_mulai": "Mulai",
  "lbl_nama": "Nama*",
  "lbl_nama2": "Nama",
  "lbl_nomor_handphone": "Nomor Handphone",
  "msg_already_have_an": "Already have an account?",
  "msg_daftar_sekarang": "Daftar sekarang untuk memulai akunmu",
  "msg_i_have_read_and": "I have read and agree to the Terms of Service",
  "msg_nomor_handphone": "Nomor Handphone*",

  // Forgot Password Screen
  "msg_kembali_ke_masuk": "Kembali ke Masuk",
  "msg_kode_konfirmasi": "Kode Konfirmasi akan dikirimkan ke\nalamat emailmu",

  // Sent Code Password Screen
  "lbl_kode_verifikasi": "Kode Verifikasi",
  "lbl_lanjut": "Lanjut",
  "msg_silakan_input_token":
      "Silakan input token untuk \nbergabung dengan workspace ",
  "msg_sudah_punya_akun": "Sudah punya akun?",

  // Set New Password Screen
  "msg_atur_ulang_sandi": "Atur Ulang Sandi",
  "msg_sandi_baru_kamu":
      "Sandi baru kamu harus berbeda dari sandi sebelumnya yang anda gunakan",

  // Notifikasi Screen
  "lbl_published": "Published",
  "lbl_unpublished": "Unpublished",
  "lbl_user": "User",
  "lbl_user_aktif": "User Aktif",
  "lbl_workspace_aktif": "Workspace Aktif",
  "msg_user_tidak_aktif": "User Tidak Aktif",
  "msg_worksace_tidak_aktif": "Worksace Tidak Aktif",

  // Tidak Ada Koneksi Screen
  "msg_tidak_ada_koneksi": "Tidak ada koneksi internet",
  "msg_whoops_mohon_periksa":
      "Whoops...Mohon periksa kembali koneksi\ninternet Anda dan coba lagi",

  // Data Sukses Screen
  "lbl_okay": "Okay",
  "msg_data_sukses_terkirim": "Data Sukses Terkirim",
  "msg_yeay_data_anda":
      "Yeay, data anda berhasil terkirim\ncek selengkapnya di form laporan",

  // Data Gagal Terkirim Screen
  "msg_data_gagal_terkirim": "Data Gagal Terkirim",
  "msg_whoops_sepertinya":
      "Whoops...Sepertinya data anda gagal terkirim, silahkan kirim ulang",

  // GPS Tidak Aktif Screen
  "lbl_gps_tidak_aktif": "GPS Tidak Aktif",
  "msg_pergi_ke_pengaturan": "Pergi ke Pengaturan",
  "msg_whoops_sepertinya2":
      "Whoops...Sepertinya GPS Anda tidak \naktif, ke pengaturan untuk mengaktifkan",

  // Lokasi Tidak Terdeteksi Screen
  "lbl_kembali": "Kembali",
  "msg_lokasi_anda_tidak": "Lokasi  Anda Tidak Terdeteksi",
  "msg_sepertinya_lokasi":
      "Sepertinya Lokasi Anda tidak Benar, sesuaikan lokasi Anda",

  // Common String
  "lbl": "*",
  "lbl2": "?",
  "lbl_12_march_2022": "12 March 2022",
  "lbl_1300": "1300",
  "lbl_1_jam_lalu": "1 Jam Lalu",
  "lbl_2021103923": "2021103923",
  "lbl_230": "230",
  "lbl_23_03_22_16_36": "23/03/22 16:36",
  "lbl_2_jam_lalu": "2 Jam Lalu",
  "lbl_45": "45",
  "lbl_aktif": "Aktif",
  "lbl_albert_flores": "Albert Flores",
  "lbl_andri": "Andri",
  "lbl_atau": "Atau",
  "lbl_bantuan": "Bantuan",
  "lbl_bessie_cooper": "Bessie Cooper",
  "lbl_bulan_ini": "Bulan Ini",
  "lbl_coba_lagi": "Coba Lagi",
  "lbl_detail": "Detail",
  "lbl_edit_profile": "Edit Profile",
  "lbl_email": "Email*",
  "lbl_email2": "Email",
  "lbl_filter": "Filter",
  "lbl_form": "Form",
  "lbl_hari_ini": "Hari Ini",
  "lbl_hasil_pencarian": "Hasil Pencarian",
  "lbl_hi_michael": "Hi, Michael",
  "lbl_kepemilikan": "Kepemilikan",
  "lbl_kirim": "Kirim",
  "lbl_klien_a": "Klien A",
  "lbl_klien_b": "Klien B",
  "lbl_kristin_watson": "Kristin Watson",
  "lbl_laporan_covid": "Laporan Covid",
  "lbl_laporan_saya": "Laporan Saya",
  "lbl_lupa_sandi": "Lupa Sandi?",
  "lbl_margareth": "Margareth",
  "lbl_masuk": "Masuk",
  "lbl_minggu_ini": "Minggu Ini",
  "lbl_nik": "NIK*",
  "lbl_nik2": "NIK",
  "lbl_notifikasi": "Notifikasi",
  "lbl_pembuat": "Pembuat",
  "lbl_safarudin": "Safarudin",
  "lbl_sampai": "Sampai*",
  "lbl_sampai2": "Sampai",
  "lbl_sandi": "Sandi",
  "lbl_sandi2": "Sandi*",
  "lbl_save_changes": "Save & Changes",
  "lbl_search": "Search",
  "lbl_semua_laporan": "Semua Laporan",
  "lbl_status": "Status",
  "lbl_tahun_ini": "Tahun Ini",
  "lbl_tampilkan": "Tampilkan",
  "lbl_tanggal": "Tanggal",
  "lbl_tanggal2": "Tanggal*",
  "lbl_tanggal_dibuat": "Tanggal Dibuat",
  "lbl_terbaru": "Terbaru",
  "lbl_theresa_webb": "Theresa Webb",
  "lbl_tugas": "Tugas",
  "lbl_ulang_sandi": "Ulang Sandi*",
  "lbl_ulang_sandi2": "Ulang Sandi",
  "lbl_workspace": "Workspace",
  "msg_01_jan_2021_29": "01 Jan 2021 - 29 Maret 2021 ",
  "msg_29_maret_2021_14_30": "29 Maret 2021 14:30",
  "msg_amet_minim_mollit": "Amet minim mollit non deserunt ullamco est sit... ",
  "msg_amet_minim_mollit2": "Amet minim mollit non deserunt est sit... ",
  "msg_andri_yulianto_rosadi": "Andri Yulianto Rosadi",
  "msg_brooklyn_simmons": "Brooklyn Simmons",
  "msg_elektabilitas_paslon": "Elektabilitas Paslon Kaltim",
  "msg_form_survey_terkait":
      "Form Survey terkait elektabilitas paslon pilpres \n2024",
  "msg_form_survey_terkait2":
      "Form Survey terkait elektabilitas paslon pilpres 2024",
  "msg_harits_harits_ebdesk_com": "Harits (Harits@ebdesk.com)",
  "msg_ini_berisikan_deskripsi":
      "Ini berisikan deskripsi yang berkitan dengan \npembuatan workspace A",
  "msg_isi_informasi_basik":
      "Isi informasi basik dibawah ini, agar bisa \nmencari data yang akurat",
  "msg_laporan_kegiatan": "Laporan Kegiatan Marketing",
  "msg_laporan_terakhir": "Laporan Terakhir",
  "msg_masuk_dengan_google": "Masuk dengan Google",
  "msg_selamat_anda_tergabung":
      "Selamat Anda Tergabung dengan workspace Pilpres\n2024",
  "msg_tandai_sudah_di": "Tandai Sudah di Baca (5)",

// Network Error String
  "msg_network_err": "Network Error",
  "msg_something_went_wrong": "Something Went Wrong!",

  // Validation Error String
  "err_msg_please_enter_valid_email": "Please enter valid email",
  "err_msg_please_enter_valid_password": "Please enter valid password",
};
