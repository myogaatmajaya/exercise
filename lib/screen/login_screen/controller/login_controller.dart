import 'package:exercise/core/app_export.dart';
import 'package:exercise/screen/register_screen/models/register_model.dart';
import 'package:exercise/screen/register_screen/repository/auth_repo.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginController extends GetxController {
  static LoginController get instance => Get.find();

  final email = TextEditingController();
  final phone = TextEditingController();
  final password = TextEditingController();
  final fullname = TextEditingController();

  void registerUser(String email, String password) {
    AuthRepo.instance.createUserWithEmailAndPassword(email, password);
  }

  Future<void> googleSignIn() async {
    try {
      final auth = AuthRepo.instance;
      await AuthRepo.instance.signInWithGoogle();
      auth.setInitialScreen(auth.firebaseUser.value);
    } catch (e) {
      print(e);
    }
  }
}
