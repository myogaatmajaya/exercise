import 'controller/login_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/core/utils/validation_functions.dart';
import 'package:exercise/widgets/custom_checkbox_button.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:exercise/widgets/custom_outlined_button.dart';
import 'package:exercise/widgets/custom_text_form_field.dart';
import 'package:flutter/material.dart';

class LoginScreen extends GetWidget<LoginController> {
  final controller = Get.put(LoginController());

  LoginScreen({Key? key})
      : super(
          key: key,
        );

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Form(
          key: _formKey,
          child: Container(
            width: double.maxFinite,
            padding: EdgeInsets.symmetric(horizontal: 16.h),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 37.v),
                Text(
                  "msg_masuk_ke_dalam_akunmu".tr,
                  style: CustomTextStyles.titleLarge22,
                ),
                SizedBox(height: 3.v),
                Text(
                  "msg_selamat_datang".tr,
                  style: CustomTextStyles.bodyMediumBluegray900d3,
                ),
                SizedBox(height: 25.v),
                CustomOutlinedButton(
                  onPressed: controller.googleSignIn,
                  text: "msg_masuk_dengan_google".tr,
                  margin: EdgeInsets.only(
                    left: 4.h,
                    right: 3.h,
                  ),
                  leftIcon: Container(
                    margin: EdgeInsets.only(right: 20.h),
                    child: CustomImageView(
                      imagePath: ImageConstant.imgGoogleglogo1,
                      height: 19.adaptSize,
                      width: 19.adaptSize,
                    ),
                  ),
                ),
                SizedBox(height: 16.v),
                _buildOrSeperator(),
                SizedBox(height: 13.v),
                _buildEmailAddress(),
                SizedBox(height: 14.v),
                _buildPassword(),
                SizedBox(height: 16.v),
                _buildIngatSaya(),
                SizedBox(height: 20.v),
                CustomElevatedButton(
                  height: 56.v,
                  text: "lbl_masuk".tr,
                  margin: EdgeInsets.only(
                    left: 4.h,
                    right: 3.h,
                  ),
                ),
                SizedBox(height: 32.v),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "msg_belum_punya_akun".tr,
                      style: CustomTextStyles.bodyMedium13,
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 2.h,
                        bottom: 2.v,
                      ),
                      child: Text(
                        "lbl_daftar".tr,
                        style: CustomTextStyles.labelLargeWorkSans,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildOrSeperator() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 17.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: 10.v,
              bottom: 8.v,
            ),
            child: SizedBox(
              width: 126.h,
              child: Divider(
                color: appTheme.blueGray90002.withOpacity(0.26),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 13.h),
            child: Text(
              "lbl_atau".tr,
              style: CustomTextStyles.bodyMediumBluegray90002,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: 10.v,
              bottom: 8.v,
            ),
            child: SizedBox(
              width: 138.h,
              child: Divider(
                color: appTheme.blueGray90002.withOpacity(0.26),
                indent: 12.h,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildEmailAddress() {
    return Padding(
      padding: EdgeInsets.only(left: 4.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 1.h),
            child: Text(
              "lbl_email3".tr,
              style: CustomTextStyles.labelLargeBluegray900_1,
            ),
          ),
          SizedBox(height: 3.v),
          CustomTextFormField(
            controller: controller.email,
            hintStyle: CustomTextStyles.titleSmallPrimaryContainer,
            textInputType: TextInputType.emailAddress,
            validator: (value) {
              if (value == null || (!isValidEmail(value, isRequired: true))) {
                return "err_msg_please_enter_valid_email".tr;
              }
              return null;
            },
            contentPadding: EdgeInsets.symmetric(
              horizontal: 14.h,
              vertical: 18.v,
            ),
            borderDecoration: TextFormFieldStyleHelper.outlineBlueGray,
            filled: false,
          ),
        ],
      ),
    );
  }

  Widget _buildPassword() {
    return Padding(
      padding: EdgeInsets.only(left: 4.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 1.h),
            child: Text(
              "lbl_sandi".tr,
              style: CustomTextStyles.labelLargeBluegray900_1,
            ),
          ),
          SizedBox(height: 3.v),
          CustomTextFormField(
            controller: controller.password,
            hintText: "".tr,
            hintStyle: CustomTextStyles.titleSmallPrimaryContainer,
            textInputType: TextInputType.emailAddress,
            validator: (value) {
              if (value == null || (!isValidEmail(value, isRequired: true))) {
                return "".tr;
              }
              return null;
            },
            contentPadding: EdgeInsets.symmetric(
              horizontal: 14.h,
              vertical: 18.v,
            ),
            borderDecoration: TextFormFieldStyleHelper.outlineBlueGray,
            filled: false,
          ),
        ],
      ),
    );
  }

  Widget _buildIngatSaya() {
    return Padding(
      padding: EdgeInsets.only(left: 4.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "lbl_lupa_sandi".tr,
            style: CustomTextStyles.labelLargeBluegray90003,
          ),
        ],
      ),
    );
  }
}
