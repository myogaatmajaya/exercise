import 'package:exercise/core/utils/image_constant.dart';
import 'package:exercise/core/utils/size_utils.dart';
import 'package:exercise/core/utils/utils.dart';
import 'package:exercise/screen/home_one_bottomsheet/home_one_bottomsheet.dart';
import 'package:exercise/screen/home_screen/controller/home_controller.dart';
import 'package:exercise/screen/notifikasi_screen/notifikasi_screen.dart';
import 'package:exercise/screen/profile_screen/profile_screen.dart';
import 'package:exercise/theme/custom_button_style.dart';
import 'package:exercise/theme/custom_text_style.dart';
import 'package:exercise/theme/theme_helper.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:exercise/widgets/custom_image_view.dart';
import 'package:exercise/widgets/custom_search_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  HomeController homeController = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    double baseWidth = 375;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.98;
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin:
                    EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 16 * fem),
                width: 600 * fem,
                height: 350 * fem,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(
                      'assets/page-1/images/rectangle-3009-FHX.png',
                    ),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                        child: Padding(
                      padding: EdgeInsets.only(top: 15.v),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Get.to(HomeOneBottomsheet);
                            },
                            child: Container(
                              margin: EdgeInsets.only(
                                  right: 10 * fem, left: 20 * fem),
                              child: CustomElevatedButton(
                                height: 26.v,
                                width: 61.h,
                                text: "lbl_filter".tr,
                                leftIcon: Container(
                                  margin: EdgeInsets.only(right: 4.h),
                                  child: CustomImageView(
                                    imagePath: ImageConstant.imgCifilter,
                                    height: 14.adaptSize,
                                    width: 14.adaptSize,
                                  ),
                                ),
                                buttonStyle: CustomButtonStyles.fillWhiteATL6,
                                buttonTextStyle:
                                    CustomTextStyles.bodyMediumWhiteA700,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Get.to(NotifikasiScreen());
                            },
                            child: Container(
                              margin: EdgeInsets.only(left: 190 * fem),
                              width: 32 * fem,
                              height: 32 * fem,
                              child: Image.asset(
                                'assets/page-1/images/frame-iYM.png',
                                width: 32 * fem,
                                height: 32 * fem,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Get.to(ProfileScreen());
                            },
                            child: Container(
                              margin: EdgeInsets.only(right: 25 * fem),
                              width: 28 * fem,
                              height: 28 * fem,
                              child: Image.asset(
                                'assets/page-1/images/group-2162.png',
                                width: 28 * fem,
                                height: 28 * fem,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
                    Padding(
                      padding: EdgeInsets.only(top: 30.5 * fem, left: 16 * fem),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          width: 111 * fem,
                          height: 28 * fem,
                          child: Text(
                            'Hi, Michael',
                            style: SafeGoogleFont(
                              'Open Sans',
                              fontSize: 20 * ffem,
                              fontWeight: FontWeight.w700,
                              height: 1.3625 * ffem / fem,
                              color: Color(0xffffffff),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: 50 * fem,
                      margin: EdgeInsets.only(right: 150 * fem),
                      padding: EdgeInsets.only(left: 16 * fem),
                      child: Container(
                        padding: EdgeInsets.fromLTRB(
                            5 * fem, 4 * fem, 5 * fem, 4 * fem),
                        height: 35 * fem,
                        decoration: BoxDecoration(
                          color: Color(0xffffffff),
                          borderRadius: BorderRadius.circular(20 * fem),
                        ),
                        child: DropdownButtonFormField<String>(
                          isExpanded: true,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20 * fem),
                            ),
                            contentPadding: EdgeInsets.symmetric(
                              vertical: 4 * fem,
                              horizontal: 8 * fem,
                            ),
                          ),
                          items: [
                            DropdownMenuItem<String>(
                              value: 'Option 1',
                              child: Text(
                                'Option 1',
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                            DropdownMenuItem<String>(
                              value: 'Option 2',
                              child: Text(
                                'Option 2',
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                          ],
                          onChanged: (String? value) {},
                          hint: Text('01-jan-2021'),
                          value: null,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30.h),
                      child: Stack(
                        alignment: Alignment.bottomLeft,
                        children: [
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              height: 15.adaptSize,
                              width: 15.adaptSize,
                              margin: EdgeInsets.only(right: 25.h),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  7.h,
                                ),
                                border: Border.all(
                                  color: appTheme.whiteA700.withOpacity(0.2),
                                  width: 2.h,
                                ),
                              ),
                            ),
                          ),
                          CustomImageView(
                            imagePath: ImageConstant.imgTelevision,
                            height: 11.v,
                            width: 9.h,
                            alignment: Alignment.bottomLeft,
                            margin: EdgeInsets.only(
                              left: 135.h,
                              bottom: 4.v,
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: CustomSearchView(
                              width: 343.h,
                              hintText: "lbl_search".tr,
                              alignment: Alignment.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 33.5 * fem,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Container(
                        child: Padding(
                          padding:
                              EdgeInsets.only(left: 16 * fem, bottom: 0 * fem),
                          child: Container(
                            width: 940 * fem,
                            height: 107 * fem,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  padding: EdgeInsets.fromLTRB(
                                      24 * fem, 20 * fem, 24 * fem, 19.5 * fem),
                                  width: 226 * fem,
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffeaedf6)),
                                    color: Color(0xffffffff),
                                    borderRadius:
                                        BorderRadius.circular(4 * fem),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0x26d0d2da),
                                        offset: Offset(
                                            0 * fem, 12.6698102951 * fem),
                                        blurRadius: 11.262055397 * fem,
                                      ),
                                    ],
                                  ),
                                  child: Container(
                                    width: 149 * fem,
                                    height: double.infinity,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.fromLTRB(0 * fem,
                                              0 * fem, 22.77 * fem, 1.5 * fem),
                                          width: 62.23 * fem,
                                          height: 60 * fem,
                                          child: Image.asset(
                                            'assets/page-1/images/not-found-illustration.png',
                                            width: 62.23 * fem,
                                            height: 60 * fem,
                                          ),
                                        ),
                                        Container(
                                          height: double.infinity,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    0 * fem,
                                                    0 * fem,
                                                    0 * fem,
                                                    3.5 * fem),
                                                child: Text(
                                                  '45',
                                                  style: SafeGoogleFont(
                                                    'Open Sans',
                                                    fontSize: 32 * ffem,
                                                    fontWeight: FontWeight.w600,
                                                    height: 1.3625 * ffem / fem,
                                                    color: Color(0xff000000),
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                'All Report',
                                                style: SafeGoogleFont(
                                                  'Open Sans',
                                                  fontSize: 14 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.3625 * ffem / fem,
                                                  color: Color(0xcc1b2b41),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 12 * fem,
                                ),
                                Container(
                                  padding: EdgeInsets.fromLTRB(33.76 * fem,
                                      19.5 * fem, 47.76 * fem, 20 * fem),
                                  width: 226 * fem,
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffeaedf6)),
                                    color: Color(0xffffffff),
                                    borderRadius:
                                        BorderRadius.circular(4 * fem),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0x26d0d2da),
                                        offset: Offset(
                                            0 * fem, 12.6698102951 * fem),
                                        blurRadius: 11.262055397 * fem,
                                      ),
                                    ],
                                  ),
                                  child: Container(
                                    width: double.infinity,
                                    height: double.infinity,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.fromLTRB(0 * fem,
                                              0 * fem, 16 * fem, 0.5 * fem),
                                          width: 60.48 * fem,
                                          height: 60 * fem,
                                          child: Image.asset(
                                            'assets/page-1/images/no-favorite-illustration-meM.png',
                                            width: 60.48 * fem,
                                            height: 60 * fem,
                                          ),
                                        ),
                                        Container(
                                          height: double.infinity,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    0 * fem,
                                                    0 * fem,
                                                    0 * fem,
                                                    3.5 * fem),
                                                child: Text(
                                                  '32',
                                                  style: SafeGoogleFont(
                                                    'Open Sans',
                                                    fontSize: 32 * ffem,
                                                    fontWeight: FontWeight.w600,
                                                    height: 1.3625 * ffem / fem,
                                                    color: Color(0xff000000),
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                'My Report',
                                                style: SafeGoogleFont(
                                                  'Open Sans',
                                                  fontSize: 14 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.3625 * ffem / fem,
                                                  color: Color(0xcc1b2b41),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 12 * fem,
                                ),
                                Container(
                                  padding: EdgeInsets.fromLTRB(
                                      24 * fem, 20 * fem, 24 * fem, 19.5 * fem),
                                  width: 226 * fem,
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffeaedf6)),
                                    color: Color(0xffffffff),
                                    borderRadius:
                                        BorderRadius.circular(4 * fem),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0x26d0d2da),
                                        offset: Offset(
                                            0 * fem, 12.6698102951 * fem),
                                        blurRadius: 11.262055397 * fem,
                                      ),
                                    ],
                                  ),
                                  child: Container(
                                    width: 115 * fem,
                                    height: double.infinity,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.fromLTRB(0 * fem,
                                              0 * fem, 16 * fem, 0.5 * fem),
                                          width: 62 * fem,
                                          height: 60.35 * fem,
                                          child: Image.asset(
                                            'assets/page-1/images/illustration.png',
                                            width: 62 * fem,
                                            height: 60.35 * fem,
                                          ),
                                        ),
                                        Container(
                                          height: double.infinity,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    0 * fem,
                                                    0 * fem,
                                                    0 * fem,
                                                    3.5 * fem),
                                                child: Text(
                                                  '24',
                                                  style: SafeGoogleFont(
                                                    'Open Sans',
                                                    fontSize: 32 * ffem,
                                                    fontWeight: FontWeight.w600,
                                                    height: 1.3625 * ffem / fem,
                                                    color: Color(0xff000000),
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                'Draft',
                                                style: SafeGoogleFont(
                                                  'Open Sans',
                                                  fontSize: 14 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.3625 * ffem / fem,
                                                  color: Color(0xcc1b2b41),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 12 * fem,
                                ),
                                Container(
                                  padding: EdgeInsets.fromLTRB(24 * fem,
                                      20 * fem, 46.77 * fem, 19.5 * fem),
                                  width: 226 * fem,
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffeaedf6)),
                                    color: Color(0xffffffff),
                                    borderRadius:
                                        BorderRadius.circular(4 * fem),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0x26d0d2da),
                                        offset: Offset(
                                            0 * fem, 12.6698102951 * fem),
                                        blurRadius: 11.262055397 * fem,
                                      ),
                                    ],
                                  ),
                                  child: Container(
                                    width: double.infinity,
                                    height: double.infinity,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.fromLTRB(0 * fem,
                                              0 * fem, 16 * fem, 0.5 * fem),
                                          width: 62.23 * fem,
                                          height: 60 * fem,
                                          child: Image.asset(
                                            'assets/page-1/images/not-found-illustration-G85.png',
                                            width: 62.23 * fem,
                                            height: 60 * fem,
                                          ),
                                        ),
                                        Container(
                                          height: double.infinity,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    0 * fem,
                                                    0 * fem,
                                                    0 * fem,
                                                    3.5 * fem),
                                                child: Text(
                                                  '5',
                                                  style: SafeGoogleFont(
                                                    'Open Sans',
                                                    fontSize: 32 * ffem,
                                                    fontWeight: FontWeight.w600,
                                                    height: 1.3625 * ffem / fem,
                                                    color: Color(0xff000000),
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                'Active Form',
                                                style: SafeGoogleFont(
                                                  'Open Sans',
                                                  fontSize: 14 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.3625 * ffem / fem,
                                                  color: Color(0xcc1b2b41),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin:
                    EdgeInsets.fromLTRB(16 * fem, 0 * fem, 16 * fem, 0 * fem),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 0 * fem, 0 * fem),
                      child: Text(
                        'Laporan Terakhir',
                        style: SafeGoogleFont(
                          'Open Sans',
                          fontSize: 16 * ffem,
                          fontWeight: FontWeight.w600,
                          height: 1.3000 * ffem / fem,
                          color: Color(0xff000000),
                        ),
                      ),
                    ),
                    Container(
                      height: 500,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Container(
                              child: Obx(() {
                                if (homeController.isLoading.value) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                } else {
                                  return ListView.builder(
                                    itemCount: homeController
                                            .customModel?.customData?.length ??
                                        0,
                                    itemBuilder: (context, index) {
                                      final data = homeController
                                          .customModel?.customData?[index];
                                      return Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                          side: BorderSide(
                                            color: Colors.grey,
                                            width: 1.0,
                                          ),
                                        ),
                                        margin:
                                            EdgeInsets.symmetric(vertical: 8.0),
                                        child: ListTile(
                                          title: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                data?.nama ?? 'no name',
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                              SizedBox(height: 3),
                                              Text(
                                                data?.deskripsi ??
                                                    'no description',
                                                style: TextStyle(
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xff9299a9),
                                                ),
                                              ),
                                              SizedBox(height: 3),
                                              Row(
                                                children: [
                                                  Text(
                                                    data?.tanggal ?? 'no date',
                                                    style: TextStyle(
                                                      fontSize: 12.0,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Color(0xff9299a9),
                                                    ),
                                                  ),
                                                  Text(
                                                    data?.jam ?? 'no time',
                                                    style: TextStyle(
                                                      fontSize: 12.0,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                }
                              }),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
