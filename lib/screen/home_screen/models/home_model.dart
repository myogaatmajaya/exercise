class CustomModel {
  List<CustomData>? customData;

  CustomModel({
    required this.customData,
  });

  factory CustomModel.fromJson(Map<String, dynamic>? json) {
    if (json != null && json['custom_data'] is List<dynamic>) {
      var list = json['custom_data'] as List<dynamic>;
      List<CustomData> dataList =
          list.map((e) => CustomData.fromJson(e)).toList();
      return CustomModel(customData: dataList);
    } else {
      return CustomModel(customData: []);
    }
  }
}

class CustomData {
  final String nama;
  final String deskripsi;
  final String tanggal;
  final String jam;

  CustomData({
    required this.nama,
    required this.deskripsi,
    required this.tanggal,
    required this.jam,
  });

  factory CustomData.fromJson(Map<String, dynamic> json) {
    return CustomData(
      nama: json['nama'] ?? '',
      deskripsi: json['deskripsi'] ?? '',
      tanggal: json['tanggal'] ?? '',
      jam: json['jam'] ?? '',
    );
  }
}
