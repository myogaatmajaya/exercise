import 'dart:convert';

import 'package:exercise/screen/home_screen/models/home_model.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class HomeController extends GetxController {
  var isLoading = false.obs;
  CustomModel? customModel;

  @override
  void onInit() {
    super.onInit();
    fetchData();
  }

  Future<void> fetchData() async {
    try {
      isLoading(true);
      http.Response response = await http
          .get(Uri.parse('https://api.npoint.io/613210febeef99adf26a'));
      if (response.statusCode == 200) {
        var result = jsonDecode(response.body);
        customModel = CustomModel.fromJson(result);

        // Output sementara
        print(customModel?.customData);
      } else {
        print('Error fetching data: ${response.statusCode}');
      }
    } catch (e) {
      print('Error while getting data: $e');
    } finally {
      isLoading(false);
    }
  }
}
