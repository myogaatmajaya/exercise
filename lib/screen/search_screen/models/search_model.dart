import '../../../core/app_export.dart';
import 'frame4_item_model.dart';
import 'userprofile4_item_model.dart';

class SearchModel {
  Rx<List<Frame4ItemModel>> frame4ItemList = Rx([
    Frame4ItemModel(frame: "Elektabilitas Paslon Kaltim".obs),
    Frame4ItemModel(frame: "Laporan Covid".obs),
    Frame4ItemModel(frame: "Laporan Covid".obs)
  ]);

  Rx<List<Userprofile4ItemModel>> userprofile4ItemList = Rx([
    Userprofile4ItemModel(
        userImage: ImageConstant.imgEllipse78.obs,
        username: "Brooklyn Simmons".obs,
        name: "Safarudin".obs,
        timestamp: "29 Maret 2021 14:30".obs),
    Userprofile4ItemModel(
        userImage: ImageConstant.imgEllipse7832x32.obs,
        username: "Kristin Watson".obs,
        name: "Safarudin".obs,
        timestamp: "29 Maret 2021 14:30".obs),
    Userprofile4ItemModel(
        userImage: ImageConstant.imgEllipse781.obs,
        username: "Bessie Cooper".obs,
        name: "Margareth".obs,
        timestamp: "29 Maret 2021 14:30".obs),
    Userprofile4ItemModel(
        userImage: ImageConstant.imgEllipse782.obs,
        username: "Theresa Webb".obs,
        name: "Margareth".obs,
        timestamp: "29 Maret 2021 14:30".obs),
    Userprofile4ItemModel(
        userImage: ImageConstant.imgEllipse783.obs,
        username: "Albert Flores".obs,
        name: "Safarudin".obs,
        timestamp: "29 Maret 2021 14:30".obs)
  ]);
}
