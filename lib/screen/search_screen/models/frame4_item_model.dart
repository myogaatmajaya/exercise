import '../../../core/app_export.dart';

class Frame4ItemModel {
  Frame4ItemModel({
    this.frame,
    this.id,
  }) {
    frame = frame ?? Rx("Elektabilitas Paslon Kaltim");
    id = id ?? Rx("");
  }

  Rx<String>? frame;

  Rx<String>? id;
}
