import '../../../core/app_export.dart';

class Userprofile4ItemModel {
  Userprofile4ItemModel({
    this.userImage,
    this.username,
    this.name,
    this.timestamp,
    this.id,
  }) {
    userImage = userImage ?? Rx(ImageConstant.imgEllipse78);
    username = username ?? Rx("Brooklyn Simmons");
    name = name ?? Rx("Safarudin");
    timestamp = timestamp ?? Rx("29 Maret 2021 14:30");
    id = id ?? Rx("");
  }

  Rx<String>? userImage;

  Rx<String>? username;

  Rx<String>? name;

  Rx<String>? timestamp;

  Rx<String>? id;
}
