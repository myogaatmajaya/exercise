import '../controller/search_controller.dart';
import '../models/userprofile4_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart' hide SearchController;

// ignore: must_be_immutable
class Userprofile4ItemWidget extends StatelessWidget {
  Userprofile4ItemWidget(
    this.userprofile4ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Userprofile4ItemModel userprofile4ItemModelObj;

  var controller = Get.find<SearchController>();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Obx(
          () => CustomImageView(
            imagePath: userprofile4ItemModelObj.userImage!.value,
            height: 32.adaptSize,
            width: 32.adaptSize,
            radius: BorderRadius.circular(
              16.h,
            ),
            margin: EdgeInsets.only(bottom: 8.v),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 12.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(
                () => Text(
                  userprofile4ItemModelObj.username!.value,
                  style: theme.textTheme.titleSmall,
                ),
              ),
              SizedBox(height: 1.v),
              Obx(
                () => Text(
                  userprofile4ItemModelObj.name!.value,
                  style: theme.textTheme.bodySmall,
                ),
              ),
            ],
          ),
        ),
        Spacer(),
        Padding(
          padding: EdgeInsets.only(top: 23.v),
          child: Obx(
            () => Text(
              userprofile4ItemModelObj.timestamp!.value,
              style: theme.textTheme.bodySmall,
            ),
          ),
        ),
      ],
    );
  }
}
