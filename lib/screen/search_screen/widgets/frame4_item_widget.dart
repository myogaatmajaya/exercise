import '../controller/search_controller.dart';
import '../models/frame4_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart' hide SearchController;

// ignore: must_be_immutable
class Frame4ItemWidget extends StatelessWidget {
  Frame4ItemWidget(
    this.frame4ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Frame4ItemModel frame4ItemModelObj;

  var controller = Get.find<SearchController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 195.h,
      padding: EdgeInsets.symmetric(
        horizontal: 12.h,
        vertical: 5.v,
      ),
      decoration: AppDecoration.outlinePrimary.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder6,
      ),
      child: Obx(
        () => Text(
          frame4ItemModelObj.frame!.value,
          style: CustomTextStyles.bodyMediumPrimary_1,
        ),
      ),
    );
  }
}
