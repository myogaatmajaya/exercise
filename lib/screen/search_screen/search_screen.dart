import 'widgets/frame4_item_widget.dart';
import 'widgets/userprofile4_item_widget.dart';
import 'controller/search_controller.dart';
import 'models/frame4_item_model.dart';
import 'models/userprofile4_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/appbar_leading_image.dart';
import 'package:exercise/widgets/app_bar/appbar_title_image.dart';
import 'package:exercise/widgets/app_bar/appbar_title_searchview_one.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:flutter/material.dart' hide SearchController;

class SearchScreen extends GetWidget<SearchController> {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            appBar: _buildAppBar(),
            body: Container(
                width: double.maxFinite,
                padding: EdgeInsets.symmetric(vertical: 21.v),
                child: Column(children: [
                  _buildFilterTerpasang(),
                  SizedBox(height: 54.v),
                  Container(
                      height: 10.v,
                      width: double.maxFinite,
                      decoration: BoxDecoration(color: appTheme.gray50)),
                  SizedBox(height: 19.v),
                  _buildUserProfile(),
                  SizedBox(height: 5.v)
                ]))));
  }

  /// Section Widget
  PreferredSizeWidget _buildAppBar() {
    return CustomAppBar(
        height: 54.v,
        leadingWidth: 44.h,
        leading: AppbarLeadingImage(
            imagePath: ImageConstant.imgArrowLeftBlack900,
            margin: EdgeInsets.only(left: 16.h, top: 16.v, bottom: 10.v),
            onTap: () {
              onTapArrowLeft();
            }),
        title: Padding(
            padding: EdgeInsets.only(left: 11.h),
            child: Column(children: [
              AppbarTitleImage(
                  imagePath: ImageConstant.imgTelevision,
                  margin: EdgeInsets.only(left: 271.h, right: 24.h)),
              SizedBox(height: 7.v),
              AppbarTitleSearchviewOne(
                  hintText: "lbl_search".tr,
                  controller: controller.searchController)
            ])));
  }

  /// Section Widget
  Widget _buildFilterTerpasang() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(
          padding: EdgeInsets.only(left: 16.h),
          child: Text("msg_filter_terpasang".tr,
              style: theme.textTheme.titleSmall)),
      SizedBox(height: 9.v),
      SizedBox(
          height: 31.v,
          child: Obx(() => ListView.separated(
              padding: EdgeInsets.only(left: 16.h),
              scrollDirection: Axis.horizontal,
              separatorBuilder: (context, index) {
                return SizedBox(width: 4.h);
              },
              itemCount:
                  controller.searchModelObj.value.frame4ItemList.value.length,
              itemBuilder: (context, index) {
                Frame4ItemModel model =
                    controller.searchModelObj.value.frame4ItemList.value[index];
                return Frame4ItemWidget(model);
              })))
    ]);
  }

  /// Section Widget
  Widget _buildUserProfile() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 16.h),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text("lbl_hasil_pencarian".tr, style: theme.textTheme.titleSmall),
          SizedBox(height: 15.v),
          Obx(() => ListView.separated(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              separatorBuilder: (context, index) {
                return Padding(
                    padding: EdgeInsets.symmetric(vertical: 7.0.v),
                    child: SizedBox(
                        width: 343.h,
                        child: Divider(
                            height: 1.v,
                            thickness: 1.v,
                            color: appTheme.indigo50)));
              },
              itemCount: controller
                  .searchModelObj.value.userprofile4ItemList.value.length,
              itemBuilder: (context, index) {
                Userprofile4ItemModel model = controller
                    .searchModelObj.value.userprofile4ItemList.value[index];
                return Userprofile4ItemWidget(model);
              }))
        ]));
  }

  /// Navigates to the previous screen.
  onTapArrowLeft() {
    Get.back();
  }
}
