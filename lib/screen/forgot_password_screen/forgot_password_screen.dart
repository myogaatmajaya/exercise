import 'controller/forgot_password_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/appbar_leading_image.dart';
import 'package:exercise/widgets/app_bar/appbar_trailing_button.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:exercise/widgets/custom_text_form_field.dart';
import 'package:flutter/material.dart';

class ForgotPasswordScreen extends GetWidget<ForgotPasswordController> {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: _buildAppBar(),
            body: Container(
                width: double.maxFinite,
                padding: EdgeInsets.symmetric(horizontal: 19.h, vertical: 57.v),
                child: Column(children: [
                  CustomImageView(
                      imagePath: ImageConstant.imgUndrawForgotP,
                      height: 150.v,
                      width: 225.h),
                  SizedBox(height: 32.v),
                  Text("lbl_lupa_sandi".tr, style: theme.textTheme.titleLarge),
                  SizedBox(height: 5.v),
                  SizedBox(
                      width: 233.h,
                      child: Text("msg_kode_konfirmasi".tr,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: CustomTextStyles.bodyMediumBluegray900d3
                              .copyWith(height: 1.50))),
                  SizedBox(height: 19.v),
                  _buildEmailAddress(),
                  SizedBox(height: 20.v),
                  CustomElevatedButton(height: 56.v, text: "lbl_kirim".tr),
                  Spacer(),
                  Text("msg_kembali_ke_masuk".tr,
                      style: CustomTextStyles.labelLarge13)
                ]))));
  }

  /// Section Widget
  PreferredSizeWidget _buildAppBar() {
    return CustomAppBar(
        height: 58.v,
        leadingWidth: 44.h,
        leading: AppbarLeadingImage(
            imagePath: ImageConstant.imgArrowLeftBlack900,
            margin: EdgeInsets.only(left: 16.h, top: 14.v, bottom: 14.v),
            onTap: () {
              onTapArrowLeft();
            }),
        actions: [
          AppbarTrailingButton(
              margin: EdgeInsets.symmetric(horizontal: 16.h, vertical: 15.v))
        ]);
  }

  /// Section Widget
  Widget _buildEmailAddress() {
    return Padding(
        padding: EdgeInsets.only(left: 1.h),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: EdgeInsets.only(left: 1.h),
              child: RichText(
                  text: TextSpan(children: [
                    TextSpan(
                        text: "lbl_email2".tr,
                        style: CustomTextStyles.labelLargeBluegray900),
                    TextSpan(
                        text: "lbl".tr,
                        style: CustomTextStyles.labelLargeRed500)
                  ]),
                  textAlign: TextAlign.left)),
          SizedBox(height: 3.v),
          CustomTextFormField(
              controller: controller.emailController,
              textInputAction: TextInputAction.done,
              borderDecoration: TextFormFieldStyleHelper.outlineBlueGray,
              filled: false)
        ]));
  }

  /// Navigates to the previous screen.
  onTapArrowLeft() {
    Get.back();
  }
}
