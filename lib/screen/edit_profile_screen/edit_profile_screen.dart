import 'controller/edit_profile_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/core/utils/validation_functions.dart';
import 'package:exercise/widgets/app_bar/appbar_title.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:exercise/widgets/custom_drop_down.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:exercise/widgets/custom_text_form_field.dart';
import 'package:flutter/material.dart';

// ignore_for_file: must_be_immutable
class EditProfileScreen extends GetWidget<EditProfileController> {
  EditProfileScreen({Key? key}) : super(key: key);

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: Form(
                key: _formKey,
                child: SizedBox(
                    width: double.maxFinite,
                    child: Column(children: [
                      _buildArrowLeftStack(),
                      Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.h, vertical: 2.v),
                          child: Column(children: [
                            Padding(
                                padding: EdgeInsets.only(right: 153.h),
                                child: _buildTelevisionStack()),
                            SizedBox(height: 3.v),
                            SizedBox(
                                height: 616.v,
                                width: 343.h,
                                child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Align(
                                          alignment: Alignment.topRight,
                                          child: Padding(
                                              padding: EdgeInsets.only(
                                                  top: 16.v, right: 25.h),
                                              child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(children: [
                                                      Container(
                                                          decoration: AppDecoration
                                                              .outlineWhiteA
                                                              .copyWith(
                                                                  borderRadius:
                                                                      BorderRadiusStyle
                                                                          .roundedBorder6),
                                                          child: Container(
                                                              height:
                                                                  9.adaptSize,
                                                              width:
                                                                  9.adaptSize,
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius.circular(
                                                                          4.h),
                                                                  border: Border.all(
                                                                      color: appTheme
                                                                          .whiteA700
                                                                          .withOpacity(
                                                                              0.2),
                                                                      width: 2
                                                                          .h)))),
                                                      Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 160.h),
                                                          decoration: AppDecoration
                                                              .outlineWhiteA
                                                              .copyWith(
                                                                  borderRadius:
                                                                      BorderRadiusStyle
                                                                          .roundedBorder6),
                                                          child: Container(
                                                              height:
                                                                  9.adaptSize,
                                                              width:
                                                                  9.adaptSize,
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius.circular(
                                                                          4.h),
                                                                  border: Border.all(
                                                                      color: appTheme
                                                                          .whiteA700
                                                                          .withOpacity(
                                                                              0.2),
                                                                      width: 2.h))))
                                                    ]),
                                                    SizedBox(height: 30.v),
                                                    Align(
                                                        alignment: Alignment
                                                            .centerRight,
                                                        child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            children: [
                                                              Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          bottom: 4
                                                                              .v),
                                                                  child:
                                                                      _buildTelevisionStack()),
                                                              Container(
                                                                  margin: EdgeInsets.only(
                                                                      left: 158
                                                                          .h),
                                                                  decoration: AppDecoration
                                                                      .outlineWhiteA
                                                                      .copyWith(
                                                                          borderRadius: BorderRadiusStyle
                                                                              .roundedBorder6),
                                                                  child: Container(
                                                                      height: 15
                                                                          .adaptSize,
                                                                      width: 15
                                                                          .adaptSize,
                                                                      decoration: BoxDecoration(
                                                                          borderRadius: BorderRadius.circular(7
                                                                              .h),
                                                                          border: Border.all(
                                                                              color: appTheme.whiteA700.withOpacity(0.2),
                                                                              width: 2.h))))
                                                            ]))
                                                  ]))),
                                      Align(
                                          alignment: Alignment.center,
                                          child: SingleChildScrollView(
                                            child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  CustomImageView(
                                                      imagePath: ImageConstant
                                                          .imgRectangle3009,
                                                      height: 90.adaptSize,
                                                      width: 90.adaptSize,
                                                      radius:
                                                          BorderRadius.circular(
                                                              8.h)),
                                                  SizedBox(height: 16.v),
                                                  CustomElevatedButton(
                                                      height: 35.v,
                                                      width: 118.h,
                                                      text: "lbl_edit_photo".tr,
                                                      buttonTextStyle:
                                                          CustomTextStyles
                                                              .titleSmallWhiteA700),
                                                  SizedBox(height: 20.v),
                                                  _buildNameFrame(),
                                                  SizedBox(height: 14.v),
                                                  _buildEmailFrame(),
                                                  SizedBox(height: 14.v),
                                                  _buildNikFrame(),
                                                  SizedBox(height: 14.v),
                                                  _buildGenderFrame(),
                                                  SizedBox(height: 14.v),
                                                  _buildAddressFrame()
                                                ]),
                                          ))
                                    ])),
                            SizedBox(height: 5.v)
                          ]))
                    ]))),
            bottomNavigationBar: _buildSaveChangesButton()));
  }

  /// Section Widget
  Widget _buildArrowLeftStack() {
    return SizedBox(
        height: 47.v,
        width: double.maxFinite,
        child: Stack(alignment: Alignment.topCenter, children: [
          Align(
              alignment: Alignment.center,
              child: Container(
                  width: double.maxFinite,
                  padding: EdgeInsets.fromLTRB(16.h, 11.v, 16.h, 10.v),
                  decoration: AppDecoration.outlineIndigo502,
                  child: Row(children: [
                    CustomImageView(
                        imagePath: ImageConstant.imgArrowLeft,
                        height: 24.adaptSize,
                        width: 24.adaptSize,
                        margin: EdgeInsets.only(top: 1.v),
                        onTap: () {
                          onTapImgArrowLeft();
                        }),
                    Padding(
                        padding: EdgeInsets.only(left: 68.h, bottom: 4.v),
                        child: Text("msg_account_information2".tr,
                            style: CustomTextStyles.titleMediumInterBlack900))
                  ]))),
          CustomAppBar(
              leadingWidth: 44.h,
              leading: Container(
                  height: 28.adaptSize,
                  width: 28.adaptSize,
                  margin: EdgeInsets.only(left: 16.h, top: 16.v),
                  child: Stack(children: [
                    CustomImageView(
                        imagePath: ImageConstant.imgArrowLeftWhiteA700,
                        height: 28.adaptSize,
                        width: 28.adaptSize,
                        alignment: Alignment.center,
                        onTap: () {
                          onTapImgArrowLeft1();
                        }),
                    SizedBox(
                        height: 28.adaptSize,
                        width: 28.adaptSize,
                        child: Stack(alignment: Alignment.center, children: [
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              alignment: Alignment.center),
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              alignment: Alignment.center)
                        ]))
                  ])),
              centerTitle: true,
              title: AppbarTitle(text: "lbl_form".tr),
              actions: [
                Container(
                    height: 7.v,
                    width: 9.h,
                    margin:
                        EdgeInsets.only(left: 40.h, right: 40.h, bottom: 37.v),
                    child: Stack(alignment: Alignment.center, children: [
                      CustomImageView(
                          imagePath: ImageConstant.imgTelevision,
                          height: 7.v,
                          width: 9.h,
                          alignment: Alignment.center),
                      CustomImageView(
                          imagePath: ImageConstant.imgTelevision,
                          height: 7.v,
                          width: 9.h,
                          alignment: Alignment.center)
                    ]))
              ])
        ]));
  }

  /// Section Widget
  Widget _buildNameFrame() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      RichText(
          text: TextSpan(children: [
            TextSpan(
                text: "lbl_name2".tr, style: CustomTextStyles.titleSmallInter),
            TextSpan(
                text: "lbl".tr, style: CustomTextStyles.titleSmallInterPinkA200)
          ]),
          textAlign: TextAlign.left),
      SizedBox(height: 11.v),
      CustomTextFormField(
          controller: controller.nameController,
          hintText: "msg_andri_yulianto_rosadi".tr,
          borderDecoration: TextFormFieldStyleHelper.outlineGray,
          filled: false)
    ]);
  }

  /// Section Widget
  Widget _buildEmailFrame() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      RichText(
          text: TextSpan(children: [
            TextSpan(
                text: "lbl_email2".tr, style: CustomTextStyles.titleSmallInter),
            TextSpan(
                text: "lbl".tr, style: CustomTextStyles.titleSmallInterPinkA200)
          ]),
          textAlign: TextAlign.left),
      SizedBox(height: 11.v),
      CustomTextFormField(
          controller: controller.emailController,
          hintText: "msg_y_andri_r07_gmail_com".tr,
          hintStyle: CustomTextStyles.bodyMediumInterBlack900,
          textInputType: TextInputType.emailAddress,
          validator: (value) {
            if (value == null || (!isValidEmail(value, isRequired: true))) {
              return "err_msg_please_enter_valid_email".tr;
            }
            return null;
          },
          borderDecoration: TextFormFieldStyleHelper.outlineGrayTL4,
          fillColor: appTheme.gray50)
    ]);
  }

  /// Section Widget
  Widget _buildNikFrame() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      RichText(
          text: TextSpan(children: [
            TextSpan(
                text: "lbl_nik2".tr, style: CustomTextStyles.titleSmallInter),
            TextSpan(
                text: "lbl".tr, style: CustomTextStyles.titleSmallInterPinkA200)
          ]),
          textAlign: TextAlign.left),
      SizedBox(height: 11.v),
      CustomTextFormField(
          controller: controller.valueController,
          hintText: "msg_3172321433431321".tr,
          borderDecoration: TextFormFieldStyleHelper.outlineGray,
          filled: false)
    ]);
  }

  /// Section Widget
  Widget _buildGenderFrame() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text("lbl_gender".tr, style: CustomTextStyles.titleSmallInterMedium),
      SizedBox(height: 11.v),
      CustomDropDown(
          icon: Container(
              margin: EdgeInsets.fromLTRB(30.h, 12.v, 12.h, 12.v),
              child: CustomImageView(
                  imagePath: ImageConstant.imgArrowdown,
                  height: 16.adaptSize,
                  width: 16.adaptSize)),
          hintText: "lbl_male".tr,
          items: controller.editProfileModelObj.value.dropdownItemList!.value,
          onChanged: (value) {
            controller.onSelected(value);
          })
    ]);
  }

  /// Section Widget
  Widget _buildAddressFrame() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text("lbl_address".tr, style: CustomTextStyles.titleSmallInterMedium),
      SizedBox(height: 11.v),
      CustomTextFormField(
          controller: controller.addressController,
          hintText: "msg_jalan_pasar_minggu".tr,
          textInputAction: TextInputAction.done,
          maxLines: 5,
          borderDecoration: TextFormFieldStyleHelper.outlineGray,
          filled: false)
    ]);
  }

  /// Section Widget
  Widget _buildSaveChangesButton() {
    return CustomElevatedButton(
        text: "lbl_save_changes".tr,
        margin: EdgeInsets.only(left: 16.h, right: 16.h, bottom: 21.v),
        buttonStyle: CustomButtonStyles.fillPrimaryTL8,
        buttonTextStyle: CustomTextStyles.titleSmallInterWhiteA700);
  }

  /// Common widget
  Widget _buildTelevisionStack() {
    return SizedBox(
        height: 11.v,
        width: 9.h,
        child: Stack(alignment: Alignment.center, children: [
          CustomImageView(
              imagePath: ImageConstant.imgTelevision,
              height: 11.v,
              width: 9.h,
              alignment: Alignment.center),
          CustomImageView(
              imagePath: ImageConstant.imgTelevision,
              height: 11.v,
              width: 9.h,
              alignment: Alignment.center)
        ]));
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft() {
    Get.back();
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft1() {
    Get.back();
  }
}
