import 'controller/sent_code_password_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/appbar_leading_image.dart';
import 'package:exercise/widgets/app_bar/appbar_trailing_button.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:exercise/widgets/custom_pin_code_text_field.dart';
import 'package:flutter/material.dart';

class SentCodePasswordScreen extends GetWidget<SentCodePasswordController> {
  const SentCodePasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: _buildAppBar(),
            body: Container(
                width: double.maxFinite,
                padding: EdgeInsets.symmetric(horizontal: 19.h, vertical: 40.v),
                child: Column(children: [
                  CustomImageView(
                      imagePath: ImageConstant.imgGroup2145,
                      height: 231.v,
                      width: 221.h),
                  SizedBox(height: 43.v),
                  Text("lbl_kode_verifikasi".tr,
                      style: theme.textTheme.titleLarge),
                  SizedBox(height: 8.v),
                  SizedBox(
                      width: 197.h,
                      child: Text("msg_silakan_input_token".tr,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: CustomTextStyles.bodyMediumBluegray900d3
                              .copyWith(height: 1.50))),
                  SizedBox(height: 8.v),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 73.h),
                      child: Obx(() => CustomPinCodeTextField(
                          context: Get.context!,
                          controller: controller.otpController.value,
                          onChanged: (value) {}))),
                  SizedBox(height: 32.v),
                  CustomElevatedButton(height: 56.v, text: "lbl_lanjut".tr),
                  Spacer(),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Padding(
                        padding: EdgeInsets.only(top: 1.v),
                        child: Text("msg_sudah_punya_akun".tr,
                            style: CustomTextStyles.bodyMedium13)),
                    Padding(
                        padding: EdgeInsets.only(left: 2.h),
                        child: Text("lbl_masuk".tr,
                            style: CustomTextStyles.labelLarge13))
                  ])
                ]))));
  }

  /// Section Widget
  PreferredSizeWidget _buildAppBar() {
    return CustomAppBar(
        height: 58.v,
        leadingWidth: 44.h,
        leading: AppbarLeadingImage(
            imagePath: ImageConstant.imgArrowLeftBlack900,
            margin: EdgeInsets.only(left: 16.h, top: 14.v, bottom: 14.v),
            onTap: () {
              onTapArrowLeft();
            }),
        actions: [
          AppbarTrailingButton(
              margin: EdgeInsets.symmetric(horizontal: 16.h, vertical: 15.v))
        ]);
  }

  /// Navigates to the previous screen.
  onTapArrowLeft() {
    Get.back();
  }
}
