import '../controller/sent_code_password_controller.dart';
import 'package:get/get.dart';

class SentCodePasswordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SentCodePasswordController());
  }
}
