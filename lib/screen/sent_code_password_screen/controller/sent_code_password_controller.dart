import 'package:exercise/core/app_export.dart';
import 'package:exercise/screen/sent_code_password_screen/models/sent_code_password_model.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:flutter/material.dart';

class SentCodePasswordController extends GetxController with CodeAutoFill {
  Rx<TextEditingController> otpController = TextEditingController().obs;

  Rx<SentCodePasswordModel> sentCodePasswordModelObj =
      SentCodePasswordModel().obs;

  @override
  void codeUpdated() {
    otpController.value.text = code ?? '';
  }

  @override
  void onInit() {
    super.onInit();
    listenForCode();
  }
}
