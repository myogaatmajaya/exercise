import 'package:exercise/core/app_export.dart';
import 'package:exercise/screen/hasil_pencarian_filter_bottomsheet/models/hasil_pencarian_filter_model.dart';

class HasilPencarianFilterController extends GetxController {
  Rx<HasilPencarianFilterModel> hasilPencarianFilterModelObj =
      HasilPencarianFilterModel().obs;

  Rx<bool> laporanSayaCheckBox = false.obs;
}
