import 'widgets/elektabilitaspaslonkaltim2_item_widget.dart';
import 'widgets/frame2_item_widget.dart';
import 'controller/hasil_pencarian_filter_controller.dart';
import 'models/elektabilitaspaslonkaltim2_item_model.dart';
import 'models/frame2_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/custom_checkbox_button.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:exercise/widgets/custom_icon_button.dart';
import 'package:flutter/material.dart';

class HasilPencarianFilterBottomsheet extends StatelessWidget {
  HasilPencarianFilterBottomsheet(
    this.controller, {
    Key? key,
  }) : super(
          key: key,
        );

  HasilPencarianFilterController controller;

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return Container(
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(
        horizontal: 5.h,
        vertical: 21.v,
      ),
      decoration: AppDecoration.fillWhiteA.copyWith(
        borderRadius: BorderRadiusStyle.customBorderTL34,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(height: 19.v),
          _buildDescriptionRow(),
          SizedBox(height: 17.v),
          _buildTugasColumn(),
          SizedBox(height: 21.v),
          _buildTanggalColumn(),
          SizedBox(height: 13.v),
          _buildFrameRow(),
          SizedBox(height: 13.v),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(left: 9.h),
              child: Text(
                "lbl_kepemilikan".tr,
                style: theme.textTheme.titleSmall,
              ),
            ),
          ),
          SizedBox(height: 9.v),
          _buildLaporanSayaCheckBox(),
          SizedBox(height: 17.v),
          CustomElevatedButton(
            text: "lbl_tampilkan".tr,
            margin: EdgeInsets.only(
              left: 15.h,
              right: 14.h,
            ),
          ),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildDescriptionRow() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 9.h),
      padding: EdgeInsets.symmetric(
        horizontal: 16.h,
        vertical: 7.v,
      ),
      decoration: AppDecoration.fillGray.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder6,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 6.v),
            child: CustomIconButton(
              height: 32.adaptSize,
              width: 32.adaptSize,
              padding: EdgeInsets.all(8.h),
              decoration: IconButtonStyleHelper.fillPrimary,
              child: CustomImageView(
                imagePath: ImageConstant.imgAntDesignInfoCircleFilled,
              ),
            ),
          ),
          Expanded(
            child: Container(
              width: 262.h,
              margin: EdgeInsets.only(
                left: 12.h,
                top: 3.v,
                right: 8.h,
              ),
              child: Text(
                "msg_isi_informasi_basik".tr,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: CustomTextStyles.bodyMediumBluegray300,
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildTugasColumn() {
    return Padding(
      padding: EdgeInsets.only(left: 9.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "lbl_tugas".tr,
            style: theme.textTheme.titleSmall,
          ),
          SizedBox(height: 9.v),
          Obx(
            () => Wrap(
              runSpacing: 12.v,
              spacing: 12.h,
              children: List<Widget>.generate(
                controller.hasilPencarianFilterModelObj.value
                    .elektabilitaspaslonkaltim2ItemList.value.length,
                (index) {
                  Elektabilitaspaslonkaltim2ItemModel model = controller
                      .hasilPencarianFilterModelObj
                      .value
                      .elektabilitaspaslonkaltim2ItemList
                      .value[index];

                  return Elektabilitaspaslonkaltim2ItemWidget(
                    model,
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildTanggalColumn() {
    return Padding(
      padding: EdgeInsets.only(left: 9.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "lbl_tanggal".tr,
            style: theme.textTheme.titleSmall,
          ),
          SizedBox(height: 7.v),
          Obx(
            () => Wrap(
              runSpacing: 8.v,
              spacing: 8.h,
              children: List<Widget>.generate(
                controller.hasilPencarianFilterModelObj.value.frame2ItemList
                    .value.length,
                (index) {
                  Frame2ItemModel model = controller
                      .hasilPencarianFilterModelObj
                      .value
                      .frame2ItemList
                      .value[index];

                  return Frame2ItemWidget(
                    model,
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildFrameRow() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 9.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildSampaiColumn(
            dynamicText: "lbl_tanggal2".tr,
          ),
          Padding(
            padding: EdgeInsets.only(left: 7.h),
            child: _buildSampaiColumn(
              dynamicText: "lbl_sampai".tr,
            ),
          ),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildLaporanSayaCheckBox() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.only(left: 9.h),
        child: Obx(
          () => CustomCheckboxButton(
            alignment: Alignment.centerLeft,
            text: "lbl_laporan_saya".tr,
            value: controller.laporanSayaCheckBox.value,
            onChange: (value) {
              controller.laporanSayaCheckBox.value = value;
            },
          ),
        ),
      ),
    );
  }

  /// Common widget
  Widget _buildSampaiColumn({required String dynamicText}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: "lbl_sampai2".tr,
                style: CustomTextStyles.labelLargeBluegray900,
              ),
              TextSpan(
                text: "lbl".tr,
                style: CustomTextStyles.labelLargeRed500,
              ),
            ],
          ),
          textAlign: TextAlign.left,
        ),
        SizedBox(height: 6.v),
        Container(
          height: 40.v,
          width: 170.h,
          padding: EdgeInsets.symmetric(
            horizontal: 10.h,
            vertical: 11.v,
          ),
          decoration: AppDecoration.outlineBlueGray.copyWith(
            borderRadius: BorderRadiusStyle.roundedBorder3,
          ),
          child: CustomImageView(
            imagePath: ImageConstant.imgFrame,
            height: 16.adaptSize,
            width: 16.adaptSize,
            alignment: Alignment.centerRight,
          ),
        ),
      ],
    );
  }
}
