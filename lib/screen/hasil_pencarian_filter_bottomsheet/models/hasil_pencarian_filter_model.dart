import 'elektabilitaspaslonkaltim2_item_model.dart';
import 'frame2_item_model.dart';
import '../../../core/app_export.dart';

class HasilPencarianFilterModel {
  Rx<List<Elektabilitaspaslonkaltim2ItemModel>>
      elektabilitaspaslonkaltim2ItemList =
      Rx(List.generate(3, (index) => Elektabilitaspaslonkaltim2ItemModel()));

  Rx<List<Frame2ItemModel>> frame2ItemList =
      Rx(List.generate(4, (index) => Frame2ItemModel()));
}
