import '../models/frame2_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Frame2ItemWidget extends StatelessWidget {
  Frame2ItemWidget(
    this.frame2ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Frame2ItemModel frame2ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => RawChip(
        padding: EdgeInsets.symmetric(
          horizontal: 12.h,
          vertical: 5.v,
        ),
        showCheckmark: false,
        labelPadding: EdgeInsets.zero,
        label: Text(
          frame2ItemModelObj.hariIni!.value,
          style: TextStyle(
            color: (frame2ItemModelObj.isSelected?.value ?? false)
                ? theme.colorScheme.primary
                : appTheme.blueGray300,
            fontSize: 14.fSize,
            fontFamily: 'Open Sans',
            fontWeight: FontWeight.w400,
          ),
        ),
        selected: (frame2ItemModelObj.isSelected?.value ?? false),
        backgroundColor: appTheme.gray50,
        selectedColor: theme.colorScheme.primary.withOpacity(0.1),
        shape: (frame2ItemModelObj.isSelected?.value ?? false)
            ? RoundedRectangleBorder(
                side: BorderSide(
                  color: theme.colorScheme.primary,
                  width: 1.h,
                ),
                borderRadius: BorderRadius.circular(
                  6.h,
                ),
              )
            : RoundedRectangleBorder(
                side: BorderSide.none,
                borderRadius: BorderRadius.circular(
                  6.h,
                ),
              ),
        onSelected: (value) {
          frame2ItemModelObj.isSelected!.value = value;
        },
      ),
    );
  }
}
