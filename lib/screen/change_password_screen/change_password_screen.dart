import 'controller/change_password_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/core/utils/validation_functions.dart';
import 'package:exercise/widgets/app_bar/appbar_title.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:exercise/widgets/custom_text_form_field.dart';
import 'package:flutter/material.dart';

// ignore_for_file: must_be_immutable
class ChangePasswordScreen extends GetWidget<ChangePasswordController> {
  ChangePasswordScreen({Key? key}) : super(key: key);

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: Form(
                key: _formKey,
                child: SizedBox(
                    width: double.maxFinite,
                    child: Column(children: [
                      _buildArrowLeftStack(),
                      Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.h, vertical: 2.v),
                          child: Column(children: [
                            Padding(
                                padding: EdgeInsets.only(right: 153.h),
                                child: _buildTelevisionStack()),
                            SizedBox(height: 5.v),
                            _buildPasswordStack(),
                            SizedBox(height: 5.v)
                          ]))
                    ]))),
            bottomNavigationBar: _buildSaveChangesButton()));
  }

  /// Section Widget
  Widget _buildArrowLeftStack() {
    return SizedBox(
        height: 47.v,
        width: double.maxFinite,
        child: Stack(alignment: Alignment.topCenter, children: [
          Align(
              alignment: Alignment.center,
              child: Container(
                  width: double.maxFinite,
                  padding: EdgeInsets.fromLTRB(16.h, 11.v, 16.h, 10.v),
                  decoration: AppDecoration.outlineIndigo502,
                  child: Row(children: [
                    CustomImageView(
                        imagePath: ImageConstant.imgArrowLeft,
                        height: 24.adaptSize,
                        width: 24.adaptSize,
                        margin: EdgeInsets.only(top: 1.v),
                        onTap: () {
                          onTapImgArrowLeft();
                        }),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 78.h, top: 1.v, bottom: 2.v),
                        child: Text("lbl_change_password".tr,
                            style: CustomTextStyles.titleMediumInterBlack900))
                  ]))),
          CustomAppBar(
              leadingWidth: 44.h,
              leading: Container(
                  height: 28.adaptSize,
                  width: 28.adaptSize,
                  margin: EdgeInsets.only(left: 16.h, top: 16.v),
                  child: Stack(children: [
                    CustomImageView(
                        imagePath: ImageConstant.imgArrowLeftWhiteA700,
                        height: 28.adaptSize,
                        width: 28.adaptSize,
                        alignment: Alignment.center,
                        onTap: () {
                          onTapImgArrowLeft1();
                        }),
                    SizedBox(
                        height: 28.adaptSize,
                        width: 28.adaptSize,
                        child: Stack(alignment: Alignment.center, children: [
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              alignment: Alignment.center),
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              alignment: Alignment.center)
                        ]))
                  ])),
              centerTitle: true,
              title: AppbarTitle(text: "lbl_form".tr),
              actions: [
                Container(
                    height: 7.v,
                    width: 9.h,
                    margin:
                        EdgeInsets.only(left: 40.h, right: 40.h, bottom: 37.v),
                    child: Stack(alignment: Alignment.center, children: [
                      CustomImageView(
                          imagePath: ImageConstant.imgTelevision,
                          height: 7.v,
                          width: 9.h,
                          alignment: Alignment.center),
                      CustomImageView(
                          imagePath: ImageConstant.imgTelevision,
                          height: 7.v,
                          width: 9.h,
                          alignment: Alignment.center)
                    ]))
              ])
        ]));
  }

  /// Section Widget
  Widget _buildCurrentPasswordColumn() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text("msg_current_password".tr,
          style: CustomTextStyles.titleSmallInterMedium),
      SizedBox(height: 11.v),
      CustomTextFormField(
          controller: controller.passwordController,
          hintText: "lbl_ask_me_anything".tr,
          textInputType: TextInputType.visiblePassword,
          validator: (value) {
            if (value == null || (!isValidPassword(value, isRequired: true))) {
              return "err_msg_please_enter_valid_password".tr;
            }
            return null;
          },
          obscureText: true)
    ]);
  }

  /// Section Widget
  Widget _buildNewPasswordColumn() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text("lbl_new_password".tr,
          style: CustomTextStyles.titleSmallInterMedium),
      SizedBox(height: 11.v),
      CustomTextFormField(
          controller: controller.newpasswordController,
          hintText: "lbl_ask_me_anything".tr,
          textInputType: TextInputType.visiblePassword,
          validator: (value) {
            if (value == null || (!isValidPassword(value, isRequired: true))) {
              return "err_msg_please_enter_valid_password".tr;
            }
            return null;
          },
          obscureText: true)
    ]);
  }

  /// Section Widget
  Widget _buildConfirmPasswordColumn() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text("msg_confirm_password".tr,
          style: CustomTextStyles.titleSmallInterMedium),
      SizedBox(height: 11.v),
      CustomTextFormField(
          controller: controller.confirmpasswordController,
          hintText: "lbl_ask_me_anything".tr,
          textInputAction: TextInputAction.done,
          textInputType: TextInputType.visiblePassword,
          validator: (value) {
            if (value == null || (!isValidPassword(value, isRequired: true))) {
              return "err_msg_please_enter_valid_password".tr;
            }
            return null;
          },
          obscureText: true)
    ]);
  }

  /// Section Widget
  Widget _buildPasswordStack() {
    return SizedBox(
        height: 242.v,
        width: 343.h,
        child: Stack(alignment: Alignment.center, children: [
          Align(
              alignment: Alignment.topRight,
              child: Padding(
                  padding: EdgeInsets.only(top: 13.v, right: 25.h),
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(children: [
                          Container(
                              decoration: AppDecoration.outlineWhiteA.copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.roundedBorder6),
                              child: Container(
                                  height: 9.adaptSize,
                                  width: 9.adaptSize,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.h),
                                      border: Border.all(
                                          color: appTheme.whiteA700
                                              .withOpacity(0.2),
                                          width: 2.h)))),
                          Container(
                              margin: EdgeInsets.only(left: 160.h),
                              decoration: AppDecoration.outlineWhiteA.copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.roundedBorder6),
                              child: Container(
                                  height: 9.adaptSize,
                                  width: 9.adaptSize,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.h),
                                      border: Border.all(
                                          color: appTheme.whiteA700
                                              .withOpacity(0.2),
                                          width: 2.h))))
                        ]),
                        SizedBox(height: 30.v),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Padding(
                                      padding: EdgeInsets.only(bottom: 4.v),
                                      child: _buildTelevisionStack()),
                                  Container(
                                      margin: EdgeInsets.only(left: 158.h),
                                      decoration: AppDecoration.outlineWhiteA
                                          .copyWith(
                                              borderRadius: BorderRadiusStyle
                                                  .roundedBorder6),
                                      child: Container(
                                          height: 15.adaptSize,
                                          width: 15.adaptSize,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(7.h),
                                              border: Border.all(
                                                  color: appTheme.whiteA700
                                                      .withOpacity(0.2),
                                                  width: 2.h))))
                                ]))
                      ]))),
          Align(
              alignment: Alignment.center,
              child: SingleChildScrollView(
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  _buildCurrentPasswordColumn(),
                  SizedBox(height: 18.v),
                  _buildNewPasswordColumn(),
                  SizedBox(height: 18.v),
                  _buildConfirmPasswordColumn()
                ]),
              ))
        ]));
  }

  /// Section Widget
  Widget _buildSaveChangesButton() {
    return CustomElevatedButton(
        text: "lbl_save_changes".tr,
        margin: EdgeInsets.only(left: 16.h, right: 16.h, bottom: 21.v),
        buttonStyle: CustomButtonStyles.fillPrimaryTL8,
        buttonTextStyle: CustomTextStyles.titleSmallInterWhiteA700);
  }

  /// Common widget
  Widget _buildTelevisionStack() {
    return SizedBox(
        height: 11.v,
        width: 9.h,
        child: Stack(alignment: Alignment.center, children: [
          CustomImageView(
              imagePath: ImageConstant.imgTelevision,
              height: 11.v,
              width: 9.h,
              alignment: Alignment.center),
          CustomImageView(
              imagePath: ImageConstant.imgTelevision,
              height: 11.v,
              width: 9.h,
              alignment: Alignment.center)
        ]));
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft() {
    Get.back();
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft1() {
    Get.back();
  }
}
