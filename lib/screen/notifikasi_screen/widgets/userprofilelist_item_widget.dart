import '../controller/notifikasi_controller.dart';
import '../models/userprofilelist_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/custom_icon_button.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class UserprofilelistItemWidget extends StatelessWidget {
  UserprofilelistItemWidget(
    this.userprofilelistItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  UserprofilelistItemModel userprofilelistItemModelObj;

  var controller = Get.find<NotifikasiController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 16.h,
        vertical: 6.v,
      ),
      decoration: AppDecoration.fillPrimary1,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(
              left: 61.h,
              right: 104.h,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 9.adaptSize,
                  width: 9.adaptSize,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      4.h,
                    ),
                    border: Border.all(
                      color: appTheme.whiteA700.withOpacity(0.2),
                      width: 2.h,
                    ),
                  ),
                ),
                Container(
                  height: 9.adaptSize,
                  width: 9.adaptSize,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      4.h,
                    ),
                    border: Border.all(
                      color: appTheme.whiteA700.withOpacity(0.2),
                      width: 2.h,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 4.v),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 48.v),
                child: Obx(
                  () => CustomIconButton(
                    height: 32.adaptSize,
                    width: 32.adaptSize,
                    padding: EdgeInsets.all(6.h),
                    decoration: IconButtonStyleHelper.fillTealA,
                    child: CustomImageView(
                      imagePath: userprofilelistItemModelObj.cloudIcon!.value,
                    ),
                  ),
                ),
              ),
              Container(
                height: 80.v,
                width: 298.h,
                margin: EdgeInsets.only(left: 12.h),
                child: Stack(
                  alignment: Alignment.bottomLeft,
                  children: [
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        height: 15.adaptSize,
                        width: 15.adaptSize,
                        margin: EdgeInsets.only(
                          right: 24.h,
                          bottom: 28.v,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                            7.h,
                          ),
                          border: Border.all(
                            color: appTheme.whiteA700.withOpacity(0.2),
                            width: 2.h,
                          ),
                        ),
                      ),
                    ),
                    Obx(
                      () => CustomImageView(
                        imagePath:
                            userprofilelistItemModelObj.televisionImage!.value,
                        height: 11.v,
                        width: 9.h,
                        alignment: Alignment.bottomLeft,
                        margin: EdgeInsets.only(
                          left: 91.h,
                          bottom: 32.v,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: SizedBox(
                        width: 290.h,
                        child: Obx(
                          () => Text(
                            userprofilelistItemModelObj.welcomeText!.value,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: CustomTextStyles.bodySmallBlack900,
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: EdgeInsets.only(top: 3.v),
                        child: Obx(
                          () => Text(
                            userprofilelistItemModelObj.timeText!.value,
                            style: theme.textTheme.bodySmall,
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Obx(
                            () => Text(
                              userprofilelistItemModelObj.publishedText!.value,
                              style: theme.textTheme.titleSmall,
                            ),
                          ),
                          SizedBox(height: 3.v),
                          Obx(
                            () => Text(
                              userprofilelistItemModelObj.formText!.value,
                              style: theme.textTheme.labelLarge,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
