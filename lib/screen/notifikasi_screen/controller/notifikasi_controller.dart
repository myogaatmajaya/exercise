import 'dart:convert';

import 'package:exercise/screen/notifikasi_screen/models/notifikasi_model.dart';
import 'package:get/get.dart';

class NotifikasiController extends GetxController {
  @override
  void onInit() {
    super.onInit();
  }

  Rx<NotifikasiModel> notifikasiModelObj = NotifikasiModel().obs;
}
