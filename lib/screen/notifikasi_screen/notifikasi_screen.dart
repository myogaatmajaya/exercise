import 'widgets/userprofilelist_item_widget.dart';
import 'controller/notifikasi_controller.dart';
import 'models/userprofilelist_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart';

class NotifikasiScreen extends GetWidget<NotifikasiController> {
  const NotifikasiScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final NotifikasiController controller = Get.find<NotifikasiController>();
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.maxFinite,
                child: Column(children: [
                  _buildTelevisionStack(),
                  SizedBox(height: 16.v),
                  _buildLatestRow(),
                  SizedBox(height: 3.v),
                  // _buildUserProfileList()
                ]))));
  }

  /// Section Widget
  Widget _buildTelevisionStack() {
    return SizedBox(
        height: 60.v,
        width: double.maxFinite,
        child: Stack(alignment: Alignment.bottomRight, children: [
          Align(
              alignment: Alignment.topCenter,
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.h),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(ImageConstant.imgGroup21),
                          fit: BoxFit.cover)),
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomImageView(
                            imagePath: ImageConstant.imgTelevision,
                            height: 7.v,
                            width: 9.h,
                            alignment: Alignment.centerRight,
                            margin: EdgeInsets.only(right: 23.h)),
                        SizedBox(height: 9.v),
                        Row(children: [
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              onTap: () {
                                onTapImgArrowLeft();
                              }),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 106.h, top: 2.v, bottom: 3.v),
                              child: Text("lbl_notifikasi".tr,
                                  style: theme.textTheme.titleMedium))
                        ]),
                        SizedBox(height: 13.v)
                      ]))),
          CustomImageView(
              imagePath: ImageConstant.imgTelevision,
              height: 11.v,
              width: 9.h,
              alignment: Alignment.bottomRight,
              margin: EdgeInsets.only(right: 169.h))
        ]));
  }

  /// Section Widget
  Widget _buildLatestRow() {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.h),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text("lbl_terbaru".tr, style: CustomTextStyles.titleMediumBlack900),
          Padding(
              padding: EdgeInsets.only(top: 2.v),
              child: Text("msg_tandai_sudah_di".tr,
                  style: CustomTextStyles.titleSmallPrimary))
        ]));
  }

  /// Section Widget
  Widget _buildUserProfileList() {
    return Obx(() => ListView.separated(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        separatorBuilder: (context, index) {
          return Padding(
              padding: EdgeInsets.symmetric(vertical: 0.5.v),
              child: SizedBox(
                  width: 343.h,
                  child: Divider(
                      height: 1.v, thickness: 1.v, color: appTheme.indigo50)));
        },
        itemCount: controller
            .notifikasiModelObj.value.userprofilelistItemList.value.length,
        itemBuilder: (context, index) {
          UserprofilelistItemModel model = controller
              .notifikasiModelObj.value.userprofilelistItemList.value[index];
          return UserprofilelistItemWidget(model);
        }));
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft() {
    Get.back();
  }
}
