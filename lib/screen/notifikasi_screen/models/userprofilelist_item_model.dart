import '../../../core/app_export.dart';

class UserprofilelistItemModel {
  UserprofilelistItemModel({
    this.cloudIcon,
    this.televisionImage,
    this.welcomeText,
    this.timeText,
    this.publishedText,
    this.formText,
    this.id,
  }) {
    cloudIcon = cloudIcon ?? Rx(ImageConstant.imgVuesaxBoldCloudPlus);
    televisionImage = televisionImage ?? Rx(ImageConstant.imgTelevision);
    welcomeText = welcomeText ??
        Rx("Selamat Anda Tergabung dengan workspace Pilpres\n2024");
    timeText = timeText ?? Rx("1 Jam Lalu");
    publishedText = publishedText ?? Rx("Published");
    formText = formText ?? Rx("Form");
    id = id ?? Rx("");
  }

  Rx<String>? cloudIcon;

  Rx<String>? televisionImage;

  Rx<String>? welcomeText;

  Rx<String>? timeText;

  Rx<String>? publishedText;

  Rx<String>? formText;

  Rx<String>? id;
}
