import '../../../core/app_export.dart';
import 'userprofilelist_item_model.dart';

class NotifikasiModel {
  Rx<List<UserprofilelistItemModel>> userprofilelistItemList = Rx([
    UserprofilelistItemModel(
        cloudIcon: ImageConstant.imgVuesaxBoldCloudPlus.obs,
        televisionImage: ImageConstant.imgTelevision.obs,
        welcomeText:
            "Selamat Anda Tergabung dengan workspace Pilpres\n2024".obs,
        timeText: "1 Jam Lalu".obs,
        publishedText: "Published".obs,
        formText: "Form".obs)
  ]);
}
