import '../controller/data_sukses_controller.dart';
import 'package:get/get.dart';

class DataSuksesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DataSuksesController());
  }
}
