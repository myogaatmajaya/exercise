import 'controller/data_sukses_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:flutter/material.dart';

// ignore_for_file: must_be_immutable
class DataSuksesScreen extends GetWidget<DataSuksesController> {
  const DataSuksesScreen({Key? key})
      : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(horizontal: 19.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgSuccessfulIllustration,
                height: 182.v,
                width: 200.h,
              ),
              SizedBox(height: 33.v),
              Text(
                "msg_data_sukses_terkirim".tr,
                style: theme.textTheme.titleLarge,
              ),
              SizedBox(height: 8.v),
              SizedBox(
                width: 219.h,
                child: Text(
                  "msg_yeay_data_anda".tr,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: CustomTextStyles.bodyMediumBluegray900d3.copyWith(
                    height: 1.52,
                  ),
                ),
              ),
              SizedBox(height: 27.v),
              CustomElevatedButton(
                text: "lbl_okay".tr,
              ),
              SizedBox(height: 5.v),
            ],
          ),
        ),
      ),
    );
  }
}
