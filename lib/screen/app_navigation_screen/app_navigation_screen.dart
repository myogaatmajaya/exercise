import 'controller/app_navigation_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart';

// ignore_for_file: must_be_immutable
class AppNavigationScreen extends GetWidget<AppNavigationController> {
  const AppNavigationScreen({Key? key})
      : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0XFFFFFFFF),
        body: SizedBox(
          width: double.maxFinite,
          child: Column(
            children: [
              _buildAppNavigation(),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Color(0XFFFFFFFF),
                    ),
                    child: Column(
                      children: [
                        _buildScreenTitle(
                          screenTitle: "Home".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.homeScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Hasil Pencarian".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.hasilPencarianScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Profile".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.profileScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Notifikasi One".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.notifikasiOneScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Search".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.searchScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Workspace".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.workspaceScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Form Laporan".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.formLaporanScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Form Laporan One".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.formLaporanOneScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Form".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.formScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Edit Profile One".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.editProfileOneScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Edit Profile".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.editProfileScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Change Password".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.changePasswordScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "iPhone 11 Pro / X - Three".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.iphone11ProXThreeScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Login".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.loginScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Register".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.registerScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Forgot Password".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.forgotPasswordScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Sent Code Password".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.sentCodePasswordScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Set New Password".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.setNewPasswordScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Notifikasi".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.notifikasiScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Tidak Ada Koneksi".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.tidakAdaKoneksiScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Data Sukses".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.dataSuksesScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Data Gagal Terkirim".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.dataGagalTerkirimScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "GPS Tidak Aktif".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.gpsTidakAktifScreen),
                        ),
                        _buildScreenTitle(
                          screenTitle: "Lokasi Tidak Terdeteksi".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.lokasiTidakTerdeteksiScreen),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildAppNavigation() {
    return Container(
      decoration: BoxDecoration(
        color: Color(0XFFFFFFFF),
      ),
      child: Column(
        children: [
          SizedBox(height: 10.v),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.h),
              child: Text(
                "App Navigation".tr,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0XFF000000),
                  fontSize: 20.fSize,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ),
          SizedBox(height: 10.v),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(left: 20.h),
              child: Text(
                "Pilih halaman yang ingin kamu tuju".tr,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0XFF888888),
                  fontSize: 16.fSize,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ),
          SizedBox(height: 5.v),
          Divider(
            height: 1.v,
            thickness: 1.v,
            color: Color(0XFF000000),
          ),
        ],
      ),
    );
  }

  /// Common widget
  Widget _buildScreenTitle({
    required String screenTitle,
    Function? onTapScreenTitle,
  }) {
    return GestureDetector(
      onTap: () {
        onTapScreenTitle!.call();
      },
      child: Container(
        decoration: BoxDecoration(
          color: Color(0XFFFFFFFF),
        ),
        child: Column(
          children: [
            SizedBox(height: 10.v),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.h),
                child: Text(
                  screenTitle,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0XFF000000),
                    fontSize: 20.fSize,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
            SizedBox(height: 10.v),
            SizedBox(height: 5.v),
            Divider(
              height: 1.v,
              thickness: 1.v,
              color: Color(0XFF888888),
            ),
          ],
        ),
      ),
    );
  }

  /// Common click event
  void onTapScreenTitle(String routeName) {
    Get.toNamed(routeName);
  }
}
