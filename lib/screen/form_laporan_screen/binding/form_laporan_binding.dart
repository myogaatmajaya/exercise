import '../controller/form_laporan_controller.dart';
import 'package:get/get.dart';

class FormLaporanBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => FormLaporanController());
  }
}
