import 'package:exercise/core/app_export.dart';
import 'package:exercise/screen/form_laporan_screen/models/form_laporan_model.dart';

class FormLaporanController extends GetxController {
  Rx<FormLaporanModel> formLaporanModelObj = FormLaporanModel().obs;

  SelectionPopupModel? selectedDropDownValue;

  onSelected(dynamic value) {
    for (var element in formLaporanModelObj.value.dropdownItemList.value) {
      element.isSelected = false;
      if (element.id == value.id) {
        element.isSelected = true;
      }
    }
    formLaporanModelObj.value.dropdownItemList.refresh();
  }
}
