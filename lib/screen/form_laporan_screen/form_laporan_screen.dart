import 'controller/form_laporan_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/appbar_title.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:exercise/widgets/custom_drop_down.dart';
import 'package:flutter/material.dart';

class FormLaporanScreen extends GetWidget<FormLaporanController> {
  const FormLaporanScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            appBar: _buildAppBar(),
            body: Container(
                width: double.maxFinite,
                padding: EdgeInsets.only(left: 16.h, top: 64.v, right: 16.h),
                child: Column(children: [
                  CustomImageView(
                      imagePath: ImageConstant.imgGroup2146,
                      height: 221.v,
                      width: 235.h),
                  SizedBox(height: 27.v),
                  Text("lbl_pilih_form".tr, style: theme.textTheme.titleLarge),
                  SizedBox(height: 8.v),
                  Container(
                      width: 268.h,
                      margin: EdgeInsets.only(left: 36.h, right: 37.h),
                      child: Text("msg_amet_minim_mollit3".tr,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: CustomTextStyles.bodyMediumBluegray900d3
                              .copyWith(height: 1.50))),
                  SizedBox(height: 17.v),
                  CustomDropDown(
                      icon: Container(
                          margin: EdgeInsets.fromLTRB(30.h, 8.v, 16.h, 8.v),
                          child: CustomImageView(
                              imagePath:
                                  ImageConstant.imgArrowdownBlueGray90002,
                              height: 24.adaptSize,
                              width: 24.adaptSize)),
                      hintText: "lbl_pilih_form".tr,
                      hintStyle: CustomTextStyles.bodyMediumBluegray90002,
                      items: controller
                          .formLaporanModelObj.value.dropdownItemList.value,
                      onChanged: (value) {
                        controller.onSelected(value);
                      }),
                  SizedBox(height: 5.v)
                ]))));
  }

  PreferredSizeWidget _buildAppBar() {
    return CustomAppBar(
        leadingWidth: 44.h,
        leading: Container(
            height: 28.adaptSize,
            width: 28.adaptSize,
            margin: EdgeInsets.only(left: 16.h, top: 16.v),
            child: Stack(children: [
              CustomImageView(
                  imagePath: ImageConstant.imgArrowLeftWhiteA700,
                  height: 28.adaptSize,
                  width: 28.adaptSize,
                  alignment: Alignment.center,
                  onTap: () {
                    onTapImgArrowLeft();
                  }),
              SizedBox(
                  height: 28.adaptSize,
                  width: 28.adaptSize,
                  child: Stack(children: [
                    CustomImageView(
                        imagePath: ImageConstant.imgArrowLeftWhiteA700,
                        height: 28.adaptSize,
                        width: 28.adaptSize,
                        alignment: Alignment.center),
                    SizedBox(
                        height: 28.adaptSize,
                        width: 28.adaptSize,
                        child: Stack(alignment: Alignment.center, children: [
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              alignment: Alignment.center),
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftBlack900,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              alignment: Alignment.center)
                        ]))
                  ]))
            ])),
        centerTitle: true,
        title: AppbarTitle(text: "lbl_form".tr),
        actions: [
          Container(
              height: 7.v,
              width: 9.h,
              margin: EdgeInsets.only(left: 40.h, right: 40.h, bottom: 37.v),
              child: Stack(alignment: Alignment.center, children: [
                CustomImageView(
                    imagePath: ImageConstant.imgTelevision,
                    height: 7.v,
                    width: 9.h,
                    alignment: Alignment.center),
                CustomImageView(
                    imagePath: ImageConstant.imgTelevision,
                    height: 7.v,
                    width: 9.h,
                    alignment: Alignment.center)
              ]))
        ]);
  }

  onTapImgArrowLeft() {
    Get.back();
  }
}
