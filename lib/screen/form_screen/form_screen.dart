import 'controller/form_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:exercise/widgets/custom_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';

class FormScreen extends GetWidget<FormController> {
  const FormScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.maxFinite,
                child: Column(children: [
                  _buildTelevisionStack(),
                  Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 15.h, vertical: 18.v),
                      child: Column(children: [
                        _buildFrameColumn(),
                        SizedBox(height: 19.v),
                        _buildWorkspaceColumn(),
                        SizedBox(height: 5.v)
                      ]))
                ]))));
  }

  /// Section Widget
  Widget _buildTelevisionStack() {
    return SizedBox(
        height: 250.v,
        width: double.maxFinite,
        child: Stack(alignment: Alignment.center, children: [
          Align(
              alignment: Alignment.topLeft,
              child: Padding(
                  padding: EdgeInsets.only(left: 13.h),
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        CustomAppBar(
                            leadingWidth: double.maxFinite,
                            leading: Container(
                                height: 28.adaptSize,
                                width: 28.adaptSize,
                                margin: EdgeInsets.only(
                                    left: 16.h, top: 9.v, right: 331.h),
                                child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      CustomImageView(
                                          imagePath: ImageConstant
                                              .imgArrowLeftWhiteA700,
                                          height: 28.adaptSize,
                                          width: 28.adaptSize,
                                          alignment: Alignment.center,
                                          onTap: () {
                                            onTapImgArrowLeft();
                                          }),
                                      CustomImageView(
                                          imagePath: ImageConstant
                                              .imgArrowLeftWhiteA700,
                                          height: 28.adaptSize,
                                          width: 28.adaptSize,
                                          alignment: Alignment.center)
                                    ])),
                            actions: [
                              Container(
                                  height: 7.v,
                                  width: 9.h,
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 40.h),
                                  child: Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        CustomImageView(
                                            imagePath:
                                                ImageConstant.imgTelevision,
                                            height: 7.v,
                                            width: 9.h,
                                            alignment: Alignment.center),
                                        CustomImageView(
                                            imagePath:
                                                ImageConstant.imgTelevision,
                                            height: 7.v,
                                            width: 9.h,
                                            alignment: Alignment.center)
                                      ]))
                            ]),
                        SizedBox(height: 5.v),
                        CustomImageView(
                            imagePath: ImageConstant.imgTelevision,
                            height: 11.v,
                            width: 9.h,
                            margin: EdgeInsets.only(right: 130.h)),
                        SizedBox(height: 58.v),
                        CustomImageView(
                            imagePath: ImageConstant.imgTelevision,
                            height: 11.v,
                            width: 9.h,
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(left: 135.h))
                      ]))),
          Align(
              alignment: Alignment.center,
              child: Container(
                  padding: EdgeInsets.all(10.h),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(ImageConstant.imgGroup49),
                          fit: BoxFit.cover)),
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      CustomImageView(
                          imagePath: ImageConstant.imgArrowLeftWhiteA700,
                          height: 28.adaptSize,
                          width: 28.adaptSize),
                      Spacer(flex: 69),
                      Padding(
                          padding: EdgeInsets.only(top: 3.v, bottom: 2.v),
                          child: Text("lbl_form".tr,
                              style: theme.textTheme.titleMedium)),
                      Spacer(flex: 30),
                      CustomImageView(
                          imagePath: ImageConstant.imgMaterialSymbolsEdit,
                          height: 16.adaptSize,
                          width: 16.adaptSize,
                          margin: EdgeInsets.only(top: 7.v, bottom: 5.v)),
                      Padding(
                          padding:
                              EdgeInsets.only(left: 8.h, top: 8.v, bottom: 2.v),
                          child: Text("lbl_edit_profile".tr,
                              style: CustomTextStyles.titleSmallInterWhiteA700
                                  .copyWith(
                                      decoration: TextDecoration.underline)))
                    ]),
                    SizedBox(height: 5.v),
                    Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                            padding: EdgeInsets.only(left: 40.h, right: 25.h),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                      height: 185.v,
                                      width: 245.h,
                                      child: Stack(
                                          alignment: Alignment.topRight,
                                          children: [
                                            Align(
                                                alignment: Alignment.topLeft,
                                                child: Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 14.h, top: 30.v),
                                                    child: Column(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          SizedBox(
                                                              width: 178.h,
                                                              child: Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceBetween,
                                                                  children: [
                                                                    Container(
                                                                        decoration: AppDecoration.outlineWhiteA.copyWith(
                                                                            borderRadius: BorderRadiusStyle
                                                                                .roundedBorder6),
                                                                        child: Container(
                                                                            height:
                                                                                9.adaptSize,
                                                                            width: 9.adaptSize,
                                                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(4.h), border: Border.all(color: appTheme.whiteA700.withOpacity(0.2), width: 2.h)))),
                                                                    Container(
                                                                        decoration: AppDecoration.outlineWhiteA.copyWith(
                                                                            borderRadius: BorderRadiusStyle
                                                                                .roundedBorder6),
                                                                        child: Container(
                                                                            height:
                                                                                9.adaptSize,
                                                                            width: 9.adaptSize,
                                                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(4.h), border: Border.all(color: appTheme.whiteA700.withOpacity(0.2), width: 2.h))))
                                                                  ])),
                                                          SizedBox(
                                                              height: 30.v),
                                                          CustomImageView(
                                                              imagePath:
                                                                  ImageConstant
                                                                      .imgTelevision,
                                                              height: 11.v,
                                                              width: 9.h,
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left:
                                                                          74.h))
                                                        ]))),
                                            CustomImageView(
                                                imagePath:
                                                    ImageConstant.imgTelevision,
                                                height: 11.v,
                                                width: 9.h,
                                                alignment: Alignment.topRight,
                                                margin: EdgeInsets.only(
                                                    right: 102.h)),
                                            Align(
                                                alignment: Alignment.center,
                                                child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    children: [
                                                      CustomImageView(
                                                          imagePath: ImageConstant
                                                              .imgRectangle3009,
                                                          height: 100.adaptSize,
                                                          width: 100.adaptSize,
                                                          radius: BorderRadius
                                                              .circular(8.h)),
                                                      SizedBox(height: 13.v),
                                                      Text(
                                                          "msg_laporan_kegiatan"
                                                              .tr,
                                                          style: theme.textTheme
                                                              .titleMedium),
                                                      SizedBox(height: 6.v),
                                                      SizedBox(
                                                          width: 245.h,
                                                          child: Text(
                                                              "msg_ini_berisikan_deskripsi"
                                                                  .tr,
                                                              maxLines: 2,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                              style: CustomTextStyles
                                                                  .bodySmallWhiteA700_2))
                                                    ]))
                                          ])),
                                  Container(
                                      margin: EdgeInsets.only(
                                          left: 10.h, top: 69.v, bottom: 101.v),
                                      decoration: AppDecoration.outlineWhiteA
                                          .copyWith(
                                              borderRadius: BorderRadiusStyle
                                                  .roundedBorder6),
                                      child: Container(
                                          height: 15.adaptSize,
                                          width: 15.adaptSize,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(7.h),
                                              border: Border.all(
                                                  color: appTheme.whiteA700
                                                      .withOpacity(0.2),
                                                  width: 2.h))))
                                ]))),
                    SizedBox(height: 6.v)
                  ])))
        ]));
  }

  /// Section Widget
  Widget _buildFrameColumn() {
    return Column(children: [
      _buildUserRow(
          pembuatText: "lbl_workspace".tr, haritsText: "lbl_klien_b".tr),
      SizedBox(height: 12.v),
      Divider(),
      SizedBox(height: 16.v),
      _buildUserRow(
          pembuatText: "lbl_pembuat".tr,
          haritsText: "msg_harits_harits_ebdesk_com".tr),
      SizedBox(height: 12.v),
      Divider(),
      SizedBox(height: 16.v),
      _buildUserRow(
          pembuatText: "lbl_tanggal_dibuat".tr,
          haritsText: "lbl_12_march_2022".tr),
      SizedBox(height: 12.v),
      Divider(),
      SizedBox(height: 16.v),
      _buildUserRow(pembuatText: "lbl_status".tr, haritsText: "lbl_aktif".tr),
      SizedBox(height: 12.v),
      Divider()
    ]);
  }

  /// Section Widget
  Widget _buildWorkspaceColumn() {
    return Column(children: [
      Align(
          alignment: Alignment.centerLeft,
          child: Text("lbl_detail".tr, style: theme.textTheme.titleSmall)),
      SizedBox(height: 15.v),
      Container(
          padding: EdgeInsets.symmetric(horizontal: 15.h, vertical: 17.v),
          decoration: AppDecoration.outlineIndigo50
              .copyWith(borderRadius: BorderRadiusStyle.roundedBorder6),
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            SizedBox(
                height: 50.v,
                width: 64.h,
                child: Stack(alignment: Alignment.centerLeft, children: [
                  Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                          height: 9.v,
                          width: 48.h,
                          margin: EdgeInsets.only(left: 5.h, bottom: 4.v),
                          decoration: BoxDecoration(
                              color: appTheme.blue50059,
                              borderRadius: BorderRadius.circular(24.h)))),
                  Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                          margin: EdgeInsets.only(left: 5.h),
                          padding: EdgeInsets.symmetric(
                              horizontal: 3.h, vertical: 2.v),
                          decoration: AppDecoration
                              .gradientBlueToOnPrimaryContainer
                              .copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.circleBorder26),
                          child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                    height: 39.v,
                                    width: 42.h,
                                    child: Stack(
                                        alignment: Alignment.bottomLeft,
                                        children: [
                                          Align(
                                              alignment: Alignment.topCenter,
                                              child: SizedBox(
                                                  height: 29.v,
                                                  width: 42.h,
                                                  child: Stack(
                                                      alignment:
                                                          Alignment.topRight,
                                                      children: [
                                                        CustomImageView(
                                                            imagePath: ImageConstant
                                                                .imgVector29x42,
                                                            height: 29.v,
                                                            width: 42.h,
                                                            alignment: Alignment
                                                                .center),
                                                        Align(
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Container(
                                                                height: 5.v,
                                                                width: 15.h,
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        top: 11
                                                                            .v,
                                                                        right: 2
                                                                            .h),
                                                                decoration: BoxDecoration(
                                                                    color: appTheme
                                                                        .pinkA2004c,
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            7.h))))
                                                      ]))),
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgVector2x1,
                                              height: 25.v,
                                              width: 17.h,
                                              alignment: Alignment.bottomLeft),
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgVector2x1,
                                              height: 25.v,
                                              width: 17.h,
                                              alignment: Alignment.bottomRight),
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgVector14x42,
                                              height: 14.v,
                                              width: 42.h,
                                              alignment: Alignment.bottomCenter)
                                        ])),
                                SizedBox(height: 1.v),
                                OutlineGradientButton(
                                    padding: EdgeInsets.only(
                                        left: 2.h,
                                        top: 2.v,
                                        right: 2.h,
                                        bottom: 2.v),
                                    strokeWidth: 2.h,
                                    gradient: LinearGradient(
                                        begin: Alignment(-0.11, 0.19),
                                        end: Alignment(1.45, 1.54),
                                        colors: [
                                          appTheme.blue20001,
                                          theme.colorScheme.secondaryContainer
                                        ]),
                                    corners: Corners(
                                        topLeft: Radius.circular(1),
                                        topRight: Radius.circular(1),
                                        bottomLeft: Radius.circular(1),
                                        bottomRight: Radius.circular(1)),
                                    child: Padding(
                                        padding: EdgeInsets.only(left: 3.h),
                                        child: Container(
                                            height: 2.adaptSize,
                                            width: 2.adaptSize,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        1.h))))),
                                SizedBox(height: 1.v)
                              ]))),
                  Align(
                      alignment: Alignment.centerRight,
                      child: SizedBox(
                          height: 46.v,
                          width: 26.h,
                          child: Stack(alignment: Alignment.center, children: [
                            Align(
                                alignment: Alignment.center,
                                child: Text("lbl2".tr,
                                    style: theme.textTheme.displayMedium)),
                            Align(
                                alignment: Alignment.center,
                                child: Padding(
                                    padding:
                                        EdgeInsets.only(left: 6.h, right: 4.h),
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          OutlineGradientButton(
                                              padding: EdgeInsets.only(
                                                  left: 2.h,
                                                  top: 2.v,
                                                  right: 2.h,
                                                  bottom: 2.v),
                                              strokeWidth: 2.h,
                                              gradient: LinearGradient(
                                                  begin: Alignment(-0.11, 0.19),
                                                  end: Alignment(1.45, 1.54),
                                                  colors: [
                                                    appTheme.blue20001,
                                                    theme.colorScheme
                                                        .secondaryContainer
                                                  ]),
                                              corners: Corners(
                                                  topLeft: Radius.circular(1),
                                                  topRight: Radius.circular(1),
                                                  bottomLeft:
                                                      Radius.circular(1),
                                                  bottomRight:
                                                      Radius.circular(1)),
                                              child: Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 4.h),
                                                  child: Container(
                                                      height: 2.adaptSize,
                                                      width: 2.adaptSize,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  1.h))))),
                                          SizedBox(height: 30.v),
                                          SizedBox(
                                              width: 15.h,
                                              child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    OutlineGradientButton(
                                                        padding: EdgeInsets.only(
                                                            left: 2.h,
                                                            top: 2.v,
                                                            right: 2.h,
                                                            bottom: 2.v),
                                                        strokeWidth: 2.h,
                                                        gradient: LinearGradient(
                                                            begin: Alignment(-0.11, 0.19),
                                                            end: Alignment(1.45, 1.54),
                                                            colors: [
                                                              appTheme
                                                                  .blue20001,
                                                              theme.colorScheme
                                                                  .secondaryContainer
                                                            ]),
                                                        corners: Corners(
                                                            topLeft: Radius.circular(
                                                                1),
                                                            topRight: Radius.circular(
                                                                1),
                                                            bottomLeft:
                                                                Radius.circular(
                                                                    1),
                                                            bottomRight:
                                                                Radius.circular(
                                                                    1)),
                                                        child: Padding(
                                                            padding: EdgeInsets.only(top: 7.v),
                                                            child: Container(height: 2.adaptSize, width: 2.adaptSize, decoration: BoxDecoration(borderRadius: BorderRadius.circular(1.h))))),
                                                    CustomImageView(
                                                        imagePath: ImageConstant
                                                            .imgStar24,
                                                        height: 9.adaptSize,
                                                        width: 9.adaptSize,
                                                        radius: BorderRadius
                                                            .circular(2.h))
                                                  ]))
                                        ])))
                          ]))),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                          height: 14.v,
                          width: 24.h,
                          margin: EdgeInsets.only(top: 12.v),
                          child: Stack(alignment: Alignment.topLeft, children: [
                            CustomImageView(
                                imagePath: ImageConstant.imgVector14x24,
                                height: 14.v,
                                width: 24.h,
                                alignment: Alignment.center),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                    height: 8.v,
                                    width: 12.h,
                                    margin:
                                        EdgeInsets.only(left: 3.h, top: 2.v),
                                    child: Stack(
                                        alignment: Alignment.topLeft,
                                        children: [
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgRectangle8,
                                              height: 3.v,
                                              width: 7.h,
                                              alignment: Alignment.topLeft),
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgRectangle8,
                                              height: 2.v,
                                              width: 5.h,
                                              alignment: Alignment.topLeft,
                                              margin: EdgeInsets.only(
                                                  left: 1.h, top: 2.v)),
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgRectangle10,
                                              height: 2.v,
                                              width: 5.h,
                                              alignment: Alignment.bottomCenter,
                                              margin:
                                                  EdgeInsets.only(bottom: 2.v)),
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgRectangle11,
                                              height: 1.v,
                                              width: 3.h,
                                              alignment: Alignment.bottomRight,
                                              margin:
                                                  EdgeInsets.only(right: 1.h)),
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgRectangle10,
                                              height: 3.v,
                                              width: 7.h,
                                              alignment: Alignment.bottomRight,
                                              margin:
                                                  EdgeInsets.only(bottom: 1.v))
                                        ])))
                          ]))),
                  CustomImageView(
                      imagePath: ImageConstant.imgGroup1,
                      height: 17.v,
                      width: 29.h,
                      alignment: Alignment.bottomRight,
                      margin: EdgeInsets.only(right: 3.h, bottom: 12.v)),
                  CustomImageView(
                      imagePath: ImageConstant.imgStar14,
                      height: 9.adaptSize,
                      width: 9.adaptSize,
                      radius: BorderRadius.circular(2.h),
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(left: 12.h, top: 5.v)),
                  CustomImageView(
                      imagePath: ImageConstant.imgPolygon1,
                      height: 5.adaptSize,
                      width: 5.adaptSize,
                      radius: BorderRadius.circular(1.h),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(left: 2.h, bottom: 9.v)),
                  CustomImageView(
                      imagePath: ImageConstant.imgPolygon2,
                      height: 5.adaptSize,
                      width: 5.adaptSize,
                      radius: BorderRadius.circular(1.h),
                      alignment: Alignment.topRight,
                      margin: EdgeInsets.only(top: 15.v, right: 5.h))
                ])),
            Padding(
                padding: EdgeInsets.only(left: 12.h, top: 3.v, bottom: 2.v),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("lbl_semua_laporan".tr,
                          style: CustomTextStyles.bodyMediumBluegray300),
                      SizedBox(height: 1.v),
                      Text("lbl_1300".tr,
                          style: CustomTextStyles.titleMediumBlack900Bold)
                    ])),
            Spacer(),
            CustomImageView(
                imagePath: ImageConstant.imgArrowRightBlueGray300,
                height: 28.adaptSize,
                width: 28.adaptSize,
                margin: EdgeInsets.symmetric(vertical: 11.v))
          ])),
      SizedBox(height: 8.v),
      Container(
          padding: EdgeInsets.symmetric(horizontal: 15.h, vertical: 17.v),
          decoration: AppDecoration.outlineIndigo50
              .copyWith(borderRadius: BorderRadiusStyle.roundedBorder6),
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            SizedBox(
                height: 50.v,
                width: 51.h,
                child: Stack(alignment: Alignment.bottomLeft, children: [
                  Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 7.h, vertical: 5.v),
                          decoration: AppDecoration.gradientWhiteAToCyan
                              .copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.circleBorder26),
                          child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CustomImageView(
                                    imagePath: ImageConstant.imgVector,
                                    height: 3.v,
                                    width: 32.h),
                                SizedBox(height: 4.v),
                                SizedBox(
                                    height: 29.v,
                                    width: 32.h,
                                    child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgVector35x39,
                                              height: 29.v,
                                              width: 32.h,
                                              alignment: Alignment.center),
                                          Align(
                                              alignment: Alignment.center,
                                              child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 4.h),
                                                  child: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: [
                                                        CustomImageView(
                                                            imagePath:
                                                                ImageConstant
                                                                    .imgVector1x27,
                                                            height: 1.v,
                                                            width: 23.h),
                                                        SizedBox(height: 5.v),
                                                        CustomImageView(
                                                            imagePath:
                                                                ImageConstant
                                                                    .imgVector1x27,
                                                            height: 1.v,
                                                            width: 23.h),
                                                        SizedBox(height: 5.v),
                                                        CustomImageView(
                                                            imagePath:
                                                                ImageConstant
                                                                    .imgVector1x27,
                                                            height: 1.v,
                                                            width: 23.h),
                                                        SizedBox(height: 5.v),
                                                        CustomImageView(
                                                            imagePath:
                                                                ImageConstant
                                                                    .imgVector1x27,
                                                            height: 1.v,
                                                            width: 23.h)
                                                      ])))
                                        ]))
                              ]))),
                  Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                          height: 19.v,
                          width: 14.h,
                          margin: EdgeInsets.only(bottom: 4.v),
                          child: Stack(alignment: Alignment.center, children: [
                            CustomImageView(
                                imagePath: ImageConstant.imgVector23x17,
                                height: 19.v,
                                width: 14.h,
                                alignment: Alignment.center),
                            Align(
                                alignment: Alignment.center,
                                child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 2.h),
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgVector3x5,
                                              height: 2.v,
                                              width: 4.h,
                                              alignment: Alignment.centerLeft),
                                          SizedBox(height: 2.v),
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgVector3x5,
                                              height: 1.v,
                                              width: 10.h,
                                              radius:
                                                  BorderRadius.circular(1.h)),
                                          SizedBox(height: 2.v),
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgVector3x5,
                                              height: 1.v,
                                              width: 9.h,
                                              radius:
                                                  BorderRadius.circular(1.h)),
                                          SizedBox(height: 2.v),
                                          CustomImageView(
                                              imagePath:
                                                  ImageConstant.imgVector3x5,
                                              height: 1.v,
                                              width: 9.h,
                                              radius:
                                                  BorderRadius.circular(1.h))
                                        ])))
                          ]))),
                  Align(
                      alignment: Alignment.topRight,
                      child: Container(
                          height: 8.v,
                          width: 14.h,
                          margin: EdgeInsets.only(right: 4.h),
                          child: Stack(alignment: Alignment.center, children: [
                            CustomImageView(
                                imagePath: ImageConstant.imgVector23x17,
                                height: 8.v,
                                width: 14.h,
                                alignment: Alignment.center),
                            CustomImageView(
                                imagePath: ImageConstant.imgGroup24,
                                height: 2.v,
                                width: 11.h,
                                alignment: Alignment.center)
                          ]))),
                  CustomImageView(
                      imagePath: ImageConstant.imgVector13x14,
                      height: 11.adaptSize,
                      width: 11.adaptSize,
                      alignment: Alignment.bottomRight,
                      margin: EdgeInsets.only(right: 2.h, bottom: 3.v)),
                  Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                          height: 26.adaptSize,
                          width: 26.adaptSize,
                          margin: EdgeInsets.only(right: 7.h, bottom: 8.v),
                          child: Stack(alignment: Alignment.center, children: [
                            CustomImageView(
                                imagePath: ImageConstant.imgSubtract,
                                height: 26.adaptSize,
                                width: 26.adaptSize,
                                alignment: Alignment.center),
                            Align(
                                alignment: Alignment.center,
                                child: Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 2.h),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 2.h, vertical: 3.v),
                                    decoration: AppDecoration
                                        .gradientWhiteAToCyan5033
                                        .copyWith(
                                            borderRadius: BorderRadiusStyle
                                                .circleBorder13),
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          SizedBox(height: 12.v),
                                          Container(
                                              height: 2.v,
                                              width: 15.h,
                                              decoration: BoxDecoration(
                                                  color: appTheme.pink4008e,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          7.h)))
                                        ])))
                          ]))),
                  CustomImageView(
                      imagePath: ImageConstant.imgStar15,
                      height: 8.adaptSize,
                      width: 8.adaptSize,
                      radius: BorderRadius.circular(2.h),
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(left: 18.h)),
                  CustomImageView(
                      imagePath: ImageConstant.imgStar25,
                      height: 8.adaptSize,
                      width: 8.adaptSize,
                      radius: BorderRadius.circular(2.h),
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(left: 1.h)),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                          height: 2.adaptSize,
                          width: 2.adaptSize,
                          margin: EdgeInsets.only(left: 22.h),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(1.h),
                              gradient: LinearGradient(
                                  begin: Alignment(0, 0.23),
                                  end: Alignment(1, 1),
                                  colors: [
                                    appTheme.cyan20002,
                                    appTheme.cyan300
                                  ])))),
                  CustomImageView(
                      imagePath: ImageConstant.imgPolygon2,
                      height: 5.adaptSize,
                      width: 5.adaptSize,
                      radius: BorderRadius.circular(1.h),
                      alignment: Alignment.topRight,
                      margin: EdgeInsets.only(top: 20.v, right: 1.h)),
                  CustomImageView(
                      imagePath: ImageConstant.imgPolygon3,
                      height: 5.adaptSize,
                      width: 5.adaptSize,
                      radius: BorderRadius.circular(1.h),
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(top: 18.v)),
                  CustomImageView(
                      imagePath: ImageConstant.imgPolygon3,
                      height: 5.adaptSize,
                      width: 5.adaptSize,
                      radius: BorderRadius.circular(1.h),
                      alignment: Alignment.bottomRight,
                      margin: EdgeInsets.only(right: 11.h, bottom: 1.v)),
                  Align(
                      alignment: Alignment.topRight,
                      child: Container(
                          height: 2.adaptSize,
                          width: 2.adaptSize,
                          margin: EdgeInsets.only(top: 11.v, right: 2.h),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(1.h),
                              gradient: LinearGradient(
                                  begin: Alignment(0, 0.23),
                                  end: Alignment(1, 1),
                                  colors: [
                                    appTheme.cyan20002,
                                    appTheme.cyan300
                                  ])))),
                  CustomImageView(
                      imagePath: ImageConstant.imgVectorCyan300,
                      height: 5.adaptSize,
                      width: 5.adaptSize,
                      alignment: Alignment.bottomRight,
                      margin: EdgeInsets.only(bottom: 1.v))
                ])),
            Padding(
                padding: EdgeInsets.only(left: 25.h, top: 3.v, bottom: 2.v),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("lbl_laporan_saya".tr,
                          style: CustomTextStyles.bodyMediumBluegray300),
                      SizedBox(height: 1.v),
                      Text("lbl_230".tr,
                          style: CustomTextStyles.titleMediumBlack900Bold)
                    ])),
            Spacer(),
            CustomImageView(
                imagePath: ImageConstant.imgArrowRightBlueGray300,
                height: 28.adaptSize,
                width: 28.adaptSize,
                margin: EdgeInsets.symmetric(vertical: 11.v))
          ]))
    ]);
  }

  /// Common widget
  Widget _buildUserRow({
    required String pembuatText,
    required String haritsText,
  }) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      CustomIconButton(
          height: 32.adaptSize,
          width: 32.adaptSize,
          padding: EdgeInsets.all(8.h),
          decoration: IconButtonStyleHelper.fillLightBlue,
          child: CustomImageView(imagePath: ImageConstant.imgBxsUser)),
      Padding(
          padding: EdgeInsets.only(left: 12.h, top: 6.v, bottom: 5.v),
          child: Text(pembuatText,
              style: theme.textTheme.bodyMedium!
                  .copyWith(color: appTheme.black900))),
      Spacer(),
      Padding(
          padding: EdgeInsets.only(top: 7.v, bottom: 4.v),
          child: Text(haritsText,
              style: theme.textTheme.bodyMedium!
                  .copyWith(color: appTheme.black900)))
    ]);
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft() {
    Get.back();
  }
}
