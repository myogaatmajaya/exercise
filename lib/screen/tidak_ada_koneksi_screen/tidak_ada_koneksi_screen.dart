import 'controller/tidak_ada_koneksi_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:flutter/material.dart';

// ignore_for_file: must_be_immutable
class TidakAdaKoneksiScreen extends GetWidget<TidakAdaKoneksiController> {
  const TidakAdaKoneksiScreen({Key? key})
      : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(horizontal: 19.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgConnectionIllustration,
                height: 168.v,
                width: 172.h,
              ),
              SizedBox(height: 35.v),
              Text(
                "msg_tidak_ada_koneksi".tr,
                style: theme.textTheme.titleLarge,
              ),
              SizedBox(height: 8.v),
              Container(
                width: 271.h,
                margin: EdgeInsets.only(
                  left: 32.h,
                  right: 33.h,
                ),
                child: Text(
                  "msg_whoops_mohon_periksa".tr,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: CustomTextStyles.bodyMediumBluegray900d3.copyWith(
                    height: 1.52,
                  ),
                ),
              ),
              SizedBox(height: 27.v),
              CustomElevatedButton(
                text: "lbl_coba_lagi".tr,
              ),
              SizedBox(height: 5.v),
            ],
          ),
        ),
      ),
    );
  }
}
