import '../controller/tidak_ada_koneksi_controller.dart';
import 'package:get/get.dart';

class TidakAdaKoneksiBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => TidakAdaKoneksiController());
  }
}
