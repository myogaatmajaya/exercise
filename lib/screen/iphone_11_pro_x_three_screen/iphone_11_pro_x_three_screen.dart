import 'controller/iphone_11_pro_x_three_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart';

class Iphone11ProXThreeScreen extends GetWidget<Iphone11ProXThreeController> {
  const Iphone11ProXThreeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            backgroundColor: theme.colorScheme.primary,
            body: SizedBox(height: 768.v, width: double.maxFinite)));
  }
}
