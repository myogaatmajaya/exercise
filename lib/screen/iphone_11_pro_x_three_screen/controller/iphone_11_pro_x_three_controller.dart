import 'package:exercise/core/app_export.dart';
import 'package:exercise/screen/iphone_11_pro_x_three_screen/models/iphone_11_pro_x_three_model.dart';

class Iphone11ProXThreeController extends GetxController {
  Rx<Iphone11ProXThreeModel> iphone11ProXThreeModelObj =
      Iphone11ProXThreeModel().obs;

  @override
  void onReady() {
    Future.delayed(const Duration(milliseconds: 3000), () {
      Get.offNamed(
        AppRoutes.registerScreen,
      );
    });
  }
}
