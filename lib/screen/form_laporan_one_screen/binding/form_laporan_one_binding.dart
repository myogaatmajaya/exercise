import '../controller/form_laporan_one_controller.dart';
import 'package:get/get.dart';

class FormLaporanOneBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => FormLaporanOneController());
  }
}
