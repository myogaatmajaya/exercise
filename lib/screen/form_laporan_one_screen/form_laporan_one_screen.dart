import 'controller/form_laporan_one_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/appbar_subtitle.dart';
import 'package:exercise/widgets/app_bar/appbar_title.dart';
import 'package:exercise/widgets/app_bar/appbar_trailing_image.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:exercise/widgets/custom_outlined_button.dart';
import 'package:flutter/material.dart';

// ignore_for_file: must_be_immutable
class FormLaporanOneScreen extends GetWidget<FormLaporanOneController> {
  const FormLaporanOneScreen({Key? key})
      : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        appBar: _buildAppBar(),
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(
            horizontal: 20.h,
            vertical: 14.v,
          ),
          child: Column(
            children: [
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 1.h),
                  padding: EdgeInsets.symmetric(
                    horizontal: 19.h,
                    vertical: 73.v,
                  ),
                  decoration: AppDecoration.outlineIndigo501,
                  child: Column(
                    children: [
                      Divider(
                        color: appTheme.blueGray90002.withOpacity(0.26),
                      ),
                      Spacer(
                        flex: 55,
                      ),
                      Divider(
                        color: appTheme.blueGray90002.withOpacity(0.26),
                      ),
                      Spacer(
                        flex: 44,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20.v),
              _buildBatalRow(),
              SizedBox(height: 5.v),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  PreferredSizeWidget _buildAppBar() {
    return CustomAppBar(
      height: 41.v,
      title: Container(
        height: 21.999996.v,
        width: 192.h,
        margin: EdgeInsets.only(left: 16.h),
        child: Stack(
          alignment: Alignment.center,
          children: [
            AppbarTitle(
              text: "lbl_form".tr,
              margin: EdgeInsets.only(left: 151.h),
            ),
            AppbarSubtitle(
              text: "msg_laporan_kegiatan".tr,
              margin: EdgeInsets.only(top: 1.v),
            ),
          ],
        ),
      ),
      actions: [
        Padding(
          padding: EdgeInsets.only(
            left: 8.h,
            right: 40.h,
          ),
          child: Column(
            children: [
              Container(
                width: 9.h,
                margin: EdgeInsets.only(left: 110.h),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    CustomImageView(
                      imagePath: ImageConstant.imgTelevision,
                      height: 7.v,
                      width: 9.h,
                      alignment: Alignment.center,
                    ),
                    CustomImageView(
                      imagePath: ImageConstant.imgTelevision,
                      height: 7.v,
                      width: 9.h,
                      alignment: Alignment.center,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 5.v),
              AppbarTrailingImage(
                imagePath: ImageConstant.imgArrowLeftBlack900,
                margin: EdgeInsets.only(right: 103.h),
              ),
            ],
          ),
        ),
      ],
    );
  }

  /// Section Widget
  Widget _buildBatalRow() {
    return Padding(
      padding: EdgeInsets.only(left: 1.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: CustomOutlinedButton(
              height: 40.v,
              text: "lbl_batal".tr,
              margin: EdgeInsets.only(right: 5.h),
              buttonStyle: CustomButtonStyles.outlinePrimary,
              buttonTextStyle: CustomTextStyles.titleMediumPrimary,
            ),
          ),
          Expanded(
            child: CustomElevatedButton(
              text: "lbl_kirim".tr,
              margin: EdgeInsets.only(left: 5.h),
            ),
          ),
        ],
      ),
    );
  }
}
