import '../controller/hasil_pencarian_controller.dart';
import '../models/userprofile2_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Userprofile2ItemWidget extends StatelessWidget {
  Userprofile2ItemWidget(
    this.userprofile2ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Userprofile2ItemModel userprofile2ItemModelObj;

  var controller = Get.find<HasilPencarianController>();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Obx(
          () => CustomImageView(
            imagePath: userprofile2ItemModelObj.userImage!.value,
            height: 32.adaptSize,
            width: 32.adaptSize,
            radius: BorderRadius.circular(
              16.h,
            ),
            margin: EdgeInsets.only(bottom: 8.v),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 12.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(
                () => Text(
                  userprofile2ItemModelObj.username!.value,
                  style: theme.textTheme.titleSmall,
                ),
              ),
              SizedBox(height: 1.v),
              Obx(
                () => Text(
                  userprofile2ItemModelObj.subtitle!.value,
                  style: theme.textTheme.bodySmall,
                ),
              ),
            ],
          ),
        ),
        Spacer(),
        Padding(
          padding: EdgeInsets.only(top: 5.v),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  height: 15.adaptSize,
                  width: 15.adaptSize,
                  margin: EdgeInsets.only(right: 25.h),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      7.h,
                    ),
                    border: Border.all(
                      color: appTheme.whiteA700.withOpacity(0.2),
                      width: 2.h,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 3.v),
              Obx(
                () => Text(
                  userprofile2ItemModelObj.timestamp!.value,
                  style: theme.textTheme.bodySmall,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
