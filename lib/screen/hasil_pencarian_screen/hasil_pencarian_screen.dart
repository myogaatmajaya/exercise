import 'widgets/userprofile2_item_widget.dart';
import 'controller/hasil_pencarian_controller.dart';
import 'models/userprofile2_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/appbar_title_searchview.dart';
import 'package:exercise/widgets/app_bar/appbar_trailing_image.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:exercise/widgets/custom_outlined_button.dart';
import 'package:flutter/material.dart';

// ignore_for_file: must_be_immutable
class HasilPencarianScreen extends GetWidget<HasilPencarianController> {
  const HasilPencarianScreen({Key? key})
      : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SizedBox(
          width: double.maxFinite,
          child: Column(
            children: [
              _buildTwentyOne(),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20.v),
                child: Column(
                  children: [
                    _buildHorizontalScroll(),
                    SizedBox(height: 20.v),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.only(left: 16.h),
                        child: Text(
                          "lbl_hasil_pencarian".tr,
                          style: CustomTextStyles.titleMediumBlack900,
                        ),
                      ),
                    ),
                    SizedBox(height: 16.v),
                    _buildUserProfile(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildTwentyOne() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            ImageConstant.imgGroup21,
          ),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: [
          SizedBox(height: 12.v),
          CustomAppBar(
            height: 100.v,
            title: AppbarTitleSearchview(
              margin: EdgeInsets.only(left: 16.h),
              hintText: "lbl_paslon_kaltim".tr,
              controller: controller.searchController,
            ),
            actions: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.h),
                child: Column(
                  children: [
                    AppbarTrailingImage(
                      imagePath: ImageConstant.imgTelevision,
                      margin: EdgeInsets.only(
                        left: 130.h,
                        right: 24.h,
                      ),
                    ),
                    SizedBox(height: 11.v),
                    AppbarTrailingImage(
                      imagePath: ImageConstant.imgFrameWhiteA700,
                      margin: EdgeInsets.only(left: 135.h),
                    ),
                    SizedBox(height: 3.v),
                    AppbarTrailingImage(
                      imagePath: ImageConstant.imgTelevision,
                      margin: EdgeInsets.only(right: 154.h),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildHorizontalScroll() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.only(left: 16.h),
      child: IntrinsicWidth(
        child: SizedBox(
          height: 31.v,
          width: 359.h,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  width: 46.h,
                  padding: EdgeInsets.symmetric(vertical: 3.v),
                  decoration: AppDecoration.outlineOnError.copyWith(
                    borderRadius: BorderRadiusStyle.roundedBorder6,
                  ),
                  child: Text(
                    "lbl_minggu_ini2".tr,
                    style: CustomTextStyles.bodyMediumOnError,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 31.v,
                  width: 359.h,
                  child: Stack(
                    alignment: Alignment.centerRight,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              height: 31.v,
                              width: 83.h,
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      height: 31.v,
                                      width: 83.h,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                          6.h,
                                        ),
                                        border: Border.all(
                                          color: appTheme.indigo5001,
                                          width: 1.h,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.center,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        CustomImageView(
                                          imagePath:
                                              ImageConstant.imgCiFilterBlack900,
                                          height: 14.adaptSize,
                                          width: 14.adaptSize,
                                          margin: EdgeInsets.only(
                                            top: 2.v,
                                            bottom: 3.v,
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 4.h),
                                          child: Text(
                                            "lbl_filter".tr,
                                            style: theme.textTheme.bodyMedium,
                                          ),
                                        ),
                                        Container(
                                          width: 16.h,
                                          margin: EdgeInsets.only(
                                            left: 4.h,
                                            top: 2.v,
                                          ),
                                          padding: EdgeInsets.symmetric(
                                            horizontal: 5.h,
                                            vertical: 1.v,
                                          ),
                                          decoration: AppDecoration.fillPrimary
                                              .copyWith(
                                            borderRadius: BorderRadiusStyle
                                                .roundedBorder6,
                                          ),
                                          child: Text(
                                            "lbl_1".tr,
                                            style: CustomTextStyles
                                                .bodySmallWhiteA700,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            CustomImageView(
                              imagePath: ImageConstant.imgCheckmark,
                              height: 12.adaptSize,
                              width: 12.adaptSize,
                              margin: EdgeInsets.symmetric(vertical: 9.v),
                            ),
                          ],
                        ),
                      ),
                      CustomOutlinedButton(
                        height: 31.v,
                        width: 222.h,
                        text: "msg_01_jan_2021_29".tr,
                        margin: EdgeInsets.only(right: 50.h),
                        rightIcon: Container(
                          margin: EdgeInsets.only(left: 6.h),
                          child: CustomImageView(
                            imagePath: ImageConstant.imgCheckmark,
                            height: 12.adaptSize,
                            width: 12.adaptSize,
                          ),
                        ),
                        buttonStyle: CustomButtonStyles.outlineOnError,
                        buttonTextStyle: CustomTextStyles.bodyMediumOnError,
                        alignment: Alignment.centerRight,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildUserProfile() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.h),
      child: Obx(
        () => ListView.separated(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          separatorBuilder: (
            context,
            index,
          ) {
            return Padding(
              padding: EdgeInsets.symmetric(vertical: 7.0.v),
              child: SizedBox(
                width: 343.h,
                child: Divider(
                  height: 1.v,
                  thickness: 1.v,
                  color: appTheme.indigo50,
                ),
              ),
            );
          },
          itemCount: controller
              .hasilPencarianModelObj.value.userprofile2ItemList.value.length,
          itemBuilder: (context, index) {
            Userprofile2ItemModel model = controller
                .hasilPencarianModelObj.value.userprofile2ItemList.value[index];
            return Userprofile2ItemWidget(
              model,
            );
          },
        ),
      ),
    );
  }
}
