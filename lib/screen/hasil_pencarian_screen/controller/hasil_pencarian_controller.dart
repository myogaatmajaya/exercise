import 'package:exercise/core/app_export.dart';
import 'package:exercise/screen/hasil_pencarian_screen/models/hasil_pencarian_model.dart';
import 'package:flutter/material.dart';

class HasilPencarianController extends GetxController {
  TextEditingController searchController = TextEditingController();

  Rx<HasilPencarianModel> hasilPencarianModelObj = HasilPencarianModel().obs;

  @override
  void onClose() {
    super.onClose();
    searchController.dispose();
  }
}
