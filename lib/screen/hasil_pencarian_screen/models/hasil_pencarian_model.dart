import '../../../core/app_export.dart';
import 'userprofile2_item_model.dart';

class HasilPencarianModel {
  Rx<List<Userprofile2ItemModel>> userprofile2ItemList = Rx([
    Userprofile2ItemModel(
        userImage: ImageConstant.imgEllipse78.obs,
        username: "Brooklyn Simmons".obs,
        subtitle: "Safarudin".obs,
        timestamp: "29 Maret 2021 14:30".obs),
    Userprofile2ItemModel(
        userImage: ImageConstant.imgEllipse7832x32.obs,
        username: "Kristin Watson".obs,
        subtitle: "Safarudin".obs),
    Userprofile2ItemModel(
        userImage: ImageConstant.imgEllipse781.obs,
        username: "Bessie Cooper".obs,
        subtitle: "Margareth".obs),
    Userprofile2ItemModel(
        userImage: ImageConstant.imgEllipse782.obs,
        username: "Theresa Webb".obs,
        subtitle: "Margareth".obs),
    Userprofile2ItemModel(
        userImage: ImageConstant.imgEllipse783.obs,
        username: "Albert Flores".obs,
        subtitle: "Safarudin".obs)
  ]);
}
