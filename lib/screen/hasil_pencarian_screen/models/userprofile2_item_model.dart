import '../../../core/app_export.dart';

class Userprofile2ItemModel {
  Userprofile2ItemModel({
    this.userImage,
    this.username,
    this.subtitle,
    this.timestamp,
    this.id,
  }) {
    userImage = userImage ?? Rx(ImageConstant.imgEllipse78);
    username = username ?? Rx("Brooklyn Simmons");
    subtitle = subtitle ?? Rx("Safarudin");
    timestamp = timestamp ?? Rx("29 Maret 2021 14:30");
    id = id ?? Rx("");
  }

  Rx<String>? userImage;

  Rx<String>? username;

  Rx<String>? subtitle;

  Rx<String>? timestamp;

  Rx<String>? id;
}
