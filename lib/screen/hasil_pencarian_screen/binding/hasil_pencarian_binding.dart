import '../controller/hasil_pencarian_controller.dart';
import 'package:get/get.dart';

class HasilPencarianBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HasilPencarianController());
  }
}
