import '../controller/data_gagal_terkirim_controller.dart';
import 'package:get/get.dart';

class DataGagalTerkirimBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DataGagalTerkirimController());
  }
}
