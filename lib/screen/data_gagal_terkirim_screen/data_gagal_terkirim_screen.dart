import 'controller/data_gagal_terkirim_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:flutter/material.dart';

// ignore_for_file: must_be_immutable
class DataGagalTerkirimScreen extends GetWidget<DataGagalTerkirimController> {
  const DataGagalTerkirimScreen({Key? key})
      : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(horizontal: 19.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgIllustrations,
                height: 200.adaptSize,
                width: 200.adaptSize,
              ),
              SizedBox(height: 18.v),
              Text(
                "msg_data_gagal_terkirim".tr,
                style: theme.textTheme.titleLarge,
              ),
              SizedBox(height: 6.v),
              Container(
                width: 241.h,
                margin: EdgeInsets.only(
                  left: 47.h,
                  right: 48.h,
                ),
                child: Text(
                  "msg_whoops_sepertinya".tr,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: CustomTextStyles.bodyMediumBluegray900d3.copyWith(
                    height: 1.52,
                  ),
                ),
              ),
              SizedBox(height: 27.v),
              CustomElevatedButton(
                text: "lbl_coba_lagi".tr,
              ),
              SizedBox(height: 5.v),
            ],
          ),
        ),
      ),
    );
  }
}
