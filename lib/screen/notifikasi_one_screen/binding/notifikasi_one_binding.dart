import '../controller/notifikasi_one_controller.dart';
import 'package:get/get.dart';

class NotifikasiOneBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NotifikasiOneController());
  }
}
