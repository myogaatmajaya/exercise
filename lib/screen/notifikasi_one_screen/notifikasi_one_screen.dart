import 'controller/notifikasi_one_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart';

class NotifikasiOneScreen extends GetWidget<NotifikasiOneController> {
  const NotifikasiOneScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.maxFinite,
                child: Column(children: [
                  _buildTelevisionStack(),
                  SizedBox(height: 17.v),
                  _buildTerbaruRow(),
                  SizedBox(height: 3.v),
                  _buildViewColumn(),
                  _buildViewStack(),
                  SizedBox(height: 16.v),
                  _buildPilpresCounterRow(),
                  SizedBox(height: 17.v),
                  _buildThirtyFourColumn(),
                  SizedBox(height: 5.v)
                ]))));
  }

  /// Section Widget
  Widget _buildTelevisionStack() {
    return SizedBox(
        height: 60.v,
        width: double.maxFinite,
        child: Stack(alignment: Alignment.bottomRight, children: [
          Align(
              alignment: Alignment.topCenter,
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.h),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(ImageConstant.imgGroup21),
                          fit: BoxFit.cover)),
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomImageView(
                            imagePath: ImageConstant.imgTelevision,
                            height: 7.v,
                            width: 9.h,
                            alignment: Alignment.centerRight,
                            margin: EdgeInsets.only(right: 23.h)),
                        SizedBox(height: 9.v),
                        Row(children: [
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              onTap: () {
                                onTapImgArrowLeft();
                              }),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 106.h, top: 2.v, bottom: 3.v),
                              child: Text("lbl_notifikasi".tr,
                                  style: theme.textTheme.titleMedium))
                        ]),
                        SizedBox(height: 13.v)
                      ]))),
          CustomImageView(
              imagePath: ImageConstant.imgTelevision,
              height: 11.v,
              width: 9.h,
              alignment: Alignment.bottomRight,
              margin: EdgeInsets.only(right: 169.h))
        ]));
  }

  /// Section Widget
  Widget _buildTerbaruRow() {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.h),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text("lbl_terbaru".tr, style: CustomTextStyles.titleMediumBlack900),
          Padding(
              padding: EdgeInsets.only(top: 2.v),
              child: Text("msg_tandai_sudah_di".tr,
                  style: CustomTextStyles.titleSmallPrimary))
        ]));
  }

  /// Section Widget
  Widget _buildViewColumn() {
    return Column(children: [
      Container(
          padding: EdgeInsets.symmetric(horizontal: 16.h, vertical: 6.v),
          decoration: AppDecoration.fillPrimary1,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                    padding: EdgeInsets.only(left: 61.h, right: 104.h),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              height: 9.adaptSize,
                              width: 9.adaptSize,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4.h),
                                  border: Border.all(
                                      color:
                                          appTheme.whiteA700.withOpacity(0.2),
                                      width: 2.h))),
                          Container(
                              height: 9.adaptSize,
                              width: 9.adaptSize,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4.h),
                                  border: Border.all(
                                      color:
                                          appTheme.whiteA700.withOpacity(0.2),
                                      width: 2.h)))
                        ])),
                SizedBox(height: 1.v),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  CustomImageView(
                      imagePath: ImageConstant.imgEllipse781,
                      height: 32.adaptSize,
                      width: 32.adaptSize,
                      radius: BorderRadius.circular(16.h),
                      margin: EdgeInsets.only(bottom: 48.v)),
                  Expanded(
                      child: Padding(
                          padding: EdgeInsets.only(left: 12.h),
                          child: Column(children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("lbl_pilpres_2024".tr,
                                      style: theme.textTheme.titleSmall),
                                  Text("lbl_1_jam_lalu".tr,
                                      style: theme.textTheme.bodySmall)
                                ]),
                            SizedBox(height: 3.v),
                            SizedBox(
                                height: 55.v,
                                width: 290.h,
                                child: Stack(
                                    alignment: Alignment.topLeft,
                                    children: [
                                      Align(
                                          alignment: Alignment.topRight,
                                          child: Container(
                                              height: 15.adaptSize,
                                              width: 15.adaptSize,
                                              margin: EdgeInsets.only(
                                                  top: 12.v, right: 16.h),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          7.h),
                                                  border: Border.all(
                                                      color: appTheme.whiteA700
                                                          .withOpacity(0.2),
                                                      width: 2.h)))),
                                      CustomImageView(
                                          imagePath:
                                              ImageConstant.imgTelevision,
                                          height: 11.v,
                                          width: 9.h,
                                          alignment: Alignment.topLeft,
                                          margin: EdgeInsets.only(
                                              left: 91.h, top: 12.v)),
                                      Align(
                                          alignment: Alignment.topLeft,
                                          child: Text("lbl_workspace".tr,
                                              style:
                                                  theme.textTheme.labelLarge)),
                                      Align(
                                          alignment: Alignment.bottomCenter,
                                          child: SizedBox(
                                              width: 290.h,
                                              child: Text(
                                                  "msg_selamat_anda_tergabung"
                                                      .tr,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: CustomTextStyles
                                                      .bodySmallBlack900)))
                                    ]))
                          ])))
                ]),
                SizedBox(height: 4.v)
              ])),
      Divider(indent: 16.h, endIndent: 16.h)
    ]);
  }

  /// Section Widget
  Widget _buildViewStack() {
    return SizedBox(
        height: 108.v,
        width: double.maxFinite,
        child: Stack(alignment: Alignment.bottomCenter, children: [
          Align(
              alignment: Alignment.center,
              child: Container(
                  height: 107.v,
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                      color: theme.colorScheme.primary.withOpacity(0.08)))),
          Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.h),
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomImageView(
                              imagePath: ImageConstant.imgEllipse77,
                              height: 32.adaptSize,
                              width: 32.adaptSize,
                              radius: BorderRadius.circular(16.h),
                              margin: EdgeInsets.only(bottom: 48.v)),
                          Expanded(
                              child: Padding(
                                  padding: EdgeInsets.only(left: 12.h),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              _buildPilpresCounterColumn(
                                                  dynamicProperty1:
                                                      "lbl_pilpres_2024".tr,
                                                  dynamicProperty2:
                                                      "lbl_form".tr),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      bottom: 20.v),
                                                  child: Text(
                                                      "lbl_2_jam_lalu".tr,
                                                      style: theme
                                                          .textTheme.bodySmall))
                                            ]),
                                        SizedBox(height: 7.v),
                                        Container(
                                            width: 261.h,
                                            margin:
                                                EdgeInsets.only(right: 36.h),
                                            child: Text(
                                                "msg_form_survey_terkait".tr,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: CustomTextStyles
                                                    .bodySmallBlack900))
                                      ])))
                        ]),
                    SizedBox(height: 10.v),
                    Divider()
                  ])))
        ]));
  }

  /// Section Widget
  Widget _buildPilpresCounterRow() {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.h),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomImageView(
                  imagePath: ImageConstant.imgEllipse7732x32,
                  height: 32.adaptSize,
                  width: 32.adaptSize,
                  radius: BorderRadius.circular(16.h),
                  margin: EdgeInsets.only(bottom: 216.v)),
              Expanded(
                  child: Padding(
                      padding: EdgeInsets.only(left: 12.h),
                      child: Column(children: [
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("lbl_pilpres_2024".tr,
                                  style: theme.textTheme.titleSmall),
                              SizedBox(height: 1.v),
                              Text("lbl_info".tr,
                                  style: theme.textTheme.labelLarge)
                            ]),
                        SizedBox(height: 8.v),
                        Text("msg_form_survey_terkait2".tr,
                            style: CustomTextStyles.bodySmallBlack900),
                        SizedBox(height: 9.v),
                        CustomImageView(
                            imagePath: ImageConstant.imgRectangle3019,
                            height: 173.v,
                            width: 299.h,
                            radius: BorderRadius.circular(6.h))
                      ])))
            ]));
  }

  /// Section Widget
  Widget _buildThirtyFourColumn() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 16.h),
        decoration: AppDecoration.fillPrimary1,
        child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
          SizedBox(height: 15.v),
          Column(children: [
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomImageView(
                      imagePath: ImageConstant.imgEllipse771,
                      height: 32.adaptSize,
                      width: 32.adaptSize,
                      radius: BorderRadius.circular(16.h),
                      margin: EdgeInsets.only(bottom: 8.v)),
                  Padding(
                      padding: EdgeInsets.only(left: 12.h),
                      child: _buildPilpresCounterColumn(
                          dynamicProperty1: "lbl_pilpres_2024".tr,
                          dynamicProperty2: "lbl_form".tr)),
                  Spacer(),
                  Padding(
                      padding: EdgeInsets.only(top: 3.v, bottom: 20.v),
                      child: Text("lbl_2_jam_lalu".tr,
                          style: theme.textTheme.bodySmall))
                ]),
            SizedBox(height: 7.v),
            Container(
                width: 261.h,
                margin: EdgeInsets.only(left: 44.h, right: 37.h),
                child: Text("msg_form_survey_terkait".tr,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: CustomTextStyles.bodySmallBlack900))
          ]),
          SizedBox(height: 10.v),
          Divider()
        ]));
  }

  /// Common widget
  Widget _buildPilpresCounterColumn({
    required String dynamicProperty1,
    required String dynamicProperty2,
  }) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(dynamicProperty1,
          style:
              theme.textTheme.titleSmall!.copyWith(color: appTheme.black900)),
      SizedBox(height: 1.v),
      Text(dynamicProperty2,
          style: theme.textTheme.labelLarge!
              .copyWith(color: theme.colorScheme.primary))
    ]);
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft() {
    Get.back();
  }
}
