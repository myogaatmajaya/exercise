import '../controller/workspace_controller.dart';
import 'package:get/get.dart';

class WorkspaceBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => WorkspaceController());
  }
}
