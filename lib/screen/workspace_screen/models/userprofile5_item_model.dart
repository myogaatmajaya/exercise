import '../../../core/app_export.dart';

class Userprofile5ItemModel {
  Userprofile5ItemModel({
    this.userImage,
    this.elektabilitasPaslon,
    this.ametMinimMollit,
    this.timestamp,
    this.clientName,
    this.id,
  }) {
    userImage = userImage ?? Rx(ImageConstant.imgRectangle3016);
    elektabilitasPaslon =
        elektabilitasPaslon ?? Rx("Elektabilitas Paslon Kaltim");
    ametMinimMollit =
        ametMinimMollit ?? Rx("Amet minim mollit non deserunt est sit... ");
    timestamp = timestamp ?? Rx("23/03/22 16:36");
    clientName = clientName ?? Rx("Klien A");
    id = id ?? Rx("");
  }

  Rx<String>? userImage;

  Rx<String>? elektabilitasPaslon;

  Rx<String>? ametMinimMollit;

  Rx<String>? timestamp;

  Rx<String>? clientName;

  Rx<String>? id;
}
