import '../../../core/app_export.dart';
import 'userprofile5_item_model.dart';

class WorkspaceModel {
  Rx<List<Userprofile5ItemModel>> userprofile5ItemList = Rx([
    Userprofile5ItemModel(
        userImage: ImageConstant.imgRectangle3016.obs,
        elektabilitasPaslon: "Elektabilitas Paslon Kaltim".obs,
        ametMinimMollit: "Amet minim mollit non deserunt est sit... ".obs,
        timestamp: "23/03/22 16:36".obs,
        clientName: "Klien A".obs),
    Userprofile5ItemModel(
        userImage: ImageConstant.imgRectangle301660x60.obs,
        elektabilitasPaslon: "Laporan Covid".obs,
        ametMinimMollit: "Amet minim mollit non deserunt est sit... ".obs,
        timestamp: "23/03/22 16:36".obs,
        clientName: "Klien A".obs)
  ]);
}
