import 'widgets/userprofile5_item_widget.dart';
import 'controller/workspace_controller.dart';
import 'models/userprofile5_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:exercise/widgets/custom_icon_button.dart';
import 'package:flutter/material.dart';

class WorkspaceScreen extends GetWidget<WorkspaceController> {
  const WorkspaceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.maxFinite,
                child: Column(children: [
                  _buildTelevisionStack(),
                  Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 15.h, vertical: 18.v),
                      child: Column(children: [
                        _buildFrameColumn(),
                        SizedBox(height: 19.v),
                        _buildUserProfileColumn(),
                        SizedBox(height: 5.v)
                      ]))
                ]))));
  }

  /// Section Widget
  Widget _buildTelevisionStack() {
    return SizedBox(
        height: 257.v,
        width: double.maxFinite,
        child: Stack(alignment: Alignment.center, children: [
          Align(
              alignment: Alignment.topLeft,
              child: Padding(
                  padding: EdgeInsets.only(left: 16.h),
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        CustomAppBar(
                            leadingWidth: double.maxFinite,
                            leading: Container(
                                height: 28.adaptSize,
                                width: 28.adaptSize,
                                margin: EdgeInsets.only(
                                    left: 16.h, top: 9.v, right: 331.h),
                                child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      CustomImageView(
                                          imagePath: ImageConstant
                                              .imgArrowLeftWhiteA700,
                                          height: 28.adaptSize,
                                          width: 28.adaptSize,
                                          alignment: Alignment.center,
                                          onTap: () {
                                            onTapImgArrowLeft();
                                          }),
                                      CustomImageView(
                                          imagePath: ImageConstant
                                              .imgArrowLeftWhiteA700,
                                          height: 28.adaptSize,
                                          width: 28.adaptSize,
                                          alignment: Alignment.center)
                                    ])),
                            actions: [
                              Container(
                                  height: 7.v,
                                  width: 9.h,
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 40.h),
                                  child: Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        CustomImageView(
                                            imagePath:
                                                ImageConstant.imgTelevision,
                                            height: 7.v,
                                            width: 9.h,
                                            alignment: Alignment.center),
                                        CustomImageView(
                                            imagePath:
                                                ImageConstant.imgTelevision,
                                            height: 7.v,
                                            width: 9.h,
                                            alignment: Alignment.center)
                                      ]))
                            ]),
                        SizedBox(height: 5.v),
                        CustomImageView(
                            imagePath: ImageConstant.imgTelevision,
                            height: 11.v,
                            width: 9.h,
                            margin: EdgeInsets.only(right: 130.h)),
                        SizedBox(height: 58.v),
                        CustomImageView(
                            imagePath: ImageConstant.imgTelevision,
                            height: 11.v,
                            width: 9.h,
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(left: 135.h))
                      ]))),
          Align(
              alignment: Alignment.center,
              child: Container(
                  padding: EdgeInsets.all(13.h),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(ImageConstant.imgGroup49),
                          fit: BoxFit.cover)),
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(children: [
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize),
                          Padding(
                              padding: EdgeInsets.only(left: 101.h, top: 4.v),
                              child: Text("lbl_workspace".tr,
                                  style: theme.textTheme.titleMedium))
                        ]),
                        SizedBox(height: 5.v),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Padding(
                                padding:
                                    EdgeInsets.only(left: 40.h, right: 25.h),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                          height: 185.v,
                                          width: 245.h,
                                          child: Stack(
                                              alignment: Alignment.topRight,
                                              children: [
                                                Align(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    child: Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 14.h,
                                                                top: 30.v),
                                                        child: Column(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .min,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              SizedBox(
                                                                  width: 178.h,
                                                                  child: Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      children: [
                                                                        Container(
                                                                            decoration:
                                                                                AppDecoration.outlineWhiteA.copyWith(borderRadius: BorderRadiusStyle.roundedBorder6),
                                                                            child: Container(height: 9.adaptSize, width: 9.adaptSize, decoration: BoxDecoration(borderRadius: BorderRadius.circular(4.h), border: Border.all(color: appTheme.whiteA700.withOpacity(0.2), width: 2.h)))),
                                                                        Container(
                                                                            decoration:
                                                                                AppDecoration.outlineWhiteA.copyWith(borderRadius: BorderRadiusStyle.roundedBorder6),
                                                                            child: Container(height: 9.adaptSize, width: 9.adaptSize, decoration: BoxDecoration(borderRadius: BorderRadius.circular(4.h), border: Border.all(color: appTheme.whiteA700.withOpacity(0.2), width: 2.h))))
                                                                      ])),
                                                              SizedBox(
                                                                  height: 30.v),
                                                              CustomImageView(
                                                                  imagePath:
                                                                      ImageConstant
                                                                          .imgTelevision,
                                                                  height: 11.v,
                                                                  width: 9.h,
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              74.h))
                                                            ]))),
                                                CustomImageView(
                                                    imagePath: ImageConstant
                                                        .imgTelevision,
                                                    height: 11.v,
                                                    width: 9.h,
                                                    alignment:
                                                        Alignment.topRight,
                                                    margin: EdgeInsets.only(
                                                        right: 102.h)),
                                                Align(
                                                    alignment: Alignment.center,
                                                    child: Column(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: [
                                                          CustomImageView(
                                                              imagePath:
                                                                  ImageConstant
                                                                      .imgRectangle3009,
                                                              height: 100
                                                                  .adaptSize,
                                                              width:
                                                                  100.adaptSize,
                                                              radius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          8.h)),
                                                          SizedBox(
                                                              height: 12.v),
                                                          Text("lbl_klien_b".tr,
                                                              style: theme
                                                                  .textTheme
                                                                  .titleMedium),
                                                          SizedBox(height: 8.v),
                                                          SizedBox(
                                                              width: 245.h,
                                                              child: Text(
                                                                  "msg_ini_berisikan_deskripsi"
                                                                      .tr,
                                                                  maxLines: 2,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                  textAlign:
                                                                      TextAlign
                                                                          .center,
                                                                  style: CustomTextStyles
                                                                      .bodySmallWhiteA700_2))
                                                        ]))
                                              ])),
                                      Container(
                                          margin: EdgeInsets.only(
                                              left: 10.h,
                                              top: 69.v,
                                              bottom: 101.v),
                                          decoration: AppDecoration
                                              .outlineWhiteA
                                              .copyWith(
                                                  borderRadius:
                                                      BorderRadiusStyle
                                                          .roundedBorder6),
                                          child: Container(
                                              height: 15.adaptSize,
                                              width: 15.adaptSize,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          7.h),
                                                  border: Border.all(
                                                      color: appTheme.whiteA700
                                                          .withOpacity(0.2),
                                                      width: 2.h))))
                                    ]))),
                        SizedBox(height: 6.v)
                      ])))
        ]));
  }

  /// Section Widget
  Widget _buildFrameColumn() {
    return Column(children: [
      _buildCalendarRow(
          createdDate: "lbl_pembuat".tr,
          eventDate: "msg_subroto_suroto_ebdesk".tr),
      SizedBox(height: 12.v),
      Divider(),
      SizedBox(height: 16.v),
      _buildCalendarRow(
          createdDate: "lbl_tanggal_dibuat".tr,
          eventDate: "lbl_12_march_2022".tr),
      SizedBox(height: 12.v),
      Divider(),
      SizedBox(height: 16.v),
      _buildCalendarRow(
          createdDate: "lbl_status".tr, eventDate: "lbl_aktif".tr),
      SizedBox(height: 12.v),
      Divider()
    ]);
  }

  /// Section Widget
  Widget _buildUserProfileColumn() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text("lbl_formulir".tr, style: theme.textTheme.titleSmall),
      SizedBox(height: 15.v),
      Obx(() => ListView.separated(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          separatorBuilder: (context, index) {
            return SizedBox(height: 8.v);
          },
          itemCount: controller
              .workspaceModelObj.value.userprofile5ItemList.value.length,
          itemBuilder: (context, index) {
            Userprofile5ItemModel model = controller
                .workspaceModelObj.value.userprofile5ItemList.value[index];
            return Userprofile5ItemWidget(model);
          }))
    ]);
  }

  /// Common widget
  Widget _buildCalendarRow({
    required String createdDate,
    required String eventDate,
  }) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      CustomIconButton(
          height: 32.adaptSize,
          width: 32.adaptSize,
          padding: EdgeInsets.all(8.h),
          decoration: IconButtonStyleHelper.fillCyan,
          child: CustomImageView(imagePath: ImageConstant.imgBiCalendarFill)),
      Padding(
          padding: EdgeInsets.only(left: 12.h, top: 7.v, bottom: 4.v),
          child: Text(createdDate,
              style: theme.textTheme.bodyMedium!
                  .copyWith(color: appTheme.black900))),
      Spacer(),
      Padding(
          padding: EdgeInsets.only(top: 6.v, bottom: 5.v),
          child: Text(eventDate,
              style: theme.textTheme.bodyMedium!
                  .copyWith(color: appTheme.black900)))
    ]);
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft() {
    Get.back();
  }
}
