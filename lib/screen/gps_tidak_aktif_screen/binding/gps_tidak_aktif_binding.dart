import '../controller/gps_tidak_aktif_controller.dart';
import 'package:get/get.dart';

class GpsTidakAktifBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => GpsTidakAktifController());
  }
}
