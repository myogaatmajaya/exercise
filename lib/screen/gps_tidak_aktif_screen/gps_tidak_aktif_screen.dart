import 'controller/gps_tidak_aktif_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:flutter/material.dart';

// ignore_for_file: must_be_immutable
class GpsTidakAktifScreen extends GetWidget<GpsTidakAktifController> {
  const GpsTidakAktifScreen({Key? key})
      : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(horizontal: 19.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgCurrentLocationPana,
                height: 200.v,
                width: 227.h,
              ),
              SizedBox(height: 29.v),
              Text(
                "lbl_gps_tidak_aktif".tr,
                style: theme.textTheme.titleLarge,
              ),
              SizedBox(height: 8.v),
              Container(
                width: 265.h,
                margin: EdgeInsets.only(
                  left: 35.h,
                  right: 36.h,
                ),
                child: Text(
                  "msg_whoops_sepertinya2".tr,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: CustomTextStyles.bodyMediumBluegray900d3.copyWith(
                    height: 1.52,
                  ),
                ),
              ),
              SizedBox(height: 27.v),
              CustomElevatedButton(
                text: "msg_pergi_ke_pengaturan".tr,
              ),
              SizedBox(height: 5.v),
            ],
          ),
        ),
      ),
    );
  }
}
