import '../controller/lokasi_tidak_terdeteksi_controller.dart';
import 'package:get/get.dart';

class LokasiTidakTerdeteksiBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LokasiTidakTerdeteksiController());
  }
}
