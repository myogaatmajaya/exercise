import 'controller/lokasi_tidak_terdeteksi_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:flutter/material.dart';

// ignore_for_file: must_be_immutable
class LokasiTidakTerdeteksiScreen
    extends GetWidget<LokasiTidakTerdeteksiController> {
  const LokasiTidakTerdeteksiScreen({Key? key})
      : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(horizontal: 19.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgUndrawCurrent,
                height: 200.v,
                width: 214.h,
              ),
              SizedBox(height: 29.v),
              Text(
                "msg_lokasi_anda_tidak".tr,
                style: theme.textTheme.titleLarge,
              ),
              SizedBox(height: 8.v),
              SizedBox(
                width: 233.h,
                child: Text(
                  "msg_sepertinya_lokasi".tr,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: CustomTextStyles.bodyMediumBluegray900d3.copyWith(
                    height: 1.52,
                  ),
                ),
              ),
              SizedBox(height: 28.v),
              CustomElevatedButton(
                text: "lbl_kembali".tr,
              ),
              SizedBox(height: 5.v),
            ],
          ),
        ),
      ),
    );
  }
}
