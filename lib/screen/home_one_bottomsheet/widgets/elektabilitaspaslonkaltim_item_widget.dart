import '../models/elektabilitaspaslonkaltim_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ElektabilitaspaslonkaltimItemWidget extends StatelessWidget {
  ElektabilitaspaslonkaltimItemWidget(
    this.elektabilitaspaslonkaltimItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  ElektabilitaspaslonkaltimItemModel elektabilitaspaslonkaltimItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => RawChip(
        padding: EdgeInsets.symmetric(
          horizontal: 12.h,
          vertical: 5.v,
        ),
        showCheckmark: false,
        labelPadding: EdgeInsets.zero,
        label: Text(
          elektabilitaspaslonkaltimItemModelObj
              .elektabilitasPaslonKaltim!.value,
          style: TextStyle(
            color: (elektabilitaspaslonkaltimItemModelObj.isSelected?.value ??
                    false)
                ? theme.colorScheme.primary
                : appTheme.blueGray300,
            fontSize: 14.fSize,
            fontFamily: 'Open Sans',
            fontWeight: FontWeight.w400,
          ),
        ),
        selected:
            (elektabilitaspaslonkaltimItemModelObj.isSelected?.value ?? false),
        backgroundColor: appTheme.gray50,
        selectedColor: theme.colorScheme.primary.withOpacity(0.1),
        shape:
            (elektabilitaspaslonkaltimItemModelObj.isSelected?.value ?? false)
                ? RoundedRectangleBorder(
                    side: BorderSide.none,
                    borderRadius: BorderRadius.circular(
                      6.h,
                    ),
                  )
                : RoundedRectangleBorder(
                    side: BorderSide.none,
                    borderRadius: BorderRadius.circular(
                      6.h,
                    ),
                  ),
        onSelected: (value) {
          elektabilitaspaslonkaltimItemModelObj.isSelected!.value = value;
        },
      ),
    );
  }
}
