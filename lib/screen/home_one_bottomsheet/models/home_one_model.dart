import 'elektabilitaspaslonkaltim_item_model.dart';
import 'frame_item_model.dart';
import '../../../core/app_export.dart';

class HomeOneModel {
  Rx<List<ElektabilitaspaslonkaltimItemModel>>
      elektabilitaspaslonkaltimItemList =
      Rx(List.generate(3, (index) => ElektabilitaspaslonkaltimItemModel()));

  Rx<List<FrameItemModel>> frameItemList =
      Rx(List.generate(4, (index) => FrameItemModel()));
}
