import '../../../core/app_export.dart';

class ElektabilitaspaslonkaltimItemModel {
  Rx<String>? elektabilitasPaslonKaltim = Rx("Elektabilitas Paslon Kaltim");

  Rx<bool>? isSelected = Rx(false);
}
