import 'controller/edit_profile_one_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/appbar_title.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:flutter/material.dart';

class EditProfileOneScreen extends GetWidget<EditProfileOneController> {
  const EditProfileOneScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.maxFinite,
                child: Column(children: [
                  _buildArrowLeftStack(),
                  Container(
                      height: 721.v,
                      width: double.maxFinite,
                      padding: EdgeInsets.symmetric(vertical: 2.v),
                      child: Stack(alignment: Alignment.topRight, children: [
                        Padding(
                            padding: EdgeInsets.only(right: 169.h),
                            child: _buildFortyFour()),
                        Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                                padding:
                                    EdgeInsets.only(top: 30.v, right: 41.h),
                                child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(children: [
                                        Container(
                                            decoration: AppDecoration
                                                .outlineWhiteA
                                                .copyWith(
                                                    borderRadius:
                                                        BorderRadiusStyle
                                                            .roundedBorder6),
                                            child: Container(
                                                height: 9.adaptSize,
                                                width: 9.adaptSize,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.h),
                                                    border: Border.all(
                                                        color: appTheme
                                                            .whiteA700
                                                            .withOpacity(0.2),
                                                        width: 2.h)))),
                                        Container(
                                            margin:
                                                EdgeInsets.only(left: 160.h),
                                            decoration: AppDecoration
                                                .outlineWhiteA
                                                .copyWith(
                                                    borderRadius:
                                                        BorderRadiusStyle
                                                            .roundedBorder6),
                                            child: Container(
                                                height: 9.adaptSize,
                                                width: 9.adaptSize,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.h),
                                                    border: Border.all(
                                                        color: appTheme
                                                            .whiteA700
                                                            .withOpacity(0.2),
                                                        width: 2.h))))
                                      ]),
                                      SizedBox(height: 30.v),
                                      Align(
                                          alignment: Alignment.centerRight,
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        bottom: 4.v),
                                                    child: _buildFortyFour()),
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        left: 158.h),
                                                    decoration: AppDecoration
                                                        .outlineWhiteA
                                                        .copyWith(
                                                            borderRadius:
                                                                BorderRadiusStyle
                                                                    .roundedBorder6),
                                                    child: Container(
                                                        height: 15.adaptSize,
                                                        width: 15.adaptSize,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius.circular(
                                                                    7.h),
                                                            border: Border.all(
                                                                color: appTheme
                                                                    .whiteA700
                                                                    .withOpacity(
                                                                        0.2),
                                                                width: 2.h))))
                                              ]))
                                    ]))),
                        _buildFrame3()
                      ]))
                ]))));
  }

  /// Section Widget
  Widget _buildArrowLeftStack() {
    return SizedBox(
        height: 47.v,
        width: double.maxFinite,
        child: Stack(alignment: Alignment.topCenter, children: [
          Align(
              alignment: Alignment.center,
              child: Container(
                  width: double.maxFinite,
                  padding: EdgeInsets.fromLTRB(16.h, 11.v, 16.h, 10.v),
                  decoration: AppDecoration.outlineIndigo502,
                  child: Row(children: [
                    CustomImageView(
                        imagePath: ImageConstant.imgArrowLeft,
                        height: 24.adaptSize,
                        width: 24.adaptSize,
                        margin: EdgeInsets.only(top: 1.v),
                        onTap: () {
                          onTapImgArrowLeft();
                        }),
                    Padding(
                        padding: EdgeInsets.only(left: 106.h, bottom: 4.v),
                        child: Text("lbl_edit_profile".tr,
                            style: CustomTextStyles.titleMediumInterBlack900))
                  ]))),
          CustomAppBar(
              leadingWidth: 44.h,
              leading: Container(
                  height: 28.adaptSize,
                  width: 28.adaptSize,
                  margin: EdgeInsets.only(left: 16.h, top: 16.v),
                  child: Stack(children: [
                    CustomImageView(
                        imagePath: ImageConstant.imgArrowLeftWhiteA700,
                        height: 28.adaptSize,
                        width: 28.adaptSize,
                        alignment: Alignment.center,
                        onTap: () {
                          onTapImgArrowLeft1();
                        }),
                    SizedBox(
                        height: 28.adaptSize,
                        width: 28.adaptSize,
                        child: Stack(alignment: Alignment.center, children: [
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              alignment: Alignment.center),
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              alignment: Alignment.center)
                        ]))
                  ])),
              centerTitle: true,
              title: AppbarTitle(text: "lbl_form".tr),
              actions: [
                Container(
                    height: 7.v,
                    width: 9.h,
                    margin:
                        EdgeInsets.only(left: 40.h, right: 40.h, bottom: 37.v),
                    child: Stack(alignment: Alignment.center, children: [
                      CustomImageView(
                          imagePath: ImageConstant.imgTelevision,
                          height: 7.v,
                          width: 9.h,
                          alignment: Alignment.center),
                      CustomImageView(
                          imagePath: ImageConstant.imgTelevision,
                          height: 7.v,
                          width: 9.h,
                          alignment: Alignment.center)
                    ]))
              ])
        ]));
  }

  /// Section Widget
  Widget _buildFrame3() {
    return Align(
        alignment: Alignment.topCenter,
        child: Padding(
            padding: EdgeInsets.only(top: 8.v),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              _buildFrame(
                  menuImage: ImageConstant.imgUserBlack900,
                  password: "msg_account_information".tr),
              _buildFrame(
                  menuImage: ImageConstant.imgMenu, password: "lbl_password".tr)
            ])));
  }

  /// Common widget
  Widget _buildFortyFour() {
    return SizedBox(
        height: 11.v,
        width: 9.h,
        child: Stack(alignment: Alignment.center, children: [
          CustomImageView(
              imagePath: ImageConstant.imgTelevision,
              height: 11.v,
              width: 9.h,
              alignment: Alignment.center),
          CustomImageView(
              imagePath: ImageConstant.imgTelevision,
              height: 11.v,
              width: 9.h,
              alignment: Alignment.center)
        ]));
  }

  /// Common widget
  Widget _buildFrame({
    required String menuImage,
    required String password,
  }) {
    return Container(
        width: double.maxFinite,
        padding: EdgeInsets.fromLTRB(16.h, 12.v, 16.h, 11.v),
        decoration: AppDecoration.outlineGray,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          CustomImageView(
              imagePath: menuImage, height: 20.adaptSize, width: 20.adaptSize),
          Padding(
              padding: EdgeInsets.only(left: 10.h),
              child: Text(password,
                  style: CustomTextStyles.bodyMediumInter
                      .copyWith(color: appTheme.black900))),
          Spacer(),
          CustomImageView(
              imagePath: ImageConstant.imgArrowRight,
              height: 16.adaptSize,
              width: 16.adaptSize)
        ]));
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft() {
    Get.back();
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft1() {
    Get.back();
  }
}
