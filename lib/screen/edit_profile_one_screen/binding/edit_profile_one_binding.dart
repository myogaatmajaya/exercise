import '../controller/edit_profile_one_controller.dart';
import 'package:get/get.dart';

class EditProfileOneBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => EditProfileOneController());
  }
}
