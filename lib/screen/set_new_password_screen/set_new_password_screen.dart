import 'controller/set_new_password_controller.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/appbar_leading_image.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:exercise/widgets/custom_text_form_field.dart';
import 'package:flutter/material.dart';

class SetNewPasswordScreen extends GetWidget<SetNewPasswordController> {
  const SetNewPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: _buildAppBar(),
            body: Container(
                width: double.maxFinite,
                padding: EdgeInsets.symmetric(horizontal: 19.h, vertical: 80.v),
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  Spacer(),
                  Text("msg_atur_ulang_sandi".tr,
                      style: theme.textTheme.titleLarge),
                  SizedBox(height: 6.v),
                  Container(
                      width: 249.h,
                      margin: EdgeInsets.only(left: 43.h, right: 44.h),
                      child: Text("msg_sandi_baru_kamu".tr,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: CustomTextStyles.bodyMediumBluegray900d3
                              .copyWith(height: 1.50))),
                  SizedBox(height: 26.v),
                  _buildEmailAddress(),
                  SizedBox(height: 20.v),
                  _buildUlangSandi(),
                  SizedBox(height: 20.v),
                  CustomElevatedButton(height: 56.v, text: "lbl_kirim".tr)
                ]))));
  }

  /// Section Widget
  PreferredSizeWidget _buildAppBar() {
    return CustomAppBar(
        height: 58.v,
        leadingWidth: 374.h,
        leading: AppbarLeadingImage(
            imagePath: ImageConstant.imgVectorBlack900,
            margin: EdgeInsets.fromLTRB(26.h, 21.v, 341.h, 21.v),
            onTap: () {
              onTapVector();
            }));
  }

  /// Section Widget
  Widget _buildEmailAddress() {
    return Padding(
        padding: EdgeInsets.only(left: 1.h),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: EdgeInsets.only(left: 1.h),
              child: RichText(
                  text: TextSpan(children: [
                    TextSpan(
                        text: "lbl_sandi".tr,
                        style: CustomTextStyles.labelLargeBluegray900),
                    TextSpan(
                        text: "lbl".tr,
                        style: CustomTextStyles.labelLargeRed500)
                  ]),
                  textAlign: TextAlign.left)),
          SizedBox(height: 3.v),
          CustomTextFormField(
              controller: controller.fieldController,
              borderDecoration: TextFormFieldStyleHelper.outlineBlueGray,
              filled: false)
        ]));
  }

  /// Section Widget
  Widget _buildUlangSandi() {
    return Padding(
        padding: EdgeInsets.only(left: 1.h),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: EdgeInsets.only(left: 1.h),
              child: RichText(
                  text: TextSpan(children: [
                    TextSpan(
                        text: "lbl_ulang_sandi2".tr,
                        style: CustomTextStyles.labelLargeBluegray900),
                    TextSpan(
                        text: "lbl".tr,
                        style: CustomTextStyles.labelLargeRed500)
                  ]),
                  textAlign: TextAlign.left)),
          SizedBox(height: 1.v),
          CustomTextFormField(
              controller: controller.fieldController1,
              textInputAction: TextInputAction.done,
              borderDecoration: TextFormFieldStyleHelper.outlineBlueGray,
              filled: false)
        ]));
  }

  /// Navigates to the previous screen.
  onTapVector() {
    Get.back();
  }
}
