import 'package:exercise/core/app_export.dart';
import 'package:exercise/screen/set_new_password_screen/models/set_new_password_model.dart';
import 'package:flutter/material.dart';

class SetNewPasswordController extends GetxController {
  TextEditingController fieldController = TextEditingController();

  TextEditingController fieldController1 = TextEditingController();

  Rx<SetNewPasswordModel> setNewPasswordModelObj = SetNewPasswordModel().obs;

  @override
  void onClose() {
    super.onClose();
    fieldController.dispose();
    fieldController1.dispose();
  }
}
