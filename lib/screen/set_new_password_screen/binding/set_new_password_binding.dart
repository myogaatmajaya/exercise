import '../controller/set_new_password_controller.dart';
import 'package:get/get.dart';

class SetNewPasswordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SetNewPasswordController());
  }
}
