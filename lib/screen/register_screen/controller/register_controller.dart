import 'package:exercise/core/app_export.dart';
import 'package:exercise/screen/register_screen/models/register_model.dart';
import 'package:exercise/screen/register_screen/repository/auth_repo.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class RegisterController extends GetxController {
  static RegisterController get instance => Get.find();

  final email = TextEditingController();
  final phone = TextEditingController();
  final password = TextEditingController();
  final fullname = TextEditingController();

  final isLoading = false.obs;

  void registerUser(String email, String password) async {
    try {
      isLoading.value = true;
      await AuthRepo.instance.createUserWithEmailAndPassword(email, password);
      isLoading.value = false;
    } catch (e) {
      isLoading.value = false;
      print(e);
    }
  }

  Future<void> googleSignIn() async {
    try {
      final auth = AuthRepo.instance;
      isLoading.value = true;
      await AuthRepo.instance.signInWithGoogle();
      auth.setInitialScreen(auth.firebaseUser.value);
      isLoading.value = false;
    } catch (e) {
      isLoading.value = false;
      print(e);
    }
  }
}
