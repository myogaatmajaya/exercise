import 'package:exercise/core/utils/utils.dart';
import 'package:exercise/screen/home_screen/widgets/home_screen.dart';
import 'package:exercise/screen/login_screen/login_screen.dart';
import 'package:exercise/screen/register_screen/controller/register_controller.dart';
import 'package:exercise/screen/register_screen/models/register_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:get/get.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';

class RegisterScreen extends StatelessWidget {
  final RegisterModel model = RegisterModel();
  static final _formKey = new GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(RegisterController());
    double baseWidth = 375;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.80;
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.fromLTRB(19 * fem, 0 * fem, 14 * fem, 42 * fem),
            width: double.infinity,
            decoration: BoxDecoration(
              color: Color(0xffffffff),
            ),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        36 * fem, 0 * fem, 41 * fem, 23 * fem),
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Center(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(
                                0 * fem, 0 * fem, 0 * fem, 4 * fem),
                            child: Text(
                              'Buat Akun',
                              textAlign: TextAlign.center,
                              style: SafeGoogleFont(
                                'Open Sans',
                                fontSize: 22 * ffem,
                                fontWeight: FontWeight.w600,
                                height: 1.2530273091 * ffem / fem,
                                color: Color(0xff000000),
                              ),
                            ),
                          ),
                        ),
                        Center(
                          child: Text(
                            'Daftar sekarang untuk memulai akunmu',
                            textAlign: TextAlign.center,
                            style: SafeGoogleFont(
                              'Open Sans',
                              fontSize: 14 * ffem,
                              fontWeight: FontWeight.w400,
                              height: 1.3400000163 * ffem / fem,
                              color: Color(0xd31a293d),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Center(
                    child: GestureDetector(
                      onTap: () async {
                        controller.isLoading.value = true;
                        await controller.googleSignIn();

                        controller.isLoading.value = false;
                      },
                      child: Obx(() {
                        return Container(
                          margin: EdgeInsets.fromLTRB(
                              1 * fem, 0 * fem, 5 * fem, 16 * fem),
                          padding: EdgeInsets.fromLTRB(
                              62.5 * fem, 17 * fem, 63 * fem, 17 * fem),
                          width: double.infinity,
                          height: 56 * fem,
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(0x421c3454)),
                            borderRadius: BorderRadius.circular(4 * fem),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.fromLTRB(
                                    0 * fem, 1.5 * fem, 20 * fem, 1.5 * fem),
                                padding: EdgeInsets.fromLTRB(
                                    0.5 * fem, 0 * fem, 0 * fem, 0 * fem),
                                height: double.infinity,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: SizedBox(
                                    width: 19 * fem,
                                    height: 19 * fem,
                                    child: Image.asset(
                                      'assets/page-1/images/googleglogo-1-1Jd.png',
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              Center(
                                child: Text(
                                  'Masuk dengan Google',
                                  textAlign: TextAlign.center,
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 16 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    color: Color(0xb71b2b41),
                                  ),
                                ),
                              ),
                              Center(
                                child: SizedBox(width: 10),
                              ),
                              controller.isLoading.value
                                  ? CircularProgressIndicator()
                                  : SizedBox(),
                            ],
                          ),
                        );
                      }),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        15 * fem, 0 * fem, 19 * fem, 14.5 * fem),
                    width: double.infinity,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              0 * fem, 4 * fem, 10 * fem, 0 * fem),
                          width: 126 * fem,
                          height: 1 * fem,
                          decoration: BoxDecoration(
                            color: Color(0x421c3454),
                          ),
                        ),
                        Center(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(
                                0 * fem, 0 * fem, 15 * fem, 0 * fem),
                            child: Text(
                              'Atau',
                              textAlign: TextAlign.center,
                              style: SafeGoogleFont(
                                'Open Sans',
                                fontSize: 14 * ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.3400000163 * ffem / fem,
                                color: Color(0x421c3454),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              0 * fem, 4 * fem, 0 * fem, 0 * fem),
                          width: 126 * fem,
                          height: 1 * fem,
                          decoration: BoxDecoration(
                            color: Color(0x421c3454),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 4 * fem, 14.5 * fem),
                    width: 336 * fem,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3 * fem),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              1 * fem, 0 * fem, 0 * fem, 3.5 * fem),
                          child: RichText(
                            text: TextSpan(
                              style: SafeGoogleFont(
                                'Plus Jakarta Sans',
                                fontSize: 12 * ffem,
                                fontWeight: FontWeight.w500,
                                height: 1.171875 * ffem / fem,
                                color: Color(0xffff4238),
                              ),
                              children: [
                                TextSpan(
                                  text: 'Nama',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    color: Color(0xff1676f3),
                                  ),
                                ),
                                TextSpan(
                                  text: '*',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w400,
                                    height: 1.3625 * ffem / fem,
                                    color: Color(0xffff4238),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 336 * fem,
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: controller.fullname,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 4 * fem, 14.5 * fem),
                    width: 336 * fem,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3 * fem),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              1 * fem, 0 * fem, 0 * fem, 3.5 * fem),
                          child: RichText(
                            text: TextSpan(
                              style: SafeGoogleFont(
                                'Open Sans',
                                fontSize: 12 * ffem,
                                fontWeight: FontWeight.w300,
                                height: 1.3618164062 * ffem / fem,
                                fontStyle: FontStyle.italic,
                                color: Color(0xb71b2b41),
                              ),
                              children: [
                                TextSpan(
                                  text: 'Email',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    fontStyle: FontStyle.italic,
                                    color: Color(0xb71b2b41),
                                  ),
                                ),
                                TextSpan(
                                  text: '*',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    fontStyle: FontStyle.italic,
                                    color: Color(0xffff4238),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 336 * fem,
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: controller.email,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 4 * fem, 14.5 * fem),
                    width: 336 * fem,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3 * fem),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              1 * fem, 0 * fem, 0 * fem, 3.5 * fem),
                          child: RichText(
                            text: TextSpan(
                              style: SafeGoogleFont(
                                'Open Sans',
                                fontSize: 12 * ffem,
                                fontWeight: FontWeight.w300,
                                height: 1.3618164062 * ffem / fem,
                                fontStyle: FontStyle.italic,
                                color: Color(0xb71b2b41),
                              ),
                              children: [
                                TextSpan(
                                  text: 'NIK',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    fontStyle: FontStyle.italic,
                                    color: Color(0xb71b2b41),
                                  ),
                                ),
                                TextSpan(
                                  text: '*',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    fontStyle: FontStyle.italic,
                                    color: Color(0xffff4238),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 336 * fem,
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 4 * fem, 14.5 * fem),
                    width: 336 * fem,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3 * fem),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              1 * fem, 0 * fem, 0 * fem, 3.5 * fem),
                          child: RichText(
                            text: TextSpan(
                              style: SafeGoogleFont(
                                'Open Sans',
                                fontSize: 12 * ffem,
                                fontWeight: FontWeight.w300,
                                height: 1.3618164062 * ffem / fem,
                                fontStyle: FontStyle.italic,
                                color: Color(0xb71b2b41),
                              ),
                              children: [
                                TextSpan(
                                  text: 'Nomor Handphone',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    fontStyle: FontStyle.italic,
                                    color: Color(0xb71b2b41),
                                  ),
                                ),
                                TextSpan(
                                  text: '*',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    fontStyle: FontStyle.italic,
                                    color: Color(0xffff4238),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 336 * fem,
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: controller.phone,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 4 * fem, 14.5 * fem),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              1 * fem, 0 * fem, 0 * fem, 3.5 * fem),
                          child: RichText(
                            text: TextSpan(
                              style: SafeGoogleFont(
                                'Open Sans',
                                fontSize: 12 * ffem,
                                fontWeight: FontWeight.w300,
                                height: 1.3618164062 * ffem / fem,
                                fontStyle: FontStyle.italic,
                                color: Color(0xb71b2b41),
                              ),
                              children: [
                                TextSpan(
                                  text: 'Sandi',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    fontStyle: FontStyle.italic,
                                    color: Color(0xb71b2b41),
                                  ),
                                ),
                                TextSpan(
                                  text: '*',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    fontStyle: FontStyle.italic,
                                    color: Color(0xffff4238),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 336 * fem,
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: controller.password,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 4 * fem, 18 * fem),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              1 * fem, 0 * fem, 0 * fem, 3.5 * fem),
                          child: RichText(
                            text: TextSpan(
                              style: SafeGoogleFont(
                                'Open Sans',
                                fontSize: 12 * ffem,
                                fontWeight: FontWeight.w300,
                                height: 1.3618164062 * ffem / fem,
                                fontStyle: FontStyle.italic,
                                color: Color(0xb71b2b41),
                              ),
                              children: [
                                TextSpan(
                                  text: 'Ulang Sandi',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    fontStyle: FontStyle.italic,
                                    color: Color(0xb71b2b41),
                                  ),
                                ),
                                TextSpan(
                                  text: '*',
                                  style: SafeGoogleFont(
                                    'Open Sans',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.3625 * ffem / fem,
                                    fontStyle: FontStyle.italic,
                                    color: Color(0xffff4238),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 336 * fem,
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: controller.password,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 61 * fem, 20.5 * fem),
                    width: double.infinity,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              0 * fem, 0 * fem, 10 * fem, 0.5 * fem),
                          width: 18 * fem,
                          height: 18 * fem,
                          child: Image.asset(
                            'assets/page-1/images/checkbox.png',
                            width: 18 * fem,
                            height: 18 * fem,
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            style: SafeGoogleFont(
                              'Open Sans',
                              fontSize: 12 * ffem,
                              fontWeight: FontWeight.w300,
                              height: 1.3618164062 * ffem / fem,
                              fontStyle: FontStyle.italic,
                              color: Color(0xb71b2b41),
                            ),
                            children: [
                              TextSpan(
                                text: 'I have read and agree to the ',
                                style: SafeGoogleFont(
                                  'Open Sans',
                                  fontSize: 12 * ffem,
                                  fontWeight: FontWeight.w400,
                                  height: 1.3625 * ffem / fem,
                                  fontStyle: FontStyle.italic,
                                  color: Color(0xb71b2b41),
                                ),
                              ),
                              TextSpan(
                                text: 'Terms of Service',
                                style: SafeGoogleFont(
                                  'Open Sans',
                                  fontSize: 12 * ffem,
                                  fontWeight: FontWeight.w400,
                                  height: 1.3625 * ffem / fem,
                                  fontStyle: FontStyle.italic,
                                  decoration: TextDecoration.underline,
                                  color: Color(0xff009cff),
                                  decorationColor: Color(0xff009cff),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          if (controller.email.text.trim().isEmpty ||
                              controller.password.text.trim().isEmpty) {
                            Get.to(RegisterScreen());
                          } else {
                            RegisterController.instance.registerUser(
                                controller.email.text.trim(),
                                controller.password.text.trim());
                            Get.to(HomeScreen());
                          }
                        } else {
                          print('Form is not valid');
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4 * fem),
                        ),
                        minimumSize: Size(double.infinity, 56 * fem),
                      ),
                      child: Text(
                        'Mulai',
                        style: SafeGoogleFont(
                          'Open Sans',
                          fontSize: 16 * ffem,
                          fontWeight: FontWeight.w700,
                          height: 1.3625 * ffem / fem,
                          color: Color(0xffffffff),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        68 * fem, 0 * fem, 80 * fem, 0 * fem),
                    width: double.infinity,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Center(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(
                                0 * fem, 0 * fem, 2 * fem, 0 * fem),
                            child: Text(
                              'Already have an account?',
                              textAlign: TextAlign.center,
                              style: SafeGoogleFont(
                                'Open Sans',
                                fontSize: 13 * ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.220000047 * ffem / fem,
                                color: Color(0xff000000),
                              ),
                            ),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Get.to(LoginScreen());
                          },
                          child: Center(
                            child: Text(
                              'Log in',
                              textAlign: TextAlign.center,
                              style: SafeGoogleFont(
                                'Open Sans',
                                fontSize: 13 * ffem,
                                fontWeight: FontWeight.w600,
                                height: 1.3625 * ffem / fem,
                                color: Color(0xff009cff),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
