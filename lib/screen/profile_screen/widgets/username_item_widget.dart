import '../controller/profile_controller.dart';
import '../models/username_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class UsernameItemWidget extends StatelessWidget {
  UsernameItemWidget(
    this.usernameItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  UsernameItemModel usernameItemModelObj;

  var controller = Get.find<ProfileController>();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 60.h,
      child: Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            Obx(
              () => CustomImageView(
                imagePath: usernameItemModelObj.userImage!.value,
                height: 60.adaptSize,
                width: 60.adaptSize,
                radius: BorderRadius.circular(
                  30.h,
                ),
              ),
            ),
            SizedBox(height: 7.v),
            Obx(
              () => Text(
                usernameItemModelObj.userName!.value,
                style: CustomTextStyles.bodySmallBlack900,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
