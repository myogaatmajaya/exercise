import '../controller/profile_controller.dart';
import '../models/userprofile3_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Userprofile3ItemWidget extends StatelessWidget {
  Userprofile3ItemWidget(
    this.userprofile3ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Userprofile3ItemModel userprofile3ItemModelObj;

  var controller = Get.find<ProfileController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 7.v),
      decoration: AppDecoration.outlineIndigo50.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder6,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Obx(
            () => CustomImageView(
              imagePath: userprofile3ItemModelObj.image!.value,
              height: 60.adaptSize,
              width: 60.adaptSize,
              radius: BorderRadius.circular(
                4.h,
              ),
              margin: EdgeInsets.only(
                top: 4.v,
                bottom: 6.v,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 5.v),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 3.v),
                      child: Obx(
                        () => Text(
                          userprofile3ItemModelObj.text1!.value,
                          style: theme.textTheme.titleSmall,
                        ),
                      ),
                    ),
                    Container(
                      height: 10.adaptSize,
                      width: 10.adaptSize,
                      margin: EdgeInsets.only(
                        left: 50.h,
                        bottom: 13.v,
                      ),
                      decoration: BoxDecoration(
                        color: appTheme.tealA700,
                        borderRadius: BorderRadius.circular(
                          5.h,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 3.v),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Obx(
                    () => Text(
                      userprofile3ItemModelObj.text2!.value,
                      style: theme.textTheme.bodySmall,
                    ),
                  ),
                ),
                SizedBox(height: 2.v),
                SizedBox(
                  width: 239.h,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Obx(
                        () => Text(
                          userprofile3ItemModelObj.text3!.value,
                          style: theme.textTheme.bodySmall,
                        ),
                      ),
                      Obx(
                        () => Text(
                          userprofile3ItemModelObj.text4!.value,
                          style: theme.textTheme.labelLarge,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
