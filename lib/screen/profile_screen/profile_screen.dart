import 'widgets/username_item_widget.dart';
import 'widgets/userprofile3_item_widget.dart';
import 'controller/profile_controller.dart';
import 'models/username_item_model.dart';
import 'models/userprofile3_item_model.dart';
import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/app_bar/appbar_trailing_image.dart';
import 'package:exercise/widgets/app_bar/custom_app_bar.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends GetWidget<ProfileController> {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.maxFinite,
                child: Column(children: [
                  _buildArrowLeftStack(),
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 21.v),
                      child: Column(children: [
                        _buildWorkspaceSayaColumn(),
                        SizedBox(height: 21.v),
                        _buildFormSayaColumn(),
                        SizedBox(height: 5.v)
                      ]))
                ]))));
  }

  /// Section Widget
  Widget _buildArrowLeftStack() {
    return SizedBox(
        height: 257.v,
        width: double.maxFinite,
        child: Stack(alignment: Alignment.topCenter, children: [
          Align(
              alignment: Alignment.center,
              child: Container(
                  padding: EdgeInsets.all(16.h),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(ImageConstant.imgGroup49),
                          fit: BoxFit.cover)),
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          CustomImageView(
                              imagePath: ImageConstant.imgArrowLeftWhiteA700,
                              height: 28.adaptSize,
                              width: 28.adaptSize,
                              margin: EdgeInsets.only(bottom: 96.v),
                              onTap: () {
                                onTapImgArrowLeft();
                              }),
                          Spacer(flex: 38),
                          Container(
                              height: 9.adaptSize,
                              width: 9.adaptSize,
                              margin: EdgeInsets.only(top: 63.v, bottom: 52.v),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4.h),
                                  border: Border.all(
                                      color:
                                          appTheme.whiteA700.withOpacity(0.2),
                                      width: 2.h))),
                          Spacer(flex: 61),
                          Container(
                              height: 100.adaptSize,
                              width: 100.adaptSize,
                              margin: EdgeInsets.only(top: 24.v),
                              child: Stack(
                                  alignment: Alignment.bottomLeft,
                                  children: [
                                    CustomImageView(
                                        imagePath: ImageConstant.imgTelevision,
                                        height: 11.v,
                                        width: 9.h,
                                        alignment: Alignment.topRight,
                                        margin: EdgeInsets.only(
                                            top: 9.v, right: 32.h)),
                                    CustomImageView(
                                        imagePath: ImageConstant.imgTelevision,
                                        height: 11.v,
                                        width: 9.h,
                                        alignment: Alignment.bottomLeft,
                                        margin: EdgeInsets.only(
                                            left: 13.h, bottom: 11.v)),
                                    Align(
                                        alignment: Alignment.center,
                                        child: Container(
                                            height: 100.adaptSize,
                                            width: 100.adaptSize,
                                            padding: EdgeInsets.all(12.h),
                                            decoration: AppDecoration
                                                .fillWhiteA700
                                                .copyWith(
                                                    borderRadius:
                                                        BorderRadiusStyle
                                                            .roundedBorder6),
                                            child: CustomImageView(
                                                imagePath:
                                                    ImageConstant.imgLock,
                                                height: 75.adaptSize,
                                                width: 75.adaptSize,
                                                alignment: Alignment.center)))
                                  ])),
                          Padding(
                              padding: EdgeInsets.only(left: 5.h),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CustomImageView(
                                        imagePath: ImageConstant
                                            .imgTelevisionWhiteA700,
                                        height: 28.adaptSize,
                                        width: 28.adaptSize,
                                        alignment: Alignment.centerRight),
                                    SizedBox(height: 35.v),
                                    Container(
                                        height: 9.adaptSize,
                                        width: 9.adaptSize,
                                        margin: EdgeInsets.only(left: 3.h),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(4.h),
                                            border: Border.all(
                                                color: appTheme.whiteA700
                                                    .withOpacity(0.2),
                                                width: 2.h))),
                                    SizedBox(height: 30.v),
                                    Container(
                                        width: 91.h,
                                        margin: EdgeInsets.only(right: 30.h),
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CustomImageView(
                                                    imagePath: ImageConstant
                                                        .imgFrameWhiteA70016x16,
                                                    height: 16.adaptSize,
                                                    width: 16.adaptSize,
                                                    margin: EdgeInsets.only(
                                                        top: 6.v)),
                                                Container(
                                                    height: 15.adaptSize,
                                                    width: 15.adaptSize,
                                                    margin: EdgeInsets.only(
                                                        bottom: 7.v),
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(7.h),
                                                        border: Border.all(
                                                            color: appTheme
                                                                .whiteA700
                                                                .withOpacity(
                                                                    0.2),
                                                            width: 2.h)))
                                              ]),
                                        ))
                                  ]))
                        ]),
                    SizedBox(height: 8.v),
                    Text("msg_andri_yulianto_rosadi".tr,
                        style: theme.textTheme.titleMedium),
                    SizedBox(height: 12.v),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      CustomImageView(
                          imagePath: ImageConstant.imgLockWhiteA700,
                          height: 16.adaptSize,
                          width: 16.adaptSize,
                          margin: EdgeInsets.only(bottom: 2.v)),
                      Padding(
                          padding: EdgeInsets.only(left: 4.h),
                          child: Text("lbl_test_gmail_com".tr,
                              style: CustomTextStyles.bodySmallWhiteA700_1)),
                      Padding(
                          padding: EdgeInsets.only(left: 8.h),
                          child: SizedBox(
                              height: 18.v,
                              child: VerticalDivider(
                                  width: 1.h,
                                  thickness: 1.v,
                                  color: appTheme.whiteA700,
                                  endIndent: 2.h))),
                      CustomImageView(
                          imagePath: ImageConstant.imgFrame16x16,
                          height: 16.adaptSize,
                          width: 16.adaptSize,
                          margin: EdgeInsets.only(left: 8.h, bottom: 2.v)),
                      Padding(
                          padding: EdgeInsets.only(left: 4.h, bottom: 1.v),
                          child: Text("lbl_0210230223".tr,
                              style: CustomTextStyles.bodySmallWhiteA700_1))
                    ]),
                    SizedBox(height: 6.v),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      CustomImageView(
                          imagePath: ImageConstant.imgFrame1,
                          height: 16.adaptSize,
                          width: 16.adaptSize,
                          margin: EdgeInsets.only(bottom: 1.v)),
                      Padding(
                          padding: EdgeInsets.only(left: 4.h),
                          child: Text("msg_1232138693834912".tr,
                              style: CustomTextStyles.bodySmallWhiteA700_1))
                    ]),
                    SizedBox(height: 16.v)
                  ]))),
          CustomAppBar(height: 7.v, actions: [
            AppbarTrailingImage(
                imagePath: ImageConstant.imgTelevision,
                margin: EdgeInsets.symmetric(horizontal: 40.h))
          ])
        ]));
  }

  /// Section Widget
  Widget _buildWorkspaceSayaColumn() {
    return Padding(
        padding: EdgeInsets.only(left: 16.h),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: EdgeInsets.only(right: 16.h),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("lbl_workspace_saya".tr,
                        style: CustomTextStyles.titleMediumBlack900),
                    Padding(
                        padding: EdgeInsets.only(top: 2.v),
                        child: Text("lbl_undangan".tr,
                            style: CustomTextStyles.titleSmallPrimary))
                  ])),
          SizedBox(height: 13.v),
          SizedBox(
              height: 84.v,
              width: 359.h,
              child: Stack(alignment: Alignment.centerRight, children: [
                Align(
                    alignment: Alignment.center,
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CustomImageView(
                                    imagePath: ImageConstant.imgEllipse69,
                                    height: 60.adaptSize,
                                    width: 60.adaptSize,
                                    radius: BorderRadius.circular(30.h)),
                                Container(
                                    height: 60.adaptSize,
                                    width: 60.adaptSize,
                                    decoration: BoxDecoration(
                                        color: appTheme.gray400,
                                        borderRadius:
                                            BorderRadius.circular(30.h)))
                              ]),
                          SizedBox(height: 7.v),
                          Padding(
                              padding: EdgeInsets.only(left: 10.h),
                              child: Text("lbl_klien_a".tr,
                                  style: CustomTextStyles.bodySmallBlack900))
                        ])),
                Align(
                    alignment: Alignment.centerRight,
                    child: SizedBox(
                        height: 84.v,
                        child: Obx(() => ListView.separated(
                            padding: EdgeInsets.only(left: 76.h),
                            scrollDirection: Axis.horizontal,
                            separatorBuilder: (context, index) {
                              return SizedBox(width: 16.h);
                            },
                            itemCount: controller.profileModelObj.value
                                .usernameItemList.value.length,
                            itemBuilder: (context, index) {
                              UsernameItemModel model = controller
                                  .profileModelObj
                                  .value
                                  .usernameItemList
                                  .value[index];
                              return UsernameItemWidget(model);
                            }))))
              ]))
        ]));
  }

  /// Section Widget
  Widget _buildFormSayaColumn() {
    return Padding(
        padding: EdgeInsets.only(left: 16.h),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text("lbl_form_saya".tr, style: CustomTextStyles.titleMediumBlack900),
          SizedBox(height: 13.v),
          Padding(
              padding: EdgeInsets.only(right: 16.h),
              child: Obx(() => ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  separatorBuilder: (context, index) {
                    return SizedBox(height: 8.v);
                  },
                  itemCount: controller
                      .profileModelObj.value.userprofile3ItemList.value.length,
                  itemBuilder: (context, index) {
                    Userprofile3ItemModel model = controller.profileModelObj
                        .value.userprofile3ItemList.value[index];
                    return Userprofile3ItemWidget(model);
                  })))
        ]));
  }

  /// Navigates to the previous screen.
  onTapImgArrowLeft() {
    Get.back();
  }
}
