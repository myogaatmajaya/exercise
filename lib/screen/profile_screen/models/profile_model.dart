import '../../../core/app_export.dart';
import 'username_item_model.dart';
import 'userprofile3_item_model.dart';

class ProfileModel {
  Rx<List<UsernameItemModel>> usernameItemList = Rx([
    UsernameItemModel(
        userImage: ImageConstant.imgEllipse70.obs, userName: "Klien B".obs),
    UsernameItemModel(
        userImage: ImageConstant.imgEllipse71.obs, userName: "Klien C".obs),
    UsernameItemModel(
        userImage: ImageConstant.imgEllipse72.obs, userName: "Klien C".obs),
    UsernameItemModel(
        userImage: ImageConstant.imgEllipse75.obs, userName: "Klien D".obs)
  ]);

  Rx<List<Userprofile3ItemModel>> userprofile3ItemList = Rx([
    Userprofile3ItemModel(
        image: ImageConstant.imgRectangle3016.obs,
        text1: "Elektabilitas Paslon Kaltim".obs,
        text2: "Amet minim mollit non deserunt est sit... ".obs,
        text3: "23/03/22 16:36".obs,
        text4: "Klien A".obs),
    Userprofile3ItemModel(
        image: ImageConstant.imgRectangle3016.obs,
        text1: "Elektabilitas Paslon Kaltim".obs,
        text2: "Amet minim mollit non deserunt est sit... ".obs,
        text3: "23/03/22 16:36".obs,
        text4: "Klien A".obs),
    Userprofile3ItemModel(
        image: ImageConstant.imgRectangle301660x60.obs,
        text1: "Laporan Covid".obs,
        text2: "Amet minim mollit non deserunt est sit... ".obs,
        text3: "23/03/22 16:36".obs,
        text4: "Klien A".obs)
  ]);
}
