import '../../../core/app_export.dart';

class Userprofile3ItemModel {
  Userprofile3ItemModel({
    this.image,
    this.text1,
    this.text2,
    this.text3,
    this.text4,
    this.id,
  }) {
    image = image ?? Rx(ImageConstant.imgRectangle3016);
    text1 = text1 ?? Rx("Elektabilitas Paslon Kaltim");
    text2 = text2 ?? Rx("Amet minim mollit non deserunt est sit... ");
    text3 = text3 ?? Rx("23/03/22 16:36");
    text4 = text4 ?? Rx("Klien A");
    id = id ?? Rx("");
  }

  Rx<String>? image;

  Rx<String>? text1;

  Rx<String>? text2;

  Rx<String>? text3;

  Rx<String>? text4;

  Rx<String>? id;
}
