import '../../../core/app_export.dart';

class UsernameItemModel {
  UsernameItemModel({
    this.userImage,
    this.userName,
    this.id,
  }) {
    userImage = userImage ?? Rx(ImageConstant.imgEllipse70);
    userName = userName ?? Rx("Klien B");
    id = id ?? Rx("");
  }

  Rx<String>? userImage;

  Rx<String>? userName;

  Rx<String>? id;
}
