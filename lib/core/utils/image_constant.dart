class ImageConstant {
  // Image folder path
  static String imagePath = 'assets/images';

  // Home images
  static String imgGroup116 = '$imagePath/img_group_116.png';

  static String imgGroup2161 = '$imagePath/img_group_2161.svg';

  static String imgStar1 = '$imagePath/img_star_1.svg';

  static String imgStar2 = '$imagePath/img_star_2.svg';

  static String imgPolygon3DeepOrange100 =
      '$imagePath/img_polygon_3_deep_orange_100.svg';

  static String imgVector48x35 = '$imagePath/img_vector_48x35.png';

  static String imgStar19x9 = '$imagePath/img_star_1_9x9.svg';

  static String imgVector12x19 = '$imagePath/img_vector_12x19.png';

  static String imgVector9x14 = '$imagePath/img_vector_9x14.png';

  static String imgVector24x26 = '$imagePath/img_vector_24x26.png';

  static String imgVector1 = '$imagePath/img_vector_1.png';

  static String imgStar29x9 = '$imagePath/img_star_2_9x9.svg';

  static String imgStar110x10 = '$imagePath/img_star_1_10x10.svg';

  static String imgIconParkSolidCheckOne =
      '$imagePath/img_icon_park_solid_check_one.svg';

  static String imgGroup24TealA700 = '$imagePath/img_group_24_teal_a700.svg';

  static String imgStar210x10 = '$imagePath/img_star_2_10x10.svg';

  static String imgUserTealA700 = '$imagePath/img_user_teal_a700.svg';

  // Home Two images
  static String imgStar11 = '$imagePath/img_star_1_1.svg';

  static String imgStar21 = '$imagePath/img_star_2_1.svg';

  static String imgUnion = '$imagePath/img_union.png';

  static String imgVector8x7 = '$imagePath/img_vector_8x7.png';

  static String imgVector2 = '$imagePath/img_vector_2.png';

  static String imgVector4x2 = '$imagePath/img_vector_4x2.png';

  static String imgVector12x11 = '$imagePath/img_vector_12x11.png';

  static String imgVector14x14 = '$imagePath/img_vector_14x14.png';

  static String imgProfile = '$imagePath/img_profile.svg';

  static String imgVector16x11 = '$imagePath/img_vector_16x11.png';

  static String imgUnion21x13 = '$imagePath/img_union_21x13.png';

  static String imgVector3 = '$imagePath/img_vector_3.png';

  static String imgVector4 = '$imagePath/img_vector_4.png';

  static String imgVector20x19 = '$imagePath/img_vector_20x19.png';

  static String imgVector5x3 = '$imagePath/img_vector_5x3.png';

  static String imgVectorBlue200 = '$imagePath/img_vector_blue_200.svg';

  static String imgVectorBlue2001x2 = '$imagePath/img_vector_blue_200_1x2.svg';

  static String imgStar12 = '$imagePath/img_star_1_2.svg';

  static String imgStar22 = '$imagePath/img_star_2_2.svg';

  static String imgGroup103 = '$imagePath/img_group_103.png';

  static String imgVector2WhiteA700 = '$imagePath/img_vector_2_white_a700.svg';

  // Hasil Pencarian images
  static String imgCiFilterBlack900 = '$imagePath/img_ci_filter_black_900.svg';

  // Profile images
  static String imgTelevisionWhiteA700 =
      '$imagePath/img_television_white_a700.svg';

  static String imgFrameWhiteA70016x16 =
      '$imagePath/img_frame_white_a700_16x16.svg';

  static String imgLockWhiteA700 = '$imagePath/img_lock_white_a700.svg';

  static String imgFrame16x16 = '$imagePath/img_frame_16x16.svg';

  static String imgFrame1 = '$imagePath/img_frame_1.svg';

  static String imgEllipse69 = '$imagePath/img_ellipse_69.png';

  static String imgEllipse70 = '$imagePath/img_ellipse_70.png';

  static String imgEllipse71 = '$imagePath/img_ellipse_71.png';

  static String imgEllipse72 = '$imagePath/img_ellipse_72.png';

  static String imgEllipse75 = '$imagePath/img_ellipse_75.png';

  // Notifikasi One images
  static String imgEllipse77 = '$imagePath/img_ellipse_77.png';

  static String imgEllipse7732x32 = '$imagePath/img_ellipse_77_32x32.png';

  static String imgRectangle3019 = '$imagePath/img_rectangle_3019.png';

  static String imgEllipse771 = '$imagePath/img_ellipse_77_1.png';

  // Form One images
  static String imgStar23 = '$imagePath/img_star_2_3.svg';

  static String imgStar13 = '$imagePath/img_star_1_3.svg';

  static String imgStar18x8 = '$imagePath/img_star_1_8x8.svg';

  static String imgStar28x8 = '$imagePath/img_star_2_8x8.svg';

  // Form Laporan images
  static String imgGroup2146 = '$imagePath/img_group_2146.svg';

  static String imgArrowdownBlueGray90002 =
      '$imagePath/img_arrowdown_blue_gray_900_02.svg';

  // Form images
  static String imgMaterialSymbolsEdit =
      '$imagePath/img_material_symbols_edit.svg';

  static String imgStar24 = '$imagePath/img_star_2_4.svg';

  static String imgStar14 = '$imagePath/img_star_1_4.svg';

  static String imgStar15 = '$imagePath/img_star_1_5.svg';

  static String imgStar25 = '$imagePath/img_star_2_5.svg';

  // Edit Profile One images
  static String imgUserBlack900 = '$imagePath/img_user_black_900.svg';

  static String imgArrowRight = '$imagePath/img_arrow_right.svg';

  static String imgMenu = '$imagePath/img_menu.svg';

  // Edit Profile images
  static String imgArrowdown = '$imagePath/img_arrowdown.svg';

  // Login images
  static String imgCheckmarkWhiteA700 =
      '$imagePath/img_checkmark_white_a700.svg';

  // Register images
  static String imgPlay = '$imagePath/img_play.svg';

  // Forgot Password images
  static String imgUndrawForgotP = '$imagePath/img_undraw_forgot_p.svg';

  // Sent Code Password images
  static String imgGroup2145 = '$imagePath/img_group_2145.png';

  // Set New Password images
  static String imgVectorBlack900 = '$imagePath/img_vector_black_900.svg';

  // Notifikasi images
  static String imgVuesaxBoldCloudPlus =
      '$imagePath/img_vuesax_bold_cloud_plus.svg';

  static String imgVuesaxBoldCloudCross =
      '$imagePath/img_vuesax_bold_cloud_cross.svg';

  static String imgCheckmarkTealA700 = '$imagePath/img_checkmark_teal_a700.svg';

  static String imgClose = '$imagePath/img_close.svg';

  static String imgGroup2166 = '$imagePath/img_group_2166.svg';

  static String imgGroup2165 = '$imagePath/img_group_2165.svg';

  // Tidak Ada Koneksi images
  static String imgConnectionIllustration =
      '$imagePath/img_connection_illustration.png';

  // Data Sukses images
  static String imgSuccessfulIllustration =
      '$imagePath/img_successful_illustration.svg';

  // Data Gagal Terkirim images
  static String imgIllustrations = '$imagePath/img_illustrations.svg';

  // GPS Tidak Aktif images
  static String imgCurrentLocationPana =
      '$imagePath/img_current_location_pana.svg';

  // Lokasi Tidak Terdeteksi images
  static String imgUndrawCurrent = '$imagePath/img_undraw_current.svg';

  // Common images
  static String imgAntDesignInfoCircleFilled =
      '$imagePath/img_ant_design_info_circle_filled.svg';

  static String imgFrame = '$imagePath/img_frame.svg';

  static String imgTelevision = '$imagePath/img_television.svg';

  static String imgCifilter = '$imagePath/img_cifilter.svg';

  static String imgFrameWhiteA700 = '$imagePath/img_frame_white_a700.svg';

  static String imgLock = '$imagePath/img_lock.svg';

  static String imgCheckmark = '$imagePath/img_checkmark.svg';

  static String imgRewind = '$imagePath/img_rewind.svg';

  static String imgVector = '$imagePath/img_vector.png';

  static String imgVector35x39 = '$imagePath/img_vector_35x39.png';

  static String imgVector1x27 = '$imagePath/img_vector_1x27.png';

  static String imgVector23x17 = '$imagePath/img_vector_23x17.png';

  static String imgVector3x5 = '$imagePath/img_vector_3x5.png';

  static String imgGroup24 = '$imagePath/img_group_24.svg';

  static String imgVector13x14 = '$imagePath/img_vector_13x14.png';

  static String imgSubtract = '$imagePath/img_subtract.png';

  static String imgPolygon2 = '$imagePath/img_polygon_2.png';

  static String imgPolygon3 = '$imagePath/img_polygon_3.png';

  static String imgUser = '$imagePath/img_user.svg';

  static String imgMegaphone = '$imagePath/img_megaphone.svg';

  static String imgVector2x1 = '$imagePath/img_vector_2x1.png';

  static String imgPolygon1 = '$imagePath/img_polygon_1.png';

  static String imgGroup21 = '$imagePath/img_group_21.png';

  static String imgEllipse78 = '$imagePath/img_ellipse_78.png';

  static String imgEllipse7832x32 = '$imagePath/img_ellipse_78_32x32.png';

  static String imgEllipse781 = '$imagePath/img_ellipse_78_1.png';

  static String imgEllipse782 = '$imagePath/img_ellipse_78_2.png';

  static String imgEllipse783 = '$imagePath/img_ellipse_78_3.png';

  static String imgGroup49 = '$imagePath/img_group_49.png';

  static String imgArrowLeftWhiteA700 =
      '$imagePath/img_arrow_left_white_a700.svg';

  static String imgRectangle3016 = '$imagePath/img_rectangle_3016.png';

  static String imgRectangle301660x60 =
      '$imagePath/img_rectangle_3016_60x60.png';

  static String imgArrowLeftBlack900 =
      '$imagePath/img_arrow_left_black_900.svg';

  static String imgRectangle3009 = '$imagePath/img_rectangle_3009.png';

  static String imgBxsUser = '$imagePath/img_bxs_user.svg';

  static String imgBiCalendarFill = '$imagePath/img_bi_calendar_fill.svg';

  static String imgHeroiconsSolidStatusOnline =
      '$imagePath/img_heroicons_solid_status_online.svg';

  static String imgIcSharpBusinessCenter =
      '$imagePath/img_ic_sharp_business_center.svg';

  static String imgVector29x42 = '$imagePath/img_vector_29x42.png';

  static String imgVector14x42 = '$imagePath/img_vector_14x42.png';

  static String imgVector14x24 = '$imagePath/img_vector_14x24.png';

  static String imgRectangle8 = '$imagePath/img_rectangle_8.svg';

  static String imgRectangle10 = '$imagePath/img_rectangle_10.svg';

  static String imgRectangle11 = '$imagePath/img_rectangle_11.svg';

  static String imgGroup1 = '$imagePath/img_group_1.png';

  static String imgArrowRightBlueGray300 =
      '$imagePath/img_arrow_right_blue_gray_300.svg';

  static String imgVectorCyan300 = '$imagePath/img_vector_cyan_300.svg';

  static String imgArrowLeft = '$imagePath/img_arrow_left.svg';

  static String imgGoogleglogo1 = '$imagePath/img_googleglogo_1.png';

  static String imgEye = '$imagePath/img_eye.svg';

  static String imgAkariconsquestionfill =
      '$imagePath/img_akariconsquestionfill.svg';

  static String imageNotFound = 'assets/images/image_not_found.png';
}
