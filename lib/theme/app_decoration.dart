import 'package:flutter/material.dart';
import 'package:exercise/core/app_export.dart';

class AppDecoration {
  // Fill decorations
  static BoxDecoration get fillDeepOrange => BoxDecoration(
        color: appTheme.deepOrange100,
      );
  static BoxDecoration get fillGray => BoxDecoration(
        color: appTheme.gray50,
      );
  static BoxDecoration get fillPrimary => BoxDecoration(
        color: theme.colorScheme.primary,
      );
  static BoxDecoration get fillPrimary1 => BoxDecoration(
        color: theme.colorScheme.primary.withOpacity(0.08),
      );
  static BoxDecoration get fillWhiteA => BoxDecoration(
        color: appTheme.whiteA700,
      );
  static BoxDecoration get fillWhiteA700 => BoxDecoration(
        color: appTheme.whiteA700.withOpacity(0.3),
      );

  // Gradient decorations
  static BoxDecoration get gradientBlueToBlueA => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.5, 0),
          end: Alignment(0.5, 1),
          colors: [
            appTheme.blue300,
            appTheme.blueA200,
          ],
        ),
      );
  static BoxDecoration get gradientBlueToOnPrimaryContainer => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.5, -0.16),
          end: Alignment(0.51, 1.59),
          colors: [
            appTheme.blue10001.withOpacity(0),
            theme.colorScheme.onPrimaryContainer,
          ],
        ),
      );
  static BoxDecoration get gradientCyanToCyan => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0, 0.23),
          end: Alignment(1, 1),
          colors: [
            appTheme.cyan20002,
            appTheme.cyan300,
          ],
        ),
      );
  static BoxDecoration get gradientDeepOrangeToDeepOrange => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.5, -0.16),
          end: Alignment(0.51, 1.59),
          colors: [
            appTheme.deepOrange5000,
            appTheme.deepOrange5001,
          ],
        ),
      );
  static BoxDecoration get gradientTealAToCyan => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0, 0.23),
          end: Alignment(1, 1),
          colors: [
            appTheme.tealA700,
            appTheme.cyan50019,
          ],
        ),
      );
  static BoxDecoration get gradientWhiteAToCyan => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.5, -0.16),
          end: Alignment(0.51, 1.59),
          colors: [
            appTheme.whiteA700.withOpacity(0),
            appTheme.cyan200,
          ],
        ),
      );
  static BoxDecoration get gradientWhiteAToCyan5033 => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.5, 0),
          end: Alignment(1, 1.31),
          colors: [
            appTheme.whiteA700.withOpacity(0.2),
            appTheme.cyan5033,
          ],
        ),
      );
  static BoxDecoration get gradientWhiteAToTealA => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.5, -0.16),
          end: Alignment(0.51, 1.59),
          colors: [
            appTheme.whiteA700.withOpacity(0),
            appTheme.tealA700.withOpacity(0.3),
          ],
        ),
      );

  // Outline decorations
  static BoxDecoration get outlineBlueGray => BoxDecoration(
        border: Border.all(
          color: appTheme.blueGray800.withOpacity(0.16),
          width: 1.h,
        ),
      );
  static BoxDecoration get outlineGray => BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: appTheme.gray200,
            width: 1.h,
          ),
        ),
      );
  static BoxDecoration get outlineIndigo => BoxDecoration(
        color: appTheme.whiteA700,
        border: Border.all(
          color: appTheme.indigo50,
          width: 1.h,
        ),
        boxShadow: [
          BoxShadow(
            color: appTheme.blueGray10026,
            spreadRadius: 2.h,
            blurRadius: 2.h,
            offset: Offset(
              0,
              12.67,
            ),
          ),
        ],
      );
  static BoxDecoration get outlineIndigo50 => BoxDecoration(
        color: appTheme.whiteA700,
        border: Border.all(
          color: appTheme.indigo50,
          width: 1.h,
        ),
      );
  static BoxDecoration get outlineIndigo501 => BoxDecoration(
        border: Border.all(
          color: appTheme.indigo50,
          width: 1.h,
        ),
      );
  static BoxDecoration get outlineIndigo502 => BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: appTheme.indigo50,
            width: 1.h,
          ),
        ),
      );
  static BoxDecoration get outlineOnError => BoxDecoration(
        color: theme.colorScheme.onError,
        border: Border.all(
          color: theme.colorScheme.onError.withOpacity(1),
          width: 1.h,
        ),
      );
  static BoxDecoration get outlinePrimary => BoxDecoration(
        color: theme.colorScheme.primary.withOpacity(0.1),
        border: Border.all(
          color: theme.colorScheme.primary,
          width: 1.h,
        ),
      );
  static BoxDecoration get outlineWhiteA => BoxDecoration(
        border: Border.all(
          color: appTheme.whiteA700.withOpacity(0.2),
          width: 2.h,
        ),
      );
}

class BorderRadiusStyle {
  // Circle borders
  static BorderRadius get circleBorder13 => BorderRadius.circular(
        13.h,
      );
  static BorderRadius get circleBorder16 => BorderRadius.circular(
        16.h,
      );
  static BorderRadius get circleBorder26 => BorderRadius.circular(
        26.h,
      );
  static BorderRadius get circleBorder29 => BorderRadius.circular(
        29.h,
      );

  // Custom borders
  static BorderRadius get customBorderTL34 => BorderRadius.vertical(
        top: Radius.circular(34.h),
      );

  // Rounded borders
  static BorderRadius get roundedBorder3 => BorderRadius.circular(
        3.h,
      );
  static BorderRadius get roundedBorder6 => BorderRadius.circular(
        6.h,
      );
}

// Comment/Uncomment the below code based on your Flutter SDK version.

// For Flutter SDK Version 3.7.2 or greater.

double get strokeAlignInside => BorderSide.strokeAlignInside;

double get strokeAlignCenter => BorderSide.strokeAlignCenter;

double get strokeAlignOutside => BorderSide.strokeAlignOutside;

// For Flutter SDK Version 3.7.1 or less.

// StrokeAlign get strokeAlignInside => StrokeAlign.inside;
//
// StrokeAlign get strokeAlignCenter => StrokeAlign.center;
//
// StrokeAlign get strokeAlignOutside => StrokeAlign.outside;
