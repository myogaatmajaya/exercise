import 'package:exercise/screen/home_screen/widgets/home_screen.dart';
import 'package:exercise/screen/home_screen/binding/home_binding.dart';
import 'package:exercise/screen/hasil_pencarian_screen/hasil_pencarian_screen.dart';
import 'package:exercise/screen/hasil_pencarian_screen/binding/hasil_pencarian_binding.dart';
import 'package:exercise/screen/profile_screen/profile_screen.dart';
import 'package:exercise/screen/profile_screen/binding/profile_binding.dart';
import 'package:exercise/screen/notifikasi_one_screen/notifikasi_one_screen.dart';
import 'package:exercise/screen/notifikasi_one_screen/binding/notifikasi_one_binding.dart';
import 'package:exercise/screen/register_screen/binding/register_binding.dart';
import 'package:exercise/screen/search_screen/search_screen.dart';
import 'package:exercise/screen/search_screen/binding/search_binding.dart';
import 'package:exercise/screen/workspace_screen/workspace_screen.dart';
import 'package:exercise/screen/workspace_screen/binding/workspace_binding.dart';
import 'package:exercise/screen/form_laporan_screen/form_laporan_screen.dart';
import 'package:exercise/screen/form_laporan_screen/binding/form_laporan_binding.dart';
import 'package:exercise/screen/form_laporan_one_screen/form_laporan_one_screen.dart';
import 'package:exercise/screen/form_laporan_one_screen/binding/form_laporan_one_binding.dart';
import 'package:exercise/screen/form_screen/form_screen.dart';
import 'package:exercise/screen/form_screen/binding/form_binding.dart';
import 'package:exercise/screen/edit_profile_one_screen/edit_profile_one_screen.dart';
import 'package:exercise/screen/edit_profile_one_screen/binding/edit_profile_one_binding.dart';
import 'package:exercise/screen/edit_profile_screen/edit_profile_screen.dart';
import 'package:exercise/screen/edit_profile_screen/binding/edit_profile_binding.dart';
import 'package:exercise/screen/change_password_screen/change_password_screen.dart';
import 'package:exercise/screen/change_password_screen/binding/change_password_binding.dart';
import 'package:exercise/screen/iphone_11_pro_x_three_screen/iphone_11_pro_x_three_screen.dart';
import 'package:exercise/screen/iphone_11_pro_x_three_screen/binding/iphone_11_pro_x_three_binding.dart';
import 'package:exercise/screen/login_screen/login_screen.dart';
import 'package:exercise/screen/login_screen/binding/login_binding.dart';
import 'package:exercise/screen/register_screen/register_screen.dart';
import 'package:exercise/screen/forgot_password_screen/forgot_password_screen.dart';
import 'package:exercise/screen/forgot_password_screen/binding/forgot_password_binding.dart';
import 'package:exercise/screen/sent_code_password_screen/sent_code_password_screen.dart';
import 'package:exercise/screen/sent_code_password_screen/binding/sent_code_password_binding.dart';
import 'package:exercise/screen/set_new_password_screen/set_new_password_screen.dart';
import 'package:exercise/screen/set_new_password_screen/binding/set_new_password_binding.dart';
import 'package:exercise/screen/notifikasi_screen/notifikasi_screen.dart';
import 'package:exercise/screen/notifikasi_screen/binding/notifikasi_binding.dart';
import 'package:exercise/screen/tidak_ada_koneksi_screen/tidak_ada_koneksi_screen.dart';
import 'package:exercise/screen/tidak_ada_koneksi_screen/binding/tidak_ada_koneksi_binding.dart';
import 'package:exercise/screen/data_sukses_screen/data_sukses_screen.dart';
import 'package:exercise/screen/data_sukses_screen/binding/data_sukses_binding.dart';
import 'package:exercise/screen/data_gagal_terkirim_screen/data_gagal_terkirim_screen.dart';
import 'package:exercise/screen/data_gagal_terkirim_screen/binding/data_gagal_terkirim_binding.dart';
import 'package:exercise/screen/gps_tidak_aktif_screen/gps_tidak_aktif_screen.dart';
import 'package:exercise/screen/gps_tidak_aktif_screen/binding/gps_tidak_aktif_binding.dart';
import 'package:exercise/screen/lokasi_tidak_terdeteksi_screen/lokasi_tidak_terdeteksi_screen.dart';
import 'package:exercise/screen/lokasi_tidak_terdeteksi_screen/binding/lokasi_tidak_terdeteksi_binding.dart';
import 'package:exercise/screen/app_navigation_screen/app_navigation_screen.dart';
import 'package:exercise/screen/app_navigation_screen/binding/app_navigation_binding.dart';
import 'package:get/get.dart';

class AppRoutes {
  static const String homeScreen = '/home_screen';

  static const String homeTwoScreen = '/home_two_screen';

  static const String hasilPencarianScreen = '/hasil_pencarian_screen';

  static const String profileScreen = '/profile_screen';

  static const String notifikasiOneScreen = '/notifikasi_one_screen';

  static const String searchScreen = '/search_screen';

  static const String workspaceScreen = '/workspace_screen';

  static const String formOneScreen = '/form_one_screen';

  static const String formLaporanScreen = '/form_laporan_screen';

  static const String formLaporanOneScreen = '/form_laporan_one_screen';

  static const String formScreen = '/form_screen';

  static const String editProfileOneScreen = '/edit_profile_one_screen';

  static const String editProfileScreen = '/edit_profile_screen';

  static const String changePasswordScreen = '/change_password_screen';

  static const String iphone11ProXThreeScreen = '/iphone_11_pro_x_three_screen';

  static const String loginScreen = '/login_screen';

  static const String registerScreen = '/register_screen';

  static const String forgotPasswordScreen = '/forgot_password_screen';

  static const String sentCodePasswordScreen = '/sent_code_password_screen';

  static const String setNewPasswordScreen = '/set_new_password_screen';

  static const String notifikasiScreen = '/notifikasi_screen';

  static const String tidakAdaKoneksiScreen = '/tidak_ada_koneksi_screen';

  static const String dataSuksesScreen = '/data_sukses_screen';

  static const String dataGagalTerkirimScreen = '/data_gagal_terkirim_screen';

  static const String gpsTidakAktifScreen = '/gps_tidak_aktif_screen';

  static const String lokasiTidakTerdeteksiScreen =
      '/lokasi_tidak_terdeteksi_screen';

  static const String appNavigationScreen = '/app_navigation_screen';

  static const String initialRoute = '/initialRoute';

  static List<GetPage> pages = [
    GetPage(
      name: homeScreen,
      page: () => HomeScreen(),
      bindings: [
        HomeBinding(),
      ],
    ),
    GetPage(
      name: hasilPencarianScreen,
      page: () => HasilPencarianScreen(),
      bindings: [
        HasilPencarianBinding(),
      ],
    ),
    GetPage(
      name: profileScreen,
      page: () => ProfileScreen(),
      bindings: [
        ProfileBinding(),
      ],
    ),
    GetPage(
      name: notifikasiOneScreen,
      page: () => NotifikasiOneScreen(),
      bindings: [
        NotifikasiOneBinding(),
      ],
    ),
    GetPage(
      name: searchScreen,
      page: () => SearchScreen(),
      bindings: [
        SearchBinding(),
      ],
    ),
    GetPage(
      name: workspaceScreen,
      page: () => WorkspaceScreen(),
      bindings: [
        WorkspaceBinding(),
      ],
    ),
    GetPage(
      name: formLaporanScreen,
      page: () => FormLaporanScreen(),
      bindings: [
        FormLaporanBinding(),
      ],
    ),
    GetPage(
      name: formLaporanOneScreen,
      page: () => FormLaporanOneScreen(),
      bindings: [
        FormLaporanOneBinding(),
      ],
    ),
    GetPage(
      name: formScreen,
      page: () => FormScreen(),
      bindings: [
        FormBinding(),
      ],
    ),
    GetPage(
      name: editProfileOneScreen,
      page: () => EditProfileOneScreen(),
      bindings: [
        EditProfileOneBinding(),
      ],
    ),
    GetPage(
      name: editProfileScreen,
      page: () => EditProfileScreen(),
      bindings: [
        EditProfileBinding(),
      ],
    ),
    GetPage(
      name: changePasswordScreen,
      page: () => ChangePasswordScreen(),
      bindings: [
        ChangePasswordBinding(),
      ],
    ),
    GetPage(
      name: iphone11ProXThreeScreen,
      page: () => Iphone11ProXThreeScreen(),
      bindings: [
        Iphone11ProXThreeBinding(),
      ],
    ),
    GetPage(
      name: loginScreen,
      page: () => LoginScreen(),
      bindings: [
        LoginBinding(),
      ],
    ),
    GetPage(
      name: registerScreen,
      page: () => RegisterScreen(),
      bindings: [
        RegisterBinding(),
      ],
    ),
    GetPage(
      name: forgotPasswordScreen,
      page: () => ForgotPasswordScreen(),
      bindings: [
        ForgotPasswordBinding(),
      ],
    ),
    GetPage(
      name: sentCodePasswordScreen,
      page: () => SentCodePasswordScreen(),
      bindings: [
        SentCodePasswordBinding(),
      ],
    ),
    GetPage(
      name: setNewPasswordScreen,
      page: () => SetNewPasswordScreen(),
      bindings: [
        SetNewPasswordBinding(),
      ],
    ),
    GetPage(
      name: notifikasiScreen,
      page: () => NotifikasiScreen(),
      bindings: [
        NotifikasiBinding(),
      ],
    ),
    GetPage(
      name: tidakAdaKoneksiScreen,
      page: () => TidakAdaKoneksiScreen(),
      bindings: [
        TidakAdaKoneksiBinding(),
      ],
    ),
    GetPage(
      name: dataSuksesScreen,
      page: () => DataSuksesScreen(),
      bindings: [
        DataSuksesBinding(),
      ],
    ),
    GetPage(
      name: dataGagalTerkirimScreen,
      page: () => DataGagalTerkirimScreen(),
      bindings: [
        DataGagalTerkirimBinding(),
      ],
    ),
    GetPage(
      name: gpsTidakAktifScreen,
      page: () => GpsTidakAktifScreen(),
      bindings: [
        GpsTidakAktifBinding(),
      ],
    ),
    GetPage(
      name: lokasiTidakTerdeteksiScreen,
      page: () => LokasiTidakTerdeteksiScreen(),
      bindings: [
        LokasiTidakTerdeteksiBinding(),
      ],
    ),
    GetPage(
      name: appNavigationScreen,
      page: () => AppNavigationScreen(),
      bindings: [
        AppNavigationBinding(),
      ],
    ),
    GetPage(
      name: initialRoute,
      page: () => Iphone11ProXThreeScreen(),
      bindings: [
        Iphone11ProXThreeBinding(),
      ],
    )
  ];
}
