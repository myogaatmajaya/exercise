import 'package:exercise/core/app_export.dart';
import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {
  CustomIconButton({
    Key? key,
    this.alignment,
    this.height,
    this.width,
    this.padding,
    this.decoration,
    this.child,
    this.onTap,
  }) : super(
          key: key,
        );

  final Alignment? alignment;

  final double? height;

  final double? width;

  final EdgeInsetsGeometry? padding;

  final BoxDecoration? decoration;

  final Widget? child;

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return alignment != null
        ? Align(
            alignment: alignment ?? Alignment.center,
            child: iconButtonWidget,
          )
        : iconButtonWidget;
  }

  Widget get iconButtonWidget => SizedBox(
        height: height ?? 0,
        width: width ?? 0,
        child: IconButton(
          padding: EdgeInsets.zero,
          icon: Container(
            height: height ?? 0,
            width: width ?? 0,
            padding: padding ?? EdgeInsets.zero,
            decoration: decoration ??
                BoxDecoration(
                  borderRadius: BorderRadius.circular(14.h),
                  border: Border.all(
                    color: appTheme.indigo5001,
                    width: 1.h,
                  ),
                ),
            child: child,
          ),
          onPressed: onTap,
        ),
      );
}

/// Extension on [CustomIconButton] to facilitate inclusion of all types of border style etc
extension IconButtonStyleHelper on CustomIconButton {
  static BoxDecoration get fillPrimary => BoxDecoration(
        color: theme.colorScheme.primary.withOpacity(0.1),
        borderRadius: BorderRadius.circular(16.h),
      );
  static BoxDecoration get fillWhiteA => BoxDecoration(
        color: appTheme.whiteA700.withOpacity(0.3),
        borderRadius: BorderRadius.circular(8.h),
      );
  static BoxDecoration get fillLightBlue => BoxDecoration(
        color: appTheme.lightBlue400,
        borderRadius: BorderRadius.circular(4.h),
      );
  static BoxDecoration get fillCyan => BoxDecoration(
        color: appTheme.cyan600,
        borderRadius: BorderRadius.circular(4.h),
      );
  static BoxDecoration get fillAmber => BoxDecoration(
        color: appTheme.amber500,
        borderRadius: BorderRadius.circular(4.h),
      );
  static BoxDecoration get fillBlue => BoxDecoration(
        color: appTheme.blue400,
        borderRadius: BorderRadius.circular(4.h),
      );
  static BoxDecoration get fillTealA => BoxDecoration(
        color: appTheme.tealA700.withOpacity(0.2),
        borderRadius: BorderRadius.circular(16.h),
      );
  static BoxDecoration get fillOrange => BoxDecoration(
        color: appTheme.orange500.withOpacity(0.2),
        borderRadius: BorderRadius.circular(16.h),
      );
  static BoxDecoration get fillRedA => BoxDecoration(
        color: appTheme.redA400.withOpacity(0.2),
        borderRadius: BorderRadius.circular(16.h),
      );
  static BoxDecoration get fillLightBlueA => BoxDecoration(
        color: appTheme.lightBlueA700.withOpacity(0.2),
        borderRadius: BorderRadius.circular(16.h),
      );
}
