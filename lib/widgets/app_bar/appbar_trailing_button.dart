import 'package:exercise/core/app_export.dart';
import 'package:exercise/widgets/custom_elevated_button.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class AppbarTrailingButton extends StatelessWidget {
  AppbarTrailingButton({
    Key? key,
    this.margin,
    this.onTap,
  }) : super(
          key: key,
        );

  EdgeInsetsGeometry? margin;

  Function? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap!.call();
      },
      child: Padding(
        padding: margin ?? EdgeInsets.zero,
        child: CustomElevatedButton(
          height: 26.v,
          width: 82.h,
          text: "lbl_bantuan".tr,
          leftIcon: Container(
            margin: EdgeInsets.only(right: 4.h),
            child: CustomImageView(
              imagePath: ImageConstant.imgAkariconsquestionfill,
              height: 14.adaptSize,
              width: 14.adaptSize,
            ),
          ),
          buttonStyle: CustomButtonStyles.fillBlue,
          buttonTextStyle: CustomTextStyles.bodyMediumPrimary,
        ),
      ),
    );
  }
}
